<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl_BE" sourcelanguage="en" version="2.1">
<context>
    <name>:</name>
    <message>
        <location filename="../../src/dlgrecording.cpp" line="176"/>
        <source/>
        <comment>The size of the file which has been stored during the current recording in megabytes (MB)</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AnalysisFeature</name>
    <message>
        <location filename="../../src/library/analysisfeature.cpp" line="33"/>
        <source>Analyze</source>
        <translation>Analyseren</translation>
    </message>
</context>
<context>
    <name>AutoDJFeature</name>
    <message>
        <location filename="../../src/library/autodj/autodjfeature.cpp" line="67"/>
        <source>Crates</source>
        <translation>Platenbakken</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/autodjfeature.cpp" line="87"/>
        <source>Remove Crate as Track Source</source>
        <translation>Verwijder platenbak als Track bron</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/autodjfeature.cpp" line="98"/>
        <source>Auto DJ</source>
        <translation>Auto DJ</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/autodjfeature.cpp" line="269"/>
        <source>Add Crate as Track Source</source>
        <translation>Voeg platenbak toe als Track bron</translation>
    </message>
</context>
<context>
    <name>BansheeFeature</name>
    <message>
        <location filename="../../src/library/banshee/bansheefeature.cpp" line="26"/>
        <location filename="../../src/library/banshee/bansheefeature.cpp" line="110"/>
        <source>Banshee</source>
        <translation>Banshee</translation>
    </message>
    <message>
        <location filename="../../src/library/banshee/bansheefeature.cpp" line="76"/>
        <location filename="../../src/library/banshee/bansheefeature.cpp" line="85"/>
        <source>Error loading Banshee database</source>
        <translation>Fout bij het laden van de Banshee database</translation>
    </message>
    <message>
        <location filename="../../src/library/banshee/bansheefeature.cpp" line="77"/>
        <source>Banshee database file not found at
</source>
        <translation>Banshee database bestand niet gevonden op
</translation>
    </message>
    <message>
        <location filename="../../src/library/banshee/bansheefeature.cpp" line="86"/>
        <source>There was an error loading your Banshee database at
</source>
        <translation>Er is een fout opgetreden bij het laden van uw Banshee database op
</translation>
    </message>
</context>
<context>
    <name>BaseExternalLibraryFeature</name>
    <message>
        <location filename="../../src/library/baseexternallibraryfeature.cpp" line="11"/>
        <source>Add to Auto DJ Queue (bottom)</source>
        <translation>Voeg toe aan de Auto DJ wachtrij (onderaan)</translation>
    </message>
    <message>
        <location filename="../../src/library/baseexternallibraryfeature.cpp" line="15"/>
        <source>Add to Auto DJ Queue (top)</source>
        <translation>Voeg toe aan de auto DJ wachtrij (bovenaan)</translation>
    </message>
    <message>
        <location filename="../../src/library/baseexternallibraryfeature.cpp" line="19"/>
        <source>Import Playlist</source>
        <translation>Importeer Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/baseexternallibraryfeature.cpp" line="92"/>
        <source>Playlist Creation Failed</source>
        <translation>Aanmaak van afspeellijst mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/baseexternallibraryfeature.cpp" line="93"/>
        <source>An unknown error occurred while creating playlist: </source>
        <translation>Er is een onbekende fout opgetreden tijdens het maken van de afspeellijst:</translation>
    </message>
</context>
<context>
    <name>BasePlaylistFeature</name>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="297"/>
        <source>New Playlist</source>
        <translation>Nieuwe Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="36"/>
        <source>Add to Auto DJ Queue (bottom)</source>
        <translation>Voeg toe aan de Auto DJ wachtrij (onderaan)</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="32"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="294"/>
        <source>Create New Playlist</source>
        <translation>Nieuwe Afspeellijst aanmaken</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="40"/>
        <source>Add to Auto DJ Queue (top)</source>
        <translation>Voeg toe aan de auto DJ wachtrij (bovenaan)</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="44"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="48"/>
        <source>Rename</source>
        <translation>Hernoemen</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="52"/>
        <source>Lock</source>
        <translation>Vergrendelen</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="56"/>
        <source>Duplicate</source>
        <translation>Dupliceren</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="60"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="64"/>
        <source>Import Playlist</source>
        <translation>Importeer Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="72"/>
        <source>Export Track Files</source>
        <translation>Exporteer Tracks</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="76"/>
        <source>Analyze entire Playlist</source>
        <translation>Analyseer volledige Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="198"/>
        <source>Enter new name for playlist:</source>
        <translation>Voer nieuwe naam in voor Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="238"/>
        <source>Duplicate Playlist</source>
        <translation>Dupliceer Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="239"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="295"/>
        <source>Enter name for new playlist:</source>
        <translation>Voer naam in voor nieuwe afspeellijst:</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="68"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="480"/>
        <source>Export Playlist</source>
        <translation>Exporteer Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="197"/>
        <source>Rename Playlist</source>
        <translation>Hernoem Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="210"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="214"/>
        <source>Renaming Playlist Failed</source>
        <translation>Afspeellijst hernoemen mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="211"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="253"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="307"/>
        <source>A playlist by that name already exists.</source>
        <translation>Een afspeellijst met deze naam bestaan reeds.</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="215"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="257"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="311"/>
        <source>A playlist cannot have a blank name.</source>
        <translation>Een Afspeellijst kan geen blanco naam bevatten.</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="242"/>
        <source>_copy</source>
        <comment>[noun]</comment>
        <extracomment>Appendix to default name when duplicating a playlist</extracomment>
        <translation>_kopiëren</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="252"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="256"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="306"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="310"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="323"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="448"/>
        <source>Playlist Creation Failed</source>
        <translation>Aanmaak van afspeellijst mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="324"/>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="449"/>
        <source>An unknown error occurred while creating playlist: </source>
        <translation>Er is een onbekende fout opgetreden tijdens het maken van de afspeellijst:</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="477"/>
        <source>M3U Playlist (*.m3u)</source>
        <translation>M3U speellijst(*.m3u)</translation>
    </message>
    <message>
        <location filename="../../src/library/baseplaylistfeature.cpp" line="482"/>
        <source>M3U Playlist (*.m3u);;M3U8 Playlist (*.m3u8);;PLS Playlist (*.pls);;Text CSV (*.csv);;Readable Text (*.txt)</source>
        <translation>M3U Speellijst (*.m3u);;M3U8 Speellijst (*.m3u8);;PLS Speellijst (*.pls);;Tekst CSV (*.csv);;Leesbare Tekst (*.txt)</translation>
    </message>
</context>
<context>
    <name>BaseSqlTableModel</name>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="64"/>
        <source>Played</source>
        <translation>Gespeeld</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="66"/>
        <source>Artist</source>
        <translation>Artiest</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="68"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="70"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="72"/>
        <source>Album Artist</source>
        <translation>Album Artiest</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="74"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="76"/>
        <source>Composer</source>
        <translation>Componist</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="78"/>
        <source>Grouping</source>
        <translation>Groeperen</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="80"/>
        <source>Year</source>
        <translation>Jaar</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="82"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="84"/>
        <source>Location</source>
        <translation>Plaats</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="86"/>
        <source>Comment</source>
        <translation>Ommerking</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="88"/>
        <source>Duration</source>
        <translation>Tijdsduur</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="90"/>
        <source>Rating</source>
        <translation>Beoordeling</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="92"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="94"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="96"/>
        <source>Track #</source>
        <translation>Track #</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="98"/>
        <source>Date Added</source>
        <translation>Datum Toegevoegd</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="100"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="102"/>
        <source>Timestamp</source>
        <translation>Tijdstempel</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="104"/>
        <source>Key</source>
        <translation>Toonaard</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="106"/>
        <source>BPM Lock</source>
        <translation>BPM vergrendeling</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="108"/>
        <source>Preview</source>
        <translation>Voorvertoning</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="110"/>
        <source>Cover Art</source>
        <translation>Cover Art</translation>
    </message>
    <message>
        <location filename="../../src/library/basesqltablemodel.cpp" line="112"/>
        <source>ReplayGain</source>
        <translation>ReplayGain</translation>
    </message>
</context>
<context>
    <name>BaseTrackPlayerImpl</name>
    <message>
        <location filename="../../src/mixer/basetrackplayer.cpp" line="281"/>
        <source>Couldn&apos;t load track.</source>
        <translation>Kan het nummer niet laden.</translation>
    </message>
</context>
<context>
    <name>BroadcastManager</name>
    <message>
        <location filename="../../src/broadcast/broadcastmanager.cpp" line="105"/>
        <source>Action failed</source>
        <translation>Actie mislukt</translation>
    </message>
    <message>
        <location filename="../../src/broadcast/broadcastmanager.cpp" line="106"/>
        <source>Please enable at least one connection to use Live Broadcasting.</source>
        <translation>Schakel ten minste één verbinding in om Live Broadcasting te gebruiken.</translation>
    </message>
</context>
<context>
    <name>BroadcastProfile</name>
    <message>
        <location filename="../../src/preferences/broadcastprofile.cpp" line="445"/>
        <source>Can&apos;t use secure password storage: keychain access failed.</source>
        <translation>Kan geen veilige paswoordopslag gebruiken: toegang tot keychain mislukt</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastprofile.cpp" line="476"/>
        <source>Secure password retrieval unsuccessful: keychain access failed.</source>
        <translation>Veilig ophalen van wachtwoord mislukt: toegang tot keychain mislukt.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastprofile.cpp" line="488"/>
        <source>Settings error</source>
        <translation>Fout bij instellingen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastprofile.cpp" line="489"/>
        <source>&lt;b&gt;Error with settings for &apos;%1&apos;:&lt;/b&gt;&lt;br&gt;</source>
        <translation>&lt;b&gt;Fout bij instellingen voor &apos;%1&apos;:&lt;/b&gt;&lt;br&gt;</translation>
    </message>
</context>
<context>
    <name>BroadcastSettingsModel</name>
    <message>
        <location filename="../../src/preferences/broadcastsettingsmodel.cpp" line="128"/>
        <source>Enabled</source>
        <translation>Ingeschakeld</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastsettingsmodel.cpp" line="130"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastsettingsmodel.cpp" line="132"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastsettingsmodel.cpp" line="178"/>
        <source>Disconnected</source>
        <translation>Verbroken</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastsettingsmodel.cpp" line="180"/>
        <source>Connecting...</source>
        <translation>Verbinden...</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastsettingsmodel.cpp" line="182"/>
        <source>Connected</source>
        <translation>Verbonden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastsettingsmodel.cpp" line="184"/>
        <source>Failed</source>
        <translation>Mislukt</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastsettingsmodel.cpp" line="187"/>
        <source>Unknown</source>
        <translation>Onbekend</translation>
    </message>
</context>
<context>
    <name>BrowseFeature</name>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="40"/>
        <source>Add to Quick Links</source>
        <translation>Toevoegen aan Quick Links</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="43"/>
        <source>Remove from Quick Links</source>
        <translation>Verwijder van Quick Links</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="46"/>
        <source>Add to Library</source>
        <translation>Toevoegen aan Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="61"/>
        <source>Quick Links</source>
        <translation>Quick Links</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="65"/>
        <location filename="../../src/library/browse/browsefeature.cpp" line="91"/>
        <source>Devices</source>
        <translation>Apparaten</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="93"/>
        <source>Removable Devices</source>
        <translation>Verwijderbare Apparaten</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="127"/>
        <location filename="../../src/library/browse/browsefeature.cpp" line="381"/>
        <source>Computer</source>
        <translation>Computer</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="160"/>
        <source>Music Directory Added</source>
        <translation>Muziekmap toegevoegd</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="161"/>
        <source>You added one or more music directories. The tracks in these directories won&apos;t be available until you rescan your library. Would you like to rescan now?</source>
        <translation>U hebt een of meer muziekmappen toegevoegd. De tracks in deze mappen zijn pas beschikbaar als u uw bibliotheek opnieuw scant. Wilt u nu opnieuw scannen?</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="165"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsefeature.cpp" line="382"/>
        <source>&quot;Computer&quot; lets you navigate, view, and load tracks from folders on your hard disk and external devices.</source>
        <translation>De optie &quot;Computer&quot; laat u toe om in mappen te navigeren, nummers te bekijken en te laden van op uw harde schijf en externe apparaten.</translation>
    </message>
</context>
<context>
    <name>BrowseTableModel</name>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="27"/>
        <source>Preview</source>
        <translation>Voorvertoning</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="28"/>
        <source>Filename</source>
        <translation>Bestandsnaam</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="29"/>
        <source>Artist</source>
        <translation>Artiest</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="30"/>
        <source>Title</source>
        <translation>titel</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="31"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="32"/>
        <source>Track #</source>
        <translation>Track #</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="33"/>
        <source>Year</source>
        <translation>Jaar</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="34"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="35"/>
        <source>Composer</source>
        <translation>Componist</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="36"/>
        <source>Comment</source>
        <translation>Ommerking</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="37"/>
        <source>Duration</source>
        <translation>Tijdsduur</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="38"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="39"/>
        <source>Key</source>
        <translation>Toonaard</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="40"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="41"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="42"/>
        <source>ReplayGain</source>
        <translation>ReplayGain</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="43"/>
        <source>Location</source>
        <translation>Plaats</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="44"/>
        <source>Album Artist</source>
        <translation>Album Artiest</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="45"/>
        <source>Grouping</source>
        <translation>Groeperen</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="46"/>
        <source>File Modified</source>
        <translation>Bestand Gewijzigd</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="47"/>
        <source>File Created</source>
        <translation>Bestand Aangemaakt</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="106"/>
        <source>Mixxx Library</source>
        <translation>Mixxx Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/library/browse/browsetablemodel.cpp" line="107"/>
        <source>Could not load the following file because it is in use by Mixxx or another application.</source>
        <translation>Kon het volgende bestand niet laden omdat het in gebruik is door Mixxx of een andere applicatie.</translation>
    </message>
</context>
<context>
    <name>BulkController</name>
    <message>
        <location filename="../../src/controllers/bulk/bulkcontroller.cpp" line="88"/>
        <source>USB Controller</source>
        <translation>USB Controller</translation>
    </message>
</context>
<context>
    <name>ControlDelegate</name>
    <message>
        <location filename="../../src/controllers/delegates/controldelegate.cpp" line="47"/>
        <source>No control chosen.</source>
        <translation>Geen controle gekozen.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/delegates/controldelegate.cpp" line="51"/>
        <source>Script: %1(%2)</source>
        <translation>Script: %1(%2)</translation>
    </message>
</context>
<context>
    <name>ControlModel</name>
    <message>
        <location filename="../../src/control/controlmodel.cpp" line="6"/>
        <source>Group</source>
        <translation>Groep</translation>
    </message>
    <message>
        <location filename="../../src/control/controlmodel.cpp" line="7"/>
        <source>Item</source>
        <translation>Item</translation>
    </message>
    <message>
        <location filename="../../src/control/controlmodel.cpp" line="8"/>
        <source>Value</source>
        <translation>Waarde</translation>
    </message>
    <message>
        <location filename="../../src/control/controlmodel.cpp" line="9"/>
        <source>Parameter</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="../../src/control/controlmodel.cpp" line="10"/>
        <source>Title</source>
        <translation>titel</translation>
    </message>
    <message>
        <location filename="../../src/control/controlmodel.cpp" line="11"/>
        <source>Description</source>
        <translation>Omschrijving</translation>
    </message>
</context>
<context>
    <name>ControlPickerMenu</name>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="17"/>
        <source>Master Output</source>
        <translation>Master Output</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="18"/>
        <source>Headphone Output</source>
        <translation>Hoofdtelefoon Output</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="19"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="175"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="182"/>
        <source>Deck %1</source>
        <translation>Deck %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="20"/>
        <source>Sampler %1</source>
        <translation>Sampler %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="21"/>
        <source>Preview Deck %1</source>
        <translation>Preview Deck %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="22"/>
        <source>Microphone %1</source>
        <translation>Microfoon %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="23"/>
        <source>Auxiliary %1</source>
        <translation>Auxiliary %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="24"/>
        <source>Reset to default</source>
        <translation>Reset naar standaard</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="25"/>
        <source>Effect Rack %1</source>
        <translation>Effect paneel %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="28"/>
        <source>Parameter %1</source>
        <translation>Parameter %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="32"/>
        <source>Mixer</source>
        <translation>Mixer</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="33"/>
        <source>Crossfader</source>
        <translation>Crossfader</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="35"/>
        <source>Master balance</source>
        <translation>Master balance</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="36"/>
        <source>Master delay</source>
        <translation>Master vertraging</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="38"/>
        <source>Headphone mix (pre/main)</source>
        <translation>Hoofdtelefoon mix (pre/main)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="39"/>
        <source>Toggle headphone split cueing</source>
        <translation>Schakel tussen gesplitst geluid</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="40"/>
        <source>Headphone delay</source>
        <translation>Hoofdtelefoon vertraging</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="51"/>
        <source>Transport</source>
        <translation>Transport</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="57"/>
        <source>Strip-search through track</source>
        <translation>Track grondig doorzoeken</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="52"/>
        <source>Play button</source>
        <translation>Afspeelknop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="69"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="722"/>
        <source>Set to full volume</source>
        <translation>Zet naar maximale volume</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="70"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="726"/>
        <source>Set to zero volume</source>
        <translation>Zet naar nul volume</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="64"/>
        <source>Stop button</source>
        <translation>Stopknop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="63"/>
        <source>Jump to start of track and play</source>
        <translation>Spring naar begin van track en speel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="67"/>
        <source>Jump to end of track</source>
        <translation>Spring naar einde van track</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="60"/>
        <source>Reverse roll (Censor) button</source>
        <translation>Reverse roll (Censor) knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="751"/>
        <source>Headphone listen button</source>
        <translation>Luisterknop hoofdtelefoon</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="72"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="730"/>
        <source>Mute button</source>
        <translation>Stilteknop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="75"/>
        <source>Toggle repeat mode</source>
        <translation>Activeer/deactiveer herhaalmodus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="73"/>
        <source>Eject track</source>
        <translation>Track uitwerpen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="42"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="734"/>
        <source>Mix orientation (e.g. left, right, center)</source>
        <translation>Orientatie van de mix (b.v. links, rechts, midden)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="44"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="738"/>
        <source>Set mix orientation to left</source>
        <translation>Orientatie mix links</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="46"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="743"/>
        <source>Set mix orientation to center</source>
        <translation>Orientatie mix midden</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="48"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="747"/>
        <source>Set mix orientation to right</source>
        <translation>Orientatie mix rechts</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="76"/>
        <source>Toggle slip mode</source>
        <translation>Slip mode aan/uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="79"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="80"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="81"/>
        <source>Increase BPM by 1</source>
        <translation>Verhoog BPM met 1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="82"/>
        <source>Decrease BPM by 1</source>
        <translation>Verlaag BPM met 1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="83"/>
        <source>Increase BPM by 0.1</source>
        <translation>Verhoog BPM met 0.1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="84"/>
        <source>Decrease BPM by 0.1</source>
        <translation>Verlaag BPM met 0.1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="85"/>
        <source>BPM tap button</source>
        <translation>BPM tikknop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="94"/>
        <source>Toggle quantize mode</source>
        <translation>Quantize mode aan/uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="104"/>
        <source>Increase internal master BPM by 1</source>
        <translation>Verhoog interne master BPM met 1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="107"/>
        <source>Decrease internal master BPM by 1</source>
        <translation>Verlaag interne master BPM met 1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="110"/>
        <source>Increase internal master BPM by 0.1</source>
        <translation>Verhoog interne master BPM met 0.1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="112"/>
        <source>Decrease internal master BPM by 0.1</source>
        <translation>Verlaag interne master BPM met 0.1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="113"/>
        <source>Toggle sync master</source>
        <translation>Sync master aan/uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="115"/>
        <source>Sync mode 3-state toggle (OFF, FOLLOWER, MASTER)</source>
        <translation>3-standenschakelaar voor synchronisatiemodus  (UIT, VOLG, MASTER)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="119"/>
        <source>One-time beat sync (tempo only)</source>
        <translation>Eenmalige beat-synchronisatie (alleen tempo)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="121"/>
        <source>One-time beat sync (phase only)</source>
        <translation>Eenmalige beat-synchronisatie (alleen fase)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="126"/>
        <source>Toggle keylock mode</source>
        <translation>Key-lock mode aan/uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="153"/>
        <source>Equalizers</source>
        <translation>Equalizers</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="189"/>
        <source>Vinyl Control</source>
        <translation>Vinyl Control</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="195"/>
        <source>Toggle vinyl-control cueing mode (OFF/ONE/HOT)</source>
        <translation>Wisselknop cueing mode met vinylregeling (UIT / EEN / HOT)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="193"/>
        <source>Toggle vinyl-control mode (ABS/REL/CONST)</source>
        <translation>Wisselknop vinyl-control modus (ABS/REL/CONST)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="197"/>
        <source>Pass through external audio into the internal mixer</source>
        <translation>Via externe audio naar de interne mixer doorgeven</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="202"/>
        <source>Cues</source>
        <translation>Cues</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="203"/>
        <source>Cue button</source>
        <translation>Cue knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="204"/>
        <source>Set cue point</source>
        <translation>Stel het cue-punt in</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="205"/>
        <source>Go to cue point</source>
        <translation>Ga naar het cue-punt</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="207"/>
        <source>Go to cue point and play</source>
        <translation>Ga naar het cue-punt en speel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="209"/>
        <source>Go to cue point and stop</source>
        <translation>Ga naar het cue-punt en stop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="211"/>
        <source>Preview from cue point</source>
        <translation>Voorbeluisteren vanaf het cue-punt</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="213"/>
        <source>Cue button (CDJ mode)</source>
        <translation>Cue-knop (CDJ modus)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="215"/>
        <source>Stutter cue</source>
        <translation>Stutter cue</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="220"/>
        <source>Hotcues</source>
        <translation>Hotcues</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="228"/>
        <source>Set, preview from or jump to hotcue %1</source>
        <translation>Stel in, voorbeluisteren van of spring naar hotcue% 1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="229"/>
        <source>Clear hotcue %1</source>
        <translation>Wis hotcue %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="230"/>
        <source>Set hotcue %1</source>
        <translation>Stel hotcue %1 in.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="231"/>
        <source>Jump to hotcue %1</source>
        <translation>Spring naar hotcue %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="232"/>
        <source>Jump to hotcue %1 and stop</source>
        <translation>Spring naar hotcue %1 en stop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="233"/>
        <source>Jump to hotcue %1 and play</source>
        <translation>Spring naar hotcue %1 en speel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="234"/>
        <source>Preview from hotcue %1</source>
        <translation>Voorbeluisteren vanaf hotcue %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="221"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="236"/>
        <source>Hotcue %1</source>
        <translation>Hotcue %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="268"/>
        <source>Looping</source>
        <translation>Looping</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="276"/>
        <source>Loop In button</source>
        <translation>Loop In knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="277"/>
        <source>Loop Out button</source>
        <translation>Loop Out knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="278"/>
        <source>Loop Exit button</source>
        <translation>Loop Exit knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="293"/>
        <source>1/2</source>
        <translation>1/2</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="294"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="295"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="296"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="297"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="298"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="299"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="300"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="305"/>
        <source>Move loop forward by %1 beats</source>
        <translation>Verplaats loop voorwaarts met %1 beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="306"/>
        <source>Move loop backward by %1 beats</source>
        <translation>Verplaats loop achteruit met %1 beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="323"/>
        <source>Beat-Looping</source>
        <translation>beat-Looping</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="326"/>
        <source>Create %1-beat loop</source>
        <translation>Maak %1-beat loop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="327"/>
        <source>Create temporary %1-beat loop roll</source>
        <translation>Maak een tijdelijke % 1-beat loop roll</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="29"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="369"/>
        <source>Library</source>
        <translation>Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="26"/>
        <source>Unit %1</source>
        <translation>Eenheid %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="27"/>
        <source>Slot %1</source>
        <translation>Slot %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="33"/>
        <source>Master crossfader</source>
        <translation>Master crossfader</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="35"/>
        <source>Master Balance</source>
        <translation>Master Balance</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="36"/>
        <source>Master Delay</source>
        <translation>Master Delay</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="38"/>
        <source>Headphone Mix</source>
        <translation>Hoofdtelefoon Mix</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="39"/>
        <source>Headphone Split Cue</source>
        <translation>Hoofdtelefoon Split Cue</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="40"/>
        <source>Headphone Delay</source>
        <translation>Hoofdtelefoon Delay</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="52"/>
        <source>Play</source>
        <translation>Speel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="54"/>
        <source>Fast Rewind</source>
        <translation>Snel Terugspoelen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="54"/>
        <source>Fast Rewind button</source>
        <translation>Snelterugspoel-knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="55"/>
        <source>Fast Forward</source>
        <translation>Snel Voorwaarts</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="55"/>
        <source>Fast Forward button</source>
        <translation>Snelvoorwaarts-knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="56"/>
        <source>Strip Search</source>
        <translation>Grondig zoeken</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="58"/>
        <source>Play Reverse</source>
        <translation>Speel achteruit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="58"/>
        <source>Play Reverse button</source>
        <translation>Achteruitspeel-knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="59"/>
        <source>Reverse Roll (Censor)</source>
        <translation>Reverse Roll (Censor)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="61"/>
        <source>Jump To Start</source>
        <translation>Spring naar begin</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="61"/>
        <source>Jumps to start of track</source>
        <translation>Spring naar begin van de track</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="62"/>
        <source>Play From Start</source>
        <translation>Speel vanaf begin</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="64"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="65"/>
        <source>Stop And Jump To Start</source>
        <translation>Stop en spring naar begin</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="66"/>
        <source>Stop playback and jump to start of track</source>
        <translation>Stop afspelen en spring naar begin</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="67"/>
        <source>Jump To End</source>
        <translation>Spring naar einde</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="68"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="68"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="717"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="718"/>
        <source>Volume Fader</source>
        <translation>Volume Regelaar</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="69"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="721"/>
        <source>Full Volume</source>
        <translation>Volledig Volume</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="70"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="725"/>
        <source>Zero Volume</source>
        <translation>Geen Volume</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="71"/>
        <source>Track Gain</source>
        <translation>Track Gain</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="71"/>
        <source>Track Gain knob</source>
        <translation>Track Gain knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="72"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="729"/>
        <source>Mute</source>
        <translation>Dempen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="73"/>
        <source>Eject</source>
        <translation>Uitwerpen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="74"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="750"/>
        <source>Headphone Listen</source>
        <translation>Hoofdtelefoon Luisteren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="74"/>
        <source>Headphone listen (pfl) button</source>
        <translation>Hoofdtelefoon Luisteren (pfl) knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="75"/>
        <source>Repeat Mode</source>
        <translation>Herhaal Modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="76"/>
        <source>Slip Mode</source>
        <translation>Slip Modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="41"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="733"/>
        <source>Orientation</source>
        <translation>Positionering</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="43"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="737"/>
        <source>Orient Left</source>
        <translation>Positioneer Links</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="45"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="742"/>
        <source>Orient Center</source>
        <translation>Positioneer Midden</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="47"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="746"/>
        <source>Orient Right</source>
        <translation>Positioneer Rechts</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="81"/>
        <source>BPM +1</source>
        <translation>BPM +1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="82"/>
        <source>BPM -1</source>
        <translation>BPM -1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="83"/>
        <source>BPM +0.1</source>
        <translation>BPM +0.1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="84"/>
        <source>BPM -0.1</source>
        <translation>BPM -0.1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="85"/>
        <source>BPM Tap</source>
        <translation>BPM Tik</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="86"/>
        <source>Adjust Beatgrid Faster +.01</source>
        <translation>Pas Beatraster aan naar Sneller +.01</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="86"/>
        <source>Increase track&apos;s average BPM by 0.01</source>
        <translation>Verhoog de track&apos;s gemiddelde BPM met 0.01</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="87"/>
        <source>Adjust Beatgrid Slower -.01</source>
        <translation>Pas Beat-raster aan naar Trager -.01</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="87"/>
        <source>Decrease track&apos;s average BPM by 0.01</source>
        <translation>Verlaag de track&apos;s gemiddelde BPM met 0.01</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="88"/>
        <source>Move Beatgrid Earlier</source>
        <translation>Verplaats Beatgrid naar eerder</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="88"/>
        <source>Adjust the beatgrid to the left</source>
        <translation>Pas het beat-raster naar links aan</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="89"/>
        <source>Move Beatgrid Later</source>
        <translation>Verplaats Beat-raster naar later</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="89"/>
        <source>Adjust the beatgrid to the right</source>
        <translation>Pas het beat-raster naar rechts aan</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="90"/>
        <source>Adjust Beatgrid</source>
        <translation>Pas Beat-raster aan</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="91"/>
        <source>Align beatgrid to current position</source>
        <translation>Lijn beat-raster uit met huidige positie</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="92"/>
        <source>Adjust Beatgrid - Match Alignment</source>
        <translation>Pas Beat-raster aan - Overeenkomst Uitlijning</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="93"/>
        <source>Adjust beatgrid to match another playing deck.</source>
        <translation>Pas Beat-raster overeenkomstig aan andere speler</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="94"/>
        <source>Quantize Mode</source>
        <translation>Quantize modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="96"/>
        <source>Sync</source>
        <translation>Sync</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="97"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="114"/>
        <source>Sync Mode</source>
        <translation>Sync Modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="99"/>
        <source>Internal Sync Master</source>
        <translation>Interne Sync Master</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="100"/>
        <source>Toggle Internal Sync Master</source>
        <translation>Interne Sync Master aan/uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="101"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="102"/>
        <source>Internal Master BPM</source>
        <translation>Interne Master BPM</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="103"/>
        <source>Internal Master BPM +1</source>
        <translation>Interne Master BPM +1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="106"/>
        <source>Internal Master BPM -1</source>
        <translation>Interne Master BPM -1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="109"/>
        <source>Internal Master BPM +0.1</source>
        <translation>Interne Master BPM +0.1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="111"/>
        <source>Internal Master BPM -0.1</source>
        <translation>Interne Master BPM -0.1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="113"/>
        <source>Sync Master</source>
        <translation>Sync Master</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="116"/>
        <source>Beat Sync One-Shot</source>
        <translation>Eenmalige beat-synchronisatie</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="118"/>
        <source>Sync Tempo One-Shot</source>
        <translation>Synchroniseer tempo eenmalig</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="120"/>
        <source>Sync Phase One-Shot</source>
        <translation>Synchroniseer faze eenmalig</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="124"/>
        <source>Speed (Pitch/Tempo)</source>
        <translation>Snelheid (Pitch/Tempo)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="125"/>
        <source>Keylock Mode</source>
        <translation>Keylock Modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="130"/>
        <source>Pitch control (does not affect tempo), center is original pitch</source>
        <translation>Pitch controle (geen invloed op tempo), midden is de originele pitch</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="131"/>
        <source>Pitch Adjust</source>
        <translation>Aanpassen Pitch</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="132"/>
        <source>Adjust pitch from speed slider pitch</source>
        <translation>Pas pitch aan met de pitch schuifregelaar</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="133"/>
        <source>Match musical key</source>
        <translation>Muziekale key  overeenstemmen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="133"/>
        <source>Match Key</source>
        <translation>Key overeenstemmen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="134"/>
        <source>Reset Key</source>
        <translation>Key resetten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="134"/>
        <source>Resets key to original</source>
        <translation>Key resetten naar origineel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="164"/>
        <source>High EQ</source>
        <translation>High EQ</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="163"/>
        <source>Mid EQ</source>
        <translation>Mid EQ</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="162"/>
        <source>Low EQ</source>
        <translation>Low EQ</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="190"/>
        <source>Toggle Vinyl Control</source>
        <translation>Vinyl Control aan/uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="191"/>
        <source>Toggle Vinyl Control (ON/OFF)</source>
        <translation>Wissel Vinyl Control (AAN/UIT)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="192"/>
        <source>Vinyl Control Mode</source>
        <translation>Vinyl Control Modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="194"/>
        <source>Vinyl Control Cueing Mode</source>
        <translation>Vinyl Control Cueing Modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="196"/>
        <source>Vinyl Control Passthrough</source>
        <translation>Vinylbediening doorvoeren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="198"/>
        <source>Vinyl Control Next Deck</source>
        <translation>Vinylbediening Volgende Speler</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="199"/>
        <source>Single deck mode - Switch vinyl control to next deck</source>
        <translation>Enkelvoudige Speles modus - Wissel vinylbediening naar volgende speler</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="203"/>
        <source>Cue</source>
        <translation>Verwijzing</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="204"/>
        <source>Set Cue</source>
        <translation>Verwijzing instellen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="205"/>
        <source>Go-To Cue</source>
        <translation>Ga Naar verwijzing</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="206"/>
        <source>Go-To Cue And Play</source>
        <translation>Ga Naar verwijzing en Speel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="208"/>
        <source>Go-To Cue And Stop</source>
        <translation>Ga Naar verwijzing en Stop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="210"/>
        <source>Preview Cue</source>
        <translation>Verwijzing voorbeluisteren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="212"/>
        <source>Cue (CDJ Mode)</source>
        <translation>Verwijzing (CDJ Modus)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="214"/>
        <source>Stutter Cue</source>
        <translation>Voeg de geselecteerde track vooraan toe in de AutoDJ wachtrij</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="217"/>
        <source>Go to cue point and play after release</source>
        <translation>Ga Naar het verwijzingspunt en speel na het loslaten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="222"/>
        <source>Clear Hotcue %1</source>
        <translation>Wis Hotcue %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="223"/>
        <source>Set Hotcue %1</source>
        <translation>Stel Hotcue %1 in</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="224"/>
        <source>Jump To Hotcue %1</source>
        <translation>Spring naar Hotcue %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="225"/>
        <source>Jump To Hotcue %1 And Stop</source>
        <translation>Spring naar Hotcue %1 en Stop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="226"/>
        <source>Jump To Hotcue %1 And Play</source>
        <translation>Spring naar Hotcue %1 en Speel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="227"/>
        <source>Preview Hotcue %1</source>
        <translation>Hotcue %1 voorbeluisteren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="276"/>
        <source>Loop In</source>
        <translation>Loop In</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="277"/>
        <source>Loop Out</source>
        <translation>Loop Out</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="278"/>
        <source>Loop Exit</source>
        <translation>Verlaat Loop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="279"/>
        <source>Reloop/Exit Loop</source>
        <translation>Reloop/Verlaat Loop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="281"/>
        <source>Loop Halve</source>
        <translation>Halve Loop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="282"/>
        <source>Loop Double</source>
        <translation>Dubbele Loop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="289"/>
        <source>1/32</source>
        <translation>1/32</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="290"/>
        <source>1/16</source>
        <translation>1/16</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="291"/>
        <source>1/8</source>
        <translation>1/8</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="292"/>
        <source>1/4</source>
        <translation>1/4</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="303"/>
        <source>Move Loop +%1 Beats</source>
        <translation>Verplaats Loop +%1 Beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="304"/>
        <source>Move Loop -%1 Beats</source>
        <translation>Verplaats Loop -%1 Beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="324"/>
        <source>Loop %1 Beats</source>
        <translation>Loop %1 Beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="325"/>
        <source>Loop Roll %1 Beats</source>
        <translation>Loop lus %1 Beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="423"/>
        <source>Add to Auto DJ Queue (bottom)</source>
        <translation>Voeg toe aan de Auto DJ wachtrij (onderaan)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="424"/>
        <source>Append the selected track to the Auto DJ Queue</source>
        <translation>Voeg de geselecteerde track toe aan de Auto DJ wachtrij</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="427"/>
        <source>Add to Auto DJ Queue (top)</source>
        <translation>Voeg toe aan de auto DJ wachtrij (bovenaan)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="428"/>
        <source>Prepend selected track to the Auto DJ Queue</source>
        <translation>Voeg de geselecteerde track vooraan toe in de AutoDJ wachtrij</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="438"/>
        <source>Load Track</source>
        <translation>Track Laden</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="439"/>
        <source>Load selected track</source>
        <translation>Geselecteerde tracks laden</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="440"/>
        <source>Track Load and Play</source>
        <translation>Laad track en speel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="441"/>
        <source>Load selected track and play</source>
        <translation>Laad geselecteerde track en speel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="444"/>
        <source>Record Mix</source>
        <translation>Record Mix</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="445"/>
        <source>Toggle mix recording</source>
        <translation>Wissel Opname Mix</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="449"/>
        <source>Effects</source>
        <translation>Effecten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="452"/>
        <source>Quick Effects</source>
        <translation>Snelle Effecten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="456"/>
        <source>Deck %1 Quick Effect Super Knob</source>
        <translation>Superknop snelle effecten speler %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="457"/>
        <source>Quick Effect Super Knob (control linked effect parameters)</source>
        <translation>Superknop Snelle Effecten (bedien gekoppelde effectparameters)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="458"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="464"/>
        <source>Quick Effect</source>
        <translation>Snel Effect</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="476"/>
        <source>Clear effect rack</source>
        <translation> Wis effecten rack</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="476"/>
        <source>Clear Effect Rack</source>
        <translation>Wis Effecten Rack</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="493"/>
        <source>Clear Unit</source>
        <translation>Wis Eenheid</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="494"/>
        <source>Clear effect unit</source>
        <translation>Wis effecten eenheid</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="497"/>
        <source>Toggle Unit</source>
        <translation>Wissel Eenheid</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="501"/>
        <source>Dry/Wet</source>
        <translation>Droog/Nat</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="502"/>
        <source>Adjust the balance between the original (dry) and processed (wet) signal.</source>
        <translation>Pas de balans tussen het originele (droge) en verwerkte (natte) signaal aan.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="505"/>
        <source>Super Knob</source>
        <translation>Super knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="515"/>
        <source>Next Chain</source>
        <translation>Volgende Schakel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="557"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="558"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="572"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="573"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="587"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="588"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="601"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="602"/>
        <source>Assign </source>
        <translation>Toewijzen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="624"/>
        <source>Clear</source>
        <translation>Wissen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="624"/>
        <source>Clear the current effect</source>
        <translation>Wis het huidige effect</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="632"/>
        <source>Toggle</source>
        <translation>Omschakelen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="632"/>
        <source>Toggle the current effect</source>
        <translation>Huidig effect omschakelen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="636"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="636"/>
        <source>Switch to next effect</source>
        <translation>Schakel naar volgend effect</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="640"/>
        <source>Previous</source>
        <translation>Vorige</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="640"/>
        <source>Switch to the previous effect</source>
        <translation>Schakel naar vorig effect</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="644"/>
        <source>Next or Previous</source>
        <translation>Volgende of Vorige</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="645"/>
        <source>Switch to either next or previous effect</source>
        <translation>Schakel ofwel naar volgende of vorig effect</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="671"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="672"/>
        <source>Parameter Value</source>
        <translation>Parameter Waarde</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="700"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="701"/>
        <source>Microphone Ducking Strength</source>
        <translation>Onderdrukkingssterkte Microfoon</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="704"/>
        <source>Microphone Ducking Mode</source>
        <translation>Onderdrukkingsmodus Microfoon</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="713"/>
        <source>Gain</source>
        <translation>Versterken</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="714"/>
        <source>Gain knob</source>
        <translation>Knop Versterken</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="758"/>
        <source>Shuffle the content of the Auto DJ queue</source>
        <translation>Schud de inhoud van de Auto DJ-wachtrij</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="761"/>
        <source>Skip the next track in the Auto DJ queue</source>
        <translation>Het volgende nummer overslaan in de Auto DJ-wachtrij</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="766"/>
        <source>Auto DJ Toggle</source>
        <translation>Auto DJ omschakelen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="767"/>
        <source>Toggle Auto DJ On/Off</source>
        <translation>Schakel Auto DJ Aan/Uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="790"/>
        <source>Library Maximize/Restore</source>
        <translation>Bibliotheek Maximaliseren/Herstellen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="791"/>
        <source>Maximize the track library to take up all the available screen space.</source>
        <translation>Maximaliseer de trackbibliotheek in de beschikbare schermruimte.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="784"/>
        <source>Effect Rack Show/Hide</source>
        <translation>Effeceten rack Tonen/Verbergen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="785"/>
        <source>Show/hide the effect rack</source>
        <translation>Toon/Verberg het effecten rack</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="787"/>
        <source>Cover Art Show/Hide</source>
        <translation>Hoes Tonen/Verbergen </translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="788"/>
        <source>Show/hide cover art</source>
        <translation>Toon/Verberg hoes</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="804"/>
        <source>Waveform Zoom Out</source>
        <translation>Waveform Uitzoomen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="531"/>
        <source>Toggle Effect Unit</source>
        <translation>Schakel Effecteneenheid om</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="34"/>
        <source>Master Gain</source>
        <translation>Hoofduitgang Versterking</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="34"/>
        <source>Master gain</source>
        <translation>Hoofduitgang versterking</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="37"/>
        <source>Headphone Gain</source>
        <translation>Hoofdtelefoon Versterking</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="37"/>
        <source>Headphone gain</source>
        <translation>Hoofdtelefoon versterking</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="98"/>
        <source>Tap to sync tempo (and phase with quantize enabled), hold to enable permanent sync</source>
        <translation>Tik om het tempo te synchroniseren (en fase met quantize ingeschakeld), houd ingedrukt om permanente synchronisatie in te schakelen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="117"/>
        <source>One-time beat sync tempo (and phase with quantize enabled)</source>
        <translation>Eenmalig beat-synchronisatietempo (en fase met quantize ingeschakeld)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="127"/>
        <source>Playback Speed</source>
        <translation>Afspeelsnelheid</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="128"/>
        <source>Playback speed control (Vinyl &quot;Pitch&quot; slider)</source>
        <translation>Bediening afspeelsnelheid (Vinyl &quot;Pitch&quot; regelaar)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="129"/>
        <source>Pitch (Musical key)</source>
        <translation>Pitch (Toonhoogte)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="135"/>
        <source>Increase Speed</source>
        <translation>Snelheid verhogen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="136"/>
        <source>Adjust speed faster (coarse)</source>
        <translation>Snelheid hoger bijstellen (grof)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="137"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="141"/>
        <source>Increase Speed (Fine)</source>
        <translation>Verhoog snelheid (fijn)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="138"/>
        <source>Adjust speed faster (fine)</source>
        <translation>Snelheid hoger bijstellen (fijn)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="139"/>
        <source>Decrease Speed</source>
        <translation>Snelheid verlagen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="140"/>
        <source>Adjust speed slower (coarse)</source>
        <translation>Snelheid lager bijstellen (grof)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="142"/>
        <source>Adjust speed slower (fine)</source>
        <translation>Snelheid lager bijstellen (fijn)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="143"/>
        <source>Temporarily Increase Speed</source>
        <translation>Snelheid tijdelijk verhogen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="144"/>
        <source>Temporarily increase speed (coarse)</source>
        <translation>Snelheid tijdelijk verhogen (grof)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="145"/>
        <source>Temporarily Increase Speed (Fine)</source>
        <translation>Snelheid tijdelijk verhogen (fijn)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="146"/>
        <source>Temporarily increase speed (fine)</source>
        <translation>Snelheid tijdelijk verhogen (fijn)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="147"/>
        <source>Temporarily Decrease Speed</source>
        <translation>Snelheid Tijdelijk Verlagen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="148"/>
        <source>Temporarily decrease speed (coarse)</source>
        <translation>Snelheid tijdelijk verlagen (grof)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="149"/>
        <source>Temporarily Decrease Speed (Fine)</source>
        <translation>Snelheid Tijdelijk Verlagen (Fijn)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="150"/>
        <source>Temporarily decrease speed (fine)</source>
        <translation>Snelheid tijdelijk verlagen (fijn)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="173"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="174"/>
        <source>Adjust %1</source>
        <translation>Bijstellen %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="180"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="181"/>
        <source>Kill %1</source>
        <translation>Dempen %1</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="216"/>
        <source>CUP (Cue + Play)</source>
        <translation>CUP (Cue + Afspelen)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="271"/>
        <source>Loop Selected Beats</source>
        <translation>Loop Geselecteerde Beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="272"/>
        <source>Create a beat loop of selected beat size</source>
        <translation>Maak een beat loop van de geselecteerde beatgrootte</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="273"/>
        <source>Loop Roll Selected Beats</source>
        <translation>Loop Roll van geselecteerde beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="274"/>
        <source>Create a rolling beat loop of selected beat size</source>
        <translation>Maak een rollende beat loop van de geselecteerde beatgrootte</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="279"/>
        <source>Toggle loop on/off and jump to Loop In point if loop is behind play position</source>
        <translation>Schakel het loopen aan/uit en spring naar het Loop-In punt als de loop zich achter de afspeelpositie bevindt</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="280"/>
        <source>Reloop And Stop</source>
        <translation>Reloop En Stop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="280"/>
        <source>Enable loop, jump to Loop In point, and stop</source>
        <translation>Schakel het loopen in, spring naar Loop-In punt en stop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="281"/>
        <source>Halve the loop length</source>
        <translation>Halveer de Loop lengte</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="282"/>
        <source>Double the loop length</source>
        <translation>Verdubbel de Loop lengte</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="346"/>
        <source>Beat Jump / Loop Move</source>
        <translation>Beat Jump / Loop Move</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="347"/>
        <source>Jump / Move Loop Forward %1 Beats</source>
        <translation>Spring / verplaats Loop vooruit% 1 Beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="348"/>
        <source>Jump / Move Loop Backward %1 Beats</source>
        <translation>Spring / verplaats Loop achteruit% 1 Beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="349"/>
        <source>Jump forward by %1 beats, or if a loop is enabled, move the loop forward %1 beats</source>
        <translation>Spring vooruit met %1 beats, of als een loop is ingeschakeld, verplaats de loop naar voren met %1 beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="350"/>
        <source>Jump backward by %1 beats, or if a loop is enabled, move the loop backward %1 beats</source>
        <translation>Spring achteruit met %1 slagen, of als een loop is ingeschakeld, verplaats de loop achteruit %1 beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="351"/>
        <source>Beat Jump / Loop Move Forward Selected Beats</source>
        <translation>Beat Jump / Loop Move Geselecteerde Beats Voorwaarts</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="351"/>
        <source>Jump forward by the selected number of beats, or if a loop is enabled, move the loop forward by the selected number of beats</source>
        <translation>Spring vooruit met het aantal geselecteerde beats, of als een loop is ingeschakeld, verplaats de loop naar voren met het aantal geselecteerde beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="352"/>
        <source>Beat Jump / Loop Move Backward Selected Beats</source>
        <translation>Beat Jump / Loop Move Geselecteerde Beats Achteruit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="352"/>
        <source>Jump backward by the selected number of beats, or if a loop is enabled, move the loop backward by the selected number of beats</source>
        <translation>Spring achteruit met het aantal geselecteerde beats, of als een loop is ingeschakeld, verplaats de loop achteruit met het aantal geselecteerde beats</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="371"/>
        <source>Move up</source>
        <translation>Omhoog</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="372"/>
        <source>Equivalent to pressing the UP key on the keyboard</source>
        <translation>Vergelijkbaar met het indrukken van de PgUp-toets op het toetsenbord</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="375"/>
        <source>Move down</source>
        <translation>Omlaag</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="376"/>
        <source>Equivalent to pressing the DOWN key on the keyboard</source>
        <translation>Vergelijkbaar met het indrukken van de PgDn-toets op het toetsenbord</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="379"/>
        <source>Move up/down</source>
        <translation>Omhoog/Omlaag</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="380"/>
        <source>Move vertically in either direction using a knob, as if pressing UP/DOWN keys</source>
        <translation>Beweeg verticaal in beide richtingen met een knop, alsof u op de toetsen OMHOOG / OMLAAG drukt</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="383"/>
        <source>Scroll Up</source>
        <translation>Naar Boven Scrollen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="384"/>
        <source>Equivalent to pressing the PAGE UP key on the keyboard</source>
        <translation>Vergelijkbaar met het indrukken van de PgUp-toets op het toetsenbord</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="387"/>
        <source>Scroll Down</source>
        <translation>Naar Beneden Scrollen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="388"/>
        <source>Equivalent to pressing the PAGE DOWN key on the keyboard</source>
        <translation>Vergelijkbaar met het indrukken van de PAGE DOWN-toets op het toetsenbord</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="391"/>
        <source>Scroll up/down</source>
        <translation>Scroll omhoog/omlaag</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="392"/>
        <source>Scroll vertically in either direction using a knob, as if pressing PGUP/PGDOWN keys</source>
        <translation>Beweeg verticaal in beide richtingen met een knop, alsof u de toetsen PGUP / PGDOWN indrukt</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="395"/>
        <source>Move left</source>
        <translation>Naar links</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="396"/>
        <source>Equivalent to pressing the LEFT key on the keyboard</source>
        <translation>Vergelijkbaar met het indrukken van de LEFT-toets op het toetsenbord</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="399"/>
        <source>Move right</source>
        <translation>Naar rechts</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="400"/>
        <source>Equivalent to pressing the RIGHT key on the keyboard</source>
        <translation>Vergelijkbaar met het indrukken van de RIGHT-toets op het toetsenbord</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="403"/>
        <source>Move left/right</source>
        <translation>Ga naar links/rechts</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="404"/>
        <source>Move horizontally in either direction using a knob, as if pressing LEFT/RIGHT keys</source>
        <translation>Beweeg horizontaal in beide richtingen met een knop, alsof u de toetsen LINKS / RECHTS indrukt</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="407"/>
        <source>Move focus to right pane</source>
        <translation>Verplaats de focus naar het rechter deelvenster</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="408"/>
        <source>Equivalent to pressing the TAB key on the keyboard</source>
        <translation>Vergelijkbaar met het indrukken van de TAB-toets op het toetsenbord.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="411"/>
        <source>Move focus to left pane</source>
        <translation>Verplaats de focus naar het linker deelvenster.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="412"/>
        <source>Equivalent to pressing the SHIFT+TAB key on the keyboard</source>
        <translation>Vergelijkbaar met het indrukken van de SHIFT+TAB toetsen op het toetsenbord</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="415"/>
        <source>Move focus to right/left pane</source>
        <translation>Verplaats de focus naar het rechter / linker deelvenster</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="416"/>
        <source>Move focus one pane to right or left using a knob, as if pressing TAB/SHIFT+TAB keys</source>
        <translation>Verplaats de focus één deelvenster naar rechts of links met een knop, alsof u op de TAB / SHIFT + TAB-toetsen drukt</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="419"/>
        <source>Go to the currently selected item</source>
        <translation>Ga naar het huidig geselecteerde item.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="420"/>
        <source>Choose the currently selected item and advance forward one pane if appropriate</source>
        <translation>Kies het momenteel geselecteerde item en ga indien nodig één venstergedeelte vooruit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="431"/>
        <source>Add to Auto DJ Queue (replace)</source>
        <translation>Voeg toe aan de Auto DJ wachtrij (vervang)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="432"/>
        <source>Replace Auto DJ Queue with selected tracks</source>
        <translation>Vervang de Auto DJ wachtrij met de geselecteerde tracks</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="462"/>
        <source>Deck %1 Quick Effect Enable Button</source>
        <translation>Speler %1 Snel Effect Inschakelknop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="463"/>
        <source>Quick Effect Enable Button</source>
        <translation>Snel Effect Inschakelknop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="498"/>
        <source>Enable or disable effect processing</source>
        <translation>Effectverwerking in- of uitschakelen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="506"/>
        <source>Super Knob (control effects&apos; Meta Knobs)</source>
        <translation>Super Knop (heeft controle over de Meta Knoppen effecten)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="510"/>
        <source>Mix Mode Toggle</source>
        <translation>Mix Modus Schakelen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="511"/>
        <source>Toggle effect unit between D/W and D+W modes</source>
        <translation>Wissel effecteenheid tussen de modi D/W en D+W</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="516"/>
        <source>Next chain preset</source>
        <translation>Volgende vooringestelde keten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="519"/>
        <source>Previous Chain</source>
        <translation>Vorige keten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="520"/>
        <source>Previous chain preset</source>
        <translation>Vorige vooringestelde keten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="523"/>
        <source>Next/Previous Chain</source>
        <translation>Volgende/Vorige Keten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="524"/>
        <source>Next or previous chain preset</source>
        <translation>Volgende of vorige vooringestelde keten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="527"/>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="528"/>
        <source>Show Effect Parameters</source>
        <translation>Toon Effect Parameters</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="628"/>
        <source>Meta Knob</source>
        <translation>Meta Knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="628"/>
        <source>Effect Meta Knob (control linked effect parameters)</source>
        <translation>Effect Meta Knop (controleert gelinkte effect parameters)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="677"/>
        <source>Meta Knob Mode</source>
        <translation>Meta Knop Modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="678"/>
        <source>Set how linked effect parameters change when turning the Meta Knob.</source>
        <translation>Stel in hoe gekoppelde effectparameters veranderen bij het draaien van de Meta Knop.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="682"/>
        <source>Meta Knob Mode Invert</source>
        <translation>Meta Knop Modus Omkeren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="683"/>
        <source>Invert how linked effect parameters change when turning the Meta Knob.</source>
        <translation>Keer om hoe gekoppelde effectparameters veranderen wanneer u aan de Meta-knop draait.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="693"/>
        <source>Microphone / Auxiliary</source>
        <translation>Microfoon / Aux</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="696"/>
        <source>Microphone On/Off</source>
        <translation>Microfoon Aan/Uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="697"/>
        <source>Microphone on/off</source>
        <translation>Microfoon aan/uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="705"/>
        <source>Toggle microphone ducking mode (OFF, AUTO, MANUAL)</source>
        <translation>Wisselen van onderdrukkingsmodus (UIT, AUTO, MANUEEL)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="708"/>
        <source>Auxiliary On/Off</source>
        <translation>AUX Aan/Uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="709"/>
        <source>Auxiliary on/off</source>
        <translation>AUX aan/uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="755"/>
        <source>Auto DJ</source>
        <translation>Auto DJ</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="757"/>
        <source>Auto DJ Shuffle</source>
        <translation>Auto DJ Schudden</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="760"/>
        <source>Auto DJ Skip Next</source>
        <translation>Auto DJ Volgende Overslaan</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="763"/>
        <source>Auto DJ Fade To Next</source>
        <translation>Auto DJ Invloeien Naar Volgende</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="764"/>
        <source>Trigger the transition to the next track</source>
        <translation>Activeer de overgang naar het volgende nummer</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="770"/>
        <source>User Interface</source>
        <translation>Gebruikersomgeving</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="772"/>
        <source>Samplers Show/Hide</source>
        <translation>Samplers Toon/Verberg</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="773"/>
        <source>Show/hide the sampler section</source>
        <translation>Toon/Verberg Sampler sectie</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="775"/>
        <source>Microphone Show/Hide</source>
        <translation>Microfoon Toon/Verberg</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="776"/>
        <source>Show/hide the microphone section</source>
        <translation>Toon/Verberg de microfoon sectie</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="778"/>
        <source>Vinyl Control Show/Hide</source>
        <translation>Vinyl Controle Toon/Verberg</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="779"/>
        <source>Show/hide the vinyl control section</source>
        <translation>Toon/Verberg de vinyl controle sectie</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="781"/>
        <source>Preview Deck Show/Hide</source>
        <translation>Speler Voorbeluisteren Toon/Verberg</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="782"/>
        <source>Show/hide the preview deck</source>
        <translation>Toon/Verberg de Voorbeluister-speler </translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="793"/>
        <source>Vinyl Spinner Show/Hide</source>
        <translation>Vinyl Spinner Toon/Verberg</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="794"/>
        <source>Show/hide spinning vinyl widget</source>
        <translation>Toon/Verberg de spinning vinyl widget</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="802"/>
        <source>Waveform zoom</source>
        <translation>Waveform zoom</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="802"/>
        <source>Waveform Zoom</source>
        <translation>Waveform Zoom</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="803"/>
        <source>Zoom waveform in</source>
        <translation>Waveform inzoomen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="803"/>
        <source>Waveform Zoom In</source>
        <translation>Waveform Inzoomen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlpickermenu.cpp" line="804"/>
        <source>Zoom waveform out</source>
        <translation>Waveform uitzoomen</translation>
    </message>
</context>
<context>
    <name>ControllerEngine</name>
    <message>
        <location filename="../../src/controllers/controllerengine.cpp" line="466"/>
        <source>Uncaught exception at line %1 in file %2: %3</source>
        <translation>Fout:Niet-opgevangen uitzondering (Uncaught Exception)  op regel %1 in bestand %2:%3</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerengine.cpp" line="470"/>
        <source>Uncaught exception at line %1 in passed code: %2</source>
        <translation>Fout: Uncaught exception op regel 1% in doorgegeven code: 2%</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerengine.cpp" line="493"/>
        <source>Controller script error</source>
        <translation>Controller scriptfout</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerengine.cpp" line="494"/>
        <source>A control you just used is not working properly.</source>
        <translation>Een stuureenheid die u zojuist hebt gebruikt werkt niet goed.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerengine.cpp" line="495"/>
        <source>The script code needs to be fixed.</source>
        <translation>De script code moet hersteld worden.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerengine.cpp" line="496"/>
        <source>For now, you can: Ignore this error for this session but you may experience erratic behavior.</source>
        <translation>Voor nu kunt u: deze fout voor deze sessie negeren, maar u kunt onstabiel gedrag ervaren.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerengine.cpp" line="497"/>
        <source>Try to recover by resetting your controller.</source>
        <translation>Probeer te herstellen door uw controller te resetten.</translation>
    </message>
</context>
<context>
    <name>ControllerInputMappingTableModel</name>
    <message>
        <location filename="../../src/controllers/controllerinputmappingtablemodel.cpp" line="37"/>
        <source>Channel</source>
        <translation>Kanaal</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerinputmappingtablemodel.cpp" line="38"/>
        <source>Opcode</source>
        <translation>Opcode</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerinputmappingtablemodel.cpp" line="39"/>
        <source>Control</source>
        <translation>Bediening</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerinputmappingtablemodel.cpp" line="40"/>
        <source>Options</source>
        <translation>Opties</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerinputmappingtablemodel.cpp" line="41"/>
        <source>Action</source>
        <translation>Actie</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controllerinputmappingtablemodel.cpp" line="42"/>
        <source>Comment</source>
        <translation>Ommerking</translation>
    </message>
</context>
<context>
    <name>ControllerOutputMappingTableModel</name>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="36"/>
        <source>Channel</source>
        <translation>Kanaal</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="37"/>
        <source>Opcode</source>
        <translation>Opcode</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="38"/>
        <source>Control</source>
        <translation>Bediening</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="39"/>
        <source>On Value</source>
        <translation>Aan-waarde</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="40"/>
        <source>Off Value</source>
        <translation>Uit-Waarde</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="41"/>
        <source>Action</source>
        <translation>Actie</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="42"/>
        <source>On Range Min</source>
        <translation>Op Min bereik</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="43"/>
        <source>On Range Max</source>
        <translation>Op Max bereik</translation>
    </message>
    <message>
        <location filename="../../src/controllers/controlleroutputmappingtablemodel.cpp" line="44"/>
        <source>Comment</source>
        <translation>Ommerking</translation>
    </message>
</context>
<context>
    <name>CrateFeature</name>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="69"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="65"/>
        <location filename="../../src/library/crate/cratefeature.cpp" line="146"/>
        <source>Create New Crate</source>
        <translation>Maak nieuwe krat</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="73"/>
        <source>Rename</source>
        <translation>Hernoemen</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="77"/>
        <location filename="../../src/library/crate/cratefeature.cpp" line="324"/>
        <source>Lock</source>
        <translation>Vergrendelen</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="93"/>
        <source>Export Track Files</source>
        <translation>Exporteer Tracks</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="97"/>
        <source>Duplicate</source>
        <translation>Dupliceren</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="101"/>
        <source>Analyze entire Crate</source>
        <translation>Analyseer volledige krat</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="105"/>
        <source>Auto DJ Track Source</source>
        <translation>Auto DJ Track Bron</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="380"/>
        <source>Enter new name for crate:</source>
        <translation>Geef nieuwe naam voor krat:</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="132"/>
        <location filename="../../src/library/crate/cratefeature.cpp" line="140"/>
        <source>Crates</source>
        <translation>Platenbakken</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="81"/>
        <location filename="../../src/library/crate/cratefeature.cpp" line="85"/>
        <source>Import Crate</source>
        <translation>Importeer krat</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="89"/>
        <location filename="../../src/library/crate/cratefeature.cpp" line="663"/>
        <source>Export Crate</source>
        <translation>Exporteer krat</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="324"/>
        <source>Unlock</source>
        <translation>Ontgrendel</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="620"/>
        <source>An unknown error occurred while creating crate: </source>
        <translation>Een onbekende fout opgetreden bij het maken krat:</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="379"/>
        <source>Rename Crate</source>
        <translation>Hernoem krat</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="390"/>
        <location filename="../../src/library/crate/cratefeature.cpp" line="397"/>
        <source>Renaming Crate Failed</source>
        <translation>Hernoemen krat mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="619"/>
        <source>Crate Creation Failed</source>
        <translation>Aanmaken krat mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="665"/>
        <source>M3U Playlist (*.m3u);;M3U8 Playlist (*.m3u8);;PLS Playlist (*.pls);;Text CSV (*.csv);;Readable Text (*.txt)</source>
        <translation>M3U Speellijst (*.m3u);;M3U8 Speellijst (*.m3u8);;PLS Speellijst (*.pls);;Tekst CSV (*.csv);;Leesbare Tekst (*.txt)</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="141"/>
        <source>Crates are a great way to help organize the music you want to DJ with.</source>
        <translation>Kratten zijn een geweldige manier om de muziek te organiseren waarmee je wilt DJ&apos;en.</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="142"/>
        <source>Make a crate for your next gig, for your favorite electrohouse tracks, or for your most requested songs.</source>
        <translation>Maak een krat voor je volgende optreden, voor je favoriete electrohouse-nummers of voor je meest gevraagde nummers.</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="143"/>
        <source>Crates let you organize your music however you&apos;d like!</source>
        <translation>Kratten laten je toe om je muziek te organiseren zoals jij dat  wil!</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="391"/>
        <source>A crate cannot have a blank name.</source>
        <translation>Een krat kan geen lege naam hebben.</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeature.cpp" line="398"/>
        <source>A crate by that name already exists.</source>
        <translation>Een krat met deze naam bestaat al.</translation>
    </message>
</context>
<context>
    <name>CrateFeatureHelper</name>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="36"/>
        <source>New Crate</source>
        <translation>Nieuwe krat</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="43"/>
        <source>Create New Crate</source>
        <translation>Maak nieuwe krat</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="44"/>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="99"/>
        <source>Enter name for new crate:</source>
        <translation>Geef naam in voor nieuwe krat:</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="54"/>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="61"/>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="81"/>
        <source>Creating Crate Failed</source>
        <translation>Krat aanmaken mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="55"/>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="110"/>
        <source>A crate cannot have a blank name.</source>
        <translation>Een krat kan geen lege naam hebben.</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="62"/>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="117"/>
        <source>A crate by that name already exists.</source>
        <translation>Een krat met deze naam bestaat al.</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="82"/>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="153"/>
        <source>An unknown error occurred while creating crate: </source>
        <translation>Een onbekende fout opgetreden bij het maken krat:</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="91"/>
        <source>copy</source>
        <comment>[noun]</comment>
        <translation>kopieer</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="98"/>
        <source>Duplicate Crate</source>
        <translation>Krat dupliceren</translation>
    </message>
    <message>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="109"/>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="116"/>
        <location filename="../../src/library/crate/cratefeaturehelper.cpp" line="152"/>
        <source>Duplicating Crate Failed</source>
        <translation>Krat dupliceren mislukt</translation>
    </message>
</context>
<context>
    <name>DlgAbout</name>
    <message>
        <location filename="../../src/dialog/dlgabout.cpp" line="35"/>
        <source>Mixxx %1 Development Team</source>
        <translation>Mixxx %1 Ontwikkelaarsteam</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgabout.cpp" line="36"/>
        <source>With contributions from:</source>
        <translation>Met bijdragen van:</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgabout.cpp" line="37"/>
        <source>And special thanks to:</source>
        <translation>En speciale dank aan:</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgabout.cpp" line="38"/>
        <source>Past Developers</source>
        <translation>Eerdere Ontwikkelaars</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgabout.cpp" line="39"/>
        <source>Past Contributors</source>
        <translation>Eerdere Medewerkers</translation>
    </message>
</context>
<context>
    <name>DlgAboutDlg</name>
    <message>
        <location filename="../../src/dialog/dlgaboutdlg.ui" line="26"/>
        <source>About Mixxx</source>
        <translation>Over Mixxx</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgaboutdlg.ui" line="110"/>
        <source>Credits</source>
        <translation>Dankwoord</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgaboutdlg.ui" line="145"/>
        <source>License</source>
        <translation>Licentie</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgaboutdlg.ui" line="176"/>
        <source>&lt;a href=&quot;http://mixxx.org/&quot;&gt;Official Website&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://mixxx.org/&quot;&gt;Officiele Website&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>DlgAnalysis</name>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="14"/>
        <location filename="../../src/library/dlganalysis.ui" line="105"/>
        <location filename="../../src/library/dlganalysis.cpp" line="159"/>
        <source>Analyze</source>
        <translation>Analyseren</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="52"/>
        <source>Shows tracks added to the library within the last 7 days.</source>
        <translation>Toont tracks die in de afgelopen 7 dagen aan de bibliotheek zijn toegevoegd.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="55"/>
        <source>New</source>
        <translation>Nieuw</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="62"/>
        <source>Shows all tracks in the library.</source>
        <translation>Toon alle tracks in de bibliotheek.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="65"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="85"/>
        <source>Progress</source>
        <translation>Voortgang</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="92"/>
        <source>Selects all tracks in the table below.</source>
        <translation>Selecteer alle tracks in de tabel hieronder.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="95"/>
        <source>Select All</source>
        <translation>Selecteer Alles</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.ui" line="102"/>
        <source>Runs beatgrid, key, and ReplayGain detection on the selected tracks. Does not generate waveforms for the selected tracks to save disk space.</source>
        <translation>Voert beat-rooster-, toonaard- en afspeelversterking-detectie uit op de geselecteerde tracks. Genereert geen golfvormen op de geselecteerde tracks om schijfruimte te besparen.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.cpp" line="156"/>
        <source>Stop Analysis</source>
        <translation>Stop Analyse</translation>
    </message>
    <message>
        <location filename="../../src/library/dlganalysis.cpp" line="176"/>
        <source>Analyzing %1/%2 %3%</source>
        <translation>Analyseren van %1/%2 %3%</translation>
    </message>
</context>
<context>
    <name>DlgAutoDJ</name>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="143"/>
        <source>One deck must be stopped to enable Auto DJ mode.</source>
        <translation>Eén speler moet gestopt worden om Auto DJ modus in te schakelen.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="151"/>
        <source>Decks 3 and 4 must be stopped to enable Auto DJ mode.</source>
        <translation>Spelers 3 en 4 moeten gestopt worden om Auto DJ modus in te schakelen.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="179"/>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="180"/>
        <source>Disable Auto DJ</source>
        <translation>Schakel Auto DJ uit</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="213"/>
        <source>Displays the duration and number of selected tracks.</source>
        <translation>Geeft de duur en het aantal geselecteerde tracks weer.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="14"/>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="142"/>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="150"/>
        <source>Auto DJ</source>
        <translation>Auto DJ</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="55"/>
        <source>Shuffle</source>
        <translation>Schudden</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="69"/>
        <source>Add Random</source>
        <translation>Willekeurig toevoegen</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="52"/>
        <source>Shuffle the content of the Auto DJ queue.</source>
        <translation>Schud de inhoud van de Auto DJ-wachtrij.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="65"/>
        <source>Adds a random track from track sources (crates) to the Auto DJ queue.
If no track sources are configured, the track is added from the library instead.</source>
        <translation>Voegt een willekeurig nummer van nummerbronnen (kratten) toe aan de Auto DJ-wachtrij.
Als er geen trackbronnen zijn geconfigureerd, wordt de track  toegevoegd vanuit de bibliotheek.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="76"/>
        <source>Skip the next track in the Auto DJ queue.</source>
        <translation>Het volgende nummer overslaan in de Auto DJ-wachtrij.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="79"/>
        <source>Skip Track</source>
        <translation>Track overslaan</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="89"/>
        <source>Trigger the transition to the next track.</source>
        <translation>Activeer de overgang naar de volgende track.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="92"/>
        <source>Fade Now</source>
        <translation>Fade nu</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="105"/>
        <source>Determines the duration of the transition.</source>
        <translation>Bepaalt de duur van de overgang.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="121"/>
        <source>Seconds</source>
        <comment>&quot;sec.&quot; as in seconds</comment>
        <translation>Seconden</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="124"/>
        <source>sec.</source>
        <translation>sec.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="151"/>
        <source>Turn Auto DJ on or off.</source>
        <translation>Schakel Auto DJ in of uit.</translation>
    </message>
    <message>
        <location filename="../../src/library/autodj/dlgautodj.ui" line="154"/>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="172"/>
        <location filename="../../src/library/autodj/dlgautodj.cpp" line="173"/>
        <source>Enable Auto DJ</source>
        <translation>Schakel Auto DJ in</translation>
    </message>
</context>
<context>
    <name>DlgBeatsDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="20"/>
        <source>When beat detection is enabled, Mixxx detects the beats per minute and beats of your tracks, 
automatically shows a beat-grid for them, and allows you to synchronize tracks using their beat information.</source>
        <translation>Wanneer beat detectie is ingeschakeld, detecteert Mixxx de beats per minuut en beats van je tracks,
toont hiervoor automatisch een beat-raster en stelt u in staat tracks te synchroniseren met behulp van hun beat-informatie.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="24"/>
        <source>Enable BPM and Beat Detection</source>
        <translation>Schakel BPM en Beat Detectie in</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="198"/>
        <source>Choose between different algorithms to detect beats.</source>
        <translation>Kies tussen verschillende algoritmen om beats te detecteren.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="109"/>
        <source>Enable fast beat detection. 
If activated Mixxx only analyzes the first minute of a track for beat information. 
This can speed up beat detection on slower computers but may result in lower quality beatgrids.</source>
        <translation>Activeer snelle beat-detectie.Als dit aan staat, dan analyseert Mixxx enkel de eerste minuut van een track voor beat informatie. 
Dit kan de beat detectie versnellen, maar heeft mogelijk een verminderde kwaliteit van beat-rasters als gevolg.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="133"/>
        <source>Attempts to correct the phase (first beat) of fixed-tempo beatgrids 
by analyzing the beats to discard outliers.</source>
        <translation>Probeert de fase (eerste beat) te corrigeren van beat-rasters met vast tempo 
door de beats te analyseren ten einde uitschieters over te slaan.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="14"/>
        <source>Beat Detection Preferences</source>
        <translation>Beat Detectie Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="158"/>
        <source>Choose Analyzer</source>
        <translation>Kies Analysator</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="179"/>
        <source>Beat Analyzer:</source>
        <translation>Beat Analysator:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="103"/>
        <source>Analyzer Settings</source>
        <translation>Instellingen Analysator</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="114"/>
        <source>Enable Fast Analysis (For slow computers, may be less accurate)</source>
        <translation>Snelle Analyse Inschakelen (voor langzame computers is dit mogelijk minder nauwkeurig)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="121"/>
        <source>Converts beats detected by the analyzer into a fixed-tempo beatgrid. 
Use this setting if your tracks have a constant tempo (e.g. most electronic music). 
Often results in higher quality beatgrids, but will not do well on tracks that have tempo shifts.</source>
        <translation>Zet de door de analysator gedetecteerde beats om in een beat-raster met een vast tempo.
Gebruik deze instelling als uw tracks een constant tempo hebben (bijv. De meeste elektronische muziek).
Dit resulteert vaak in beat-rasters van hogere kwaliteit, maar zal het niet goed doen op tracks met tempoveranderingen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="126"/>
        <source>Assume constant tempo (Recommended)</source>
        <translation>Ga uit van een constant tempo (Aanbevolen)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="137"/>
        <source>Enable Offset Correction (Recommended)</source>
        <translation>Offset-correctie inschakelen (Aanbevolen)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="144"/>
        <source>e.g. from 3rd-party programs or Mixxx versions before 1.11.
(Not checked: Analyze only, if no beats exist.)</source>
        <translation>bijv. van programma&apos;s van derden of Mixxx-versies vóór 1.11.
(Niet aangevinkt: alleen analyseren, als er geen beats bestaan.)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="148"/>
        <source>Re-analyze beats when settings change or beat detection data is outdated</source>
        <translation>Analyseer beats opnieuw wanneer instellingen veranderen of de detectie-data van de beat verouderd zijn</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="44"/>
        <source>BPM Range</source>
        <translation>BPM-Bereik</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="50"/>
        <source>Min:</source>
        <translation>Min:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbeatsdlg.ui" line="70"/>
        <source>Max:</source>
        <translation>Max:</translation>
    </message>
</context>
<context>
    <name>DlgControllerLearning</name>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="32"/>
        <source>Controller Learning Wizard</source>
        <translation>Controller Aanleer Assistent</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="70"/>
        <source>Learn</source>
        <translation>Aanleren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="80"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="121"/>
        <source>Choose Control...</source>
        <translation>Kies Bediening...</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="192"/>
        <source>Hints: If you&apos;re mapping a button or switch, only press or flip it once.  For knobs and sliders, move the control in both directions for best results. Make sure to touch one control at a time.</source>
        <translation>Tip: Om een drukknop of schakelaar te koppelen, één keer drukken of omschakelen. Voor draaiknoppen en schuiven, de bediening in beide richtingen bewegen voor optimaal resultaat. Let er op om slechts één bedieningselement tegelijk aan te raken.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="247"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="320"/>
        <source>Advanced MIDI Options</source>
        <translation>Geavanceerde MIDI Opties</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="326"/>
        <source>Switch mode interprets all messages for the control as button presses.</source>
        <translation>Switch-modus interpreteert alle berichten voor het bedieningselement alsof de knop wordt ingedrukt.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="329"/>
        <source>Switch Mode</source>
        <translation>Wijzig Modus</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="336"/>
        <source>Ignores slider or knob movements until they are close to the internal value. This helps prevent unwanted extreme changes while mixing but can accidentally ignore intentional rapid movements.</source>
        <translation>Negeert schuif- of knopbewegingen totdat ze dicht bij de interne waarde liggen. Dit helpt het voorkomen van  ongewenste extreme veranderingen tijdens het mixen, maar kan per ongeluk opzettelijke snelle bewegingen negeren.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="339"/>
        <source>Soft Takeover</source>
        <translation>Software Overname</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="346"/>
        <source>Reverses the direction of the control.</source>
        <translation>Draait de richting van het bedieningselement om.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="349"/>
        <source>Invert</source>
        <translation>Draai Om</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="356"/>
        <source>For jog wheels or infinite-scroll knobs. Interprets incoming messages in two&apos;s complement.</source>
        <translation>Voor jogwielen of blijvende doordraai-knoppen. Interpreteert inkomende berichten in het 2-complement.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="359"/>
        <source>Jog Wheel / Select Knob</source>
        <translation>Jogwiel / Selectieknop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="387"/>
        <source>Retry</source>
        <translation>Opnieuw Proberen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="407"/>
        <source>Learn Another</source>
        <translation>Een Andere Aanleren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.ui" line="414"/>
        <source>Done</source>
        <translation>Gereed</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="36"/>
        <source>Click anywhere in Mixxx or choose a control to learn</source>
        <translation>Klik ergens in Mixxx of kies een besturingselement om te leren</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="37"/>
        <source>You can click on any button, slider, or knob in Mixxx to teach it that control.  You can also type in the box to search for a control by name, or click the Choose Control button to select from a list.</source>
        <translation>Je kunt op een willekeurige knop, schuifregelaar of regelaar in Mixxx klikken om hem dat element aan te leren. U kunt ook in het vak typen om naar een besturingselement op naam te zoeken, of klik op de knop Besturingselement Kiezen om uit een lijst te selecteren.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="48"/>
        <source>Now test it out!</source>
        <translation>Test het nu uit!</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="49"/>
        <source>If you manipulate the control, you should see the Mixxx user interface respond the way you expect.</source>
        <translation>Als u de besturing manipuleert, zou u de Mixxx-gebruikersinterface moeten zien reageren zoals u het verwacht.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="52"/>
        <source>Not quite right?</source>
        <translation>Niet helemaal juist?</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="53"/>
        <source>If the mapping is not working try enabling an advanced option below and then try the control again. Or click Retry to redetect the midi control.</source>
        <translation>Als de koppeling niet werkt, probeer dan een geavanceerde optie hieronder in te schakelen en probeer het vervolgens opnieuw. Of klik op Opnieuw Proberen om de midi-besturing opnieuw te detecteren.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="272"/>
        <source>Didn&apos;t get any midi messages.  Please try again.</source>
        <translation>Geen MIDI-berichten ontvangen. Probeer het opnieuw a.u.b.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="288"/>
        <source>Unable to detect a mapping -- please try again. Be sure to only touch one control at once.</source>
        <translation>Kan een toewijzing niet detecteren - probeer het opnieuw a.u.b. Zorg ervoor dat u slechts één bedieningselement tegelijk aanraakt.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="333"/>
        <source>Successfully mapped control:</source>
        <translation>Bediening met succes toegewezen:</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="439"/>
        <source>&lt;i&gt;Ready to learn %1&lt;/i&gt;</source>
        <translation>&lt;i&gt;Kaar om te leren %1&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="440"/>
        <source>Learning: %1. Now move a control on your controller.</source>
        <translation>Aan Het Leren:% 1. Verplaats nu een besturingselement op uw controller.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgcontrollerlearning.cpp" line="466"/>
        <source>The control you clicked in Mixxx is not learnable.
This could be because you are using an old skin and this control is no longer supported.

You tried to learn: %1,%2</source>
        <translation>De controle die je hebt aangeklikt in Mixxx is niet aan te leren.
Dit kan zijn omdat u een oude skin gebruikt en deze controle niet langer wordt ondersteund.

Je hebt geprobeerd te leren:% 1,% 2</translation>
    </message>
</context>
<context>
    <name>DlgDeveloperTools</name>
    <message>
        <location filename="../../src/dialog/dlgdevelopertoolsdlg.ui" line="14"/>
        <source>Developer Tools</source>
        <translation>Tools voor Ontwikkelaars</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgdevelopertoolsdlg.ui" line="24"/>
        <source>Controls</source>
        <translation>Besturingen</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgdevelopertoolsdlg.ui" line="46"/>
        <source>Dumps all ControlObject values to a csv-file saved in the settings path (e.g. ~/.mixxx)</source>
        <translation>Dumpt alle BesturingsObject-waarden naar een csv-bestand dat wordt opgeslagen in het instellingenpad (bijv. ~/.mixxx)</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgdevelopertoolsdlg.ui" line="49"/>
        <source>Dump to csv</source>
        <translation>Naar csv dumpen</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgdevelopertoolsdlg.ui" line="85"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgdevelopertoolsdlg.ui" line="94"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../../src/dialog/dlgdevelopertoolsdlg.ui" line="135"/>
        <source>Stats</source>
        <translation>Statussen</translation>
    </message>
</context>
<context>
    <name>DlgHidden</name>
    <message>
        <location filename="../../src/library/dlghidden.ui" line="14"/>
        <source>Hidden Tracks</source>
        <translation>Verborgen Tracks</translation>
    </message>
    <message>
        <location filename="../../src/library/dlghidden.ui" line="65"/>
        <source>Selects all tracks in the table below.</source>
        <translation>Selecteer alle tracks in de tabel hieronder.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlghidden.ui" line="68"/>
        <source>Select All</source>
        <translation>Selecteer Alles</translation>
    </message>
    <message>
        <location filename="../../src/library/dlghidden.ui" line="75"/>
        <source>Purge selected tracks from the library.</source>
        <translation>Verwijder geselecteerd tracks uit de bibliotheek.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlghidden.ui" line="78"/>
        <source>Purge</source>
        <translation>Verwijder</translation>
    </message>
    <message>
        <location filename="../../src/library/dlghidden.ui" line="88"/>
        <source>Unhide selected tracks from the library.</source>
        <translation>Maak geselecteerde tracks zichtbaar voor de bibliotheek.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlghidden.ui" line="91"/>
        <source>Unhide</source>
        <translation>Zichtbaar maken</translation>
    </message>
    <message>
        <location filename="../../src/library/dlghidden.ui" line="94"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
</context>
<context>
    <name>DlgMissing</name>
    <message>
        <location filename="../../src/library/dlgmissing.ui" line="14"/>
        <source>Missing Tracks</source>
        <translation>Ontbrekende Tracks</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgmissing.ui" line="65"/>
        <source>Selects all tracks in the table below.</source>
        <translation>Selecteer alle tracks in de tabel hieronder.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgmissing.ui" line="68"/>
        <source>Select All</source>
        <translation>Selecteer Alles</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgmissing.ui" line="75"/>
        <source>Purge selected tracks from the library.</source>
        <translation>Verwijder geselecteerd tracks uit de bibliotheek.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgmissing.ui" line="78"/>
        <source>Purge</source>
        <translation>Verwijder</translation>
    </message>
</context>
<context>
    <name>DlgPrefAutoDJ</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodj.cpp" line="10"/>
        <location filename="../../src/preferences/dialog/dlgprefautodj.cpp" line="40"/>
        <source>Off</source>
        <translation>Uit</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodj.cpp" line="11"/>
        <location filename="../../src/preferences/dialog/dlgprefautodj.cpp" line="41"/>
        <source>On</source>
        <translation>Aan</translation>
    </message>
</context>
<context>
    <name>DlgPrefAutoDJDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="22"/>
        <source>Re-queue tracks after playback</source>
        <translation>Zet track opnieuw in wachtrij na het afspelen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="79"/>
        <source>Duration after which a track is eligible for selection by Auto DJ again</source>
        <translation>Duurtijd waarna een track opnieuw in aanmerking komt voor selectie door Auto DJ</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="82"/>
        <source>hh:mm</source>
        <translation>hh:mm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="39"/>
        <source>Minimum available tracks in Track Source</source>
        <translation>Minimum aantal beschikbare tracks in de trackbron</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="14"/>
        <source>Auto DJ Preferences</source>
        <translation>Auto DJ Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="32"/>
        <source>Add a track to the end of the Auto DJ queue once it is played, instead of removing it.</source>
        <translation>Voeg een track toe aan het einde van de Auto DJ-wachtrij zodra deze is afgespeeld, in plaats van deze te verwijderen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="46"/>
        <source>This percentage of tracks are always available for selecting, regardless of when they were last played.</source>
        <translation>Dit percentage tracks is altijd beschikbaar om te selecteren, ongeacht wanneer ze voor het laatst zijn afgespeeld.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="49"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="62"/>
        <source>Uncheck, to ignore all played tracks.</source>
        <translation>Afvinken, om alle reeds gespeelde tracks te negeren.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="65"/>
        <source>Suspend track in Track Source from re-queue</source>
        <translation>Track ontheffen in trackbron van opnieuw toevoegen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="72"/>
        <source>Suspension period for track selection</source>
        <translation>Ontheffingsperiode voor track selectie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="92"/>
        <source>Enable random track addition to queue</source>
        <translation>Schakel willekeurige track-toevoeging aan de wachtrij in</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="102"/>
        <source>Add random tracks from Track Source if the specified minimum tracks remain</source>
        <translation>Voeg willekeurig tracks van Track-bron toe wanneer het aangegeven minimum aantal tracks overblijft.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="109"/>
        <source>Minimum allowed tracks before addition</source>
        <translation>Minimaal aantal toegestane tracks alvorens toevoeging</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefautodjdlg.ui" line="119"/>
        <source>Minimum number of tracks after which random tracks may be added</source>
        <translation>Minimum aantal tracks waarna willekeurige tracks kunnen worden toegevoegd</translation>
    </message>
</context>
<context>
    <name>DlgPrefBroadcast</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="96"/>
        <source>Icecast 2</source>
        <translation>Icecast 2</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="97"/>
        <source>Shoutcast 1</source>
        <translation>Shoutcast 1</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="98"/>
        <source>Icecast 1</source>
        <translation>Icecast 1</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="121"/>
        <source>MP3</source>
        <translation>MP3</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="122"/>
        <source>Ogg Vorbis</source>
        <translation>Ogg Vorbis</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="125"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="127"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="129"/>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="202"/>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="262"/>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="537"/>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="571"/>
        <source>Action failed</source>
        <translation>Actie mislukt</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="203"/>
        <source>'%1' has the same Icecast mountpoint as '%2'.
Two source connections to the same server can&apos;t have the same mountpoint.</source>
        <translation>&apos;%1&apos; heeft hetzelfde Icecast-mountpoint als &apos;%2&apos;.
Twee bronverbindingen naar dezelfde server kunnen niet hetzelfde mountpoint hebben.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="263"/>
        <source>You can&apos;t create more than %1 source connections.</source>
        <translation>U kunt niet meer dan %1 bronverbindingen maken.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="276"/>
        <source>Source connection %1</source>
        <translation>Bronverbinding %1</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="538"/>
        <source>At least one source connection is required.</source>
        <translation>Er is ten minste één bronverbinding vereist.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="582"/>
        <source>Are you sure you want to disconnect every active source connection?</source>
        <translation>Weet u zeker dat u elke actieve bronverbinding wilt verbreken?</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="544"/>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="581"/>
        <source>Confirmation required</source>
        <translation>Bevestiging vereist</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="545"/>
        <source>Are you sure you want to delete &apos;%1&apos;?</source>
        <translation>Weet u zeker dat u %1 wilt verwijderen?</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="560"/>
        <source>Renaming &apos;%1&apos;</source>
        <translation>&apos;%1&apos; hernoemen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="561"/>
        <source>New name for &apos;%1&apos;:</source>
        <translation>Nieuwe naam voor &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcast.cpp" line="572"/>
        <source>Can&apos;t rename &apos;%1&apos; to &apos;%2&apos;: name already in use</source>
        <translation>Kan de naam van &apos;%1&apos; niet hernoemen naar &apos;%2&apos;: naam is al in gebruik</translation>
    </message>
</context>
<context>
    <name>DlgPrefBroadcastDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="14"/>
        <source>Live Broadcasting Preferences</source>
        <translation>Voorkeuren voor live-uitzendingen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="506"/>
        <source>Mixxx Icecast Testing</source>
        <translation>Testen Mixx Icecast</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="636"/>
        <source>Public stream</source>
        <translation>Openbare stream</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="551"/>
        <source>http://www.mixxx.org</source>
        <translation>http://www.mixxx.org</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="519"/>
        <source>Stream name</source>
        <translation>Stream-naam</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="437"/>
        <source>Due to flaws in some streaming clients, updating Ogg Vorbis metadata dynamically can cause listener glitches and disconnections. Check this box to update the metadata anyway.</source>
        <translation>Vanwege gebreken in sommige streaming clients, kan het dynamisch bijwerken van Ogg Vorbis-metagegevens leiden tot hoorbare storingen en onderbrekingen veroorzaken. Vink dit vakje aan om de metagegevens toch bij te werken.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="20"/>
        <source>Live Broadcasting source connections</source>
        <translation>Bronverbindingen voor Live-uitzendingen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="65"/>
        <source>Delete selected</source>
        <translation>Verwijder geselecteerde</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="92"/>
        <source>Create new connection</source>
        <translation>Maak een nieuwe verbinding</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="115"/>
        <source>Rename selected</source>
        <translation>Hernoem geselecteerde</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="72"/>
        <source>Disconnect all</source>
        <translation>Ontkoppel alles</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="124"/>
        <source>Turn on Live Broadcasting when applying these settings</source>
        <translation>Schakel Live-uitzenden in wanneer u deze instellingen toepast</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="137"/>
        <source>Settings for %1</source>
        <translation>Instellingen voor %1</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="440"/>
        <source>Dynamically update Ogg Vorbis metadata.</source>
        <translation>Ogg Vorbis-metadata dynamisch bijwerken.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="486"/>
        <source>ICQ</source>
        <translation>ICQ</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="493"/>
        <source>AIM</source>
        <translation>AIM</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="535"/>
        <source>Website</source>
        <translation>Website</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="629"/>
        <source>Live mix</source>
        <translation>Live mix</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="643"/>
        <source>IRC</source>
        <translation>IRC</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="664"/>
        <source>Select a source connection above to edit its settings here</source>
        <translation>Selecteer hierboven een bronverbinding om de instellingen hier te bewerken</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="795"/>
        <source>Password storage</source>
        <translation>Wachtwoordopslag</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="802"/>
        <source>Plain text</source>
        <translation>Platte tekst</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="812"/>
        <source>Secure storage (OS keychain)</source>
        <translation>Veilige opslag (OS keychain)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="613"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="447"/>
        <source>Use UTF-8 encoding for metadata.</source>
        <translation>Gebruik UTF-8 codering voor metadata.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="564"/>
        <source>Description</source>
        <translation>Omschrijving</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="255"/>
        <source>Encoding</source>
        <translation>Codering</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="279"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="263"/>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="373"/>
        <source>Format</source>
        <translation>Formaat</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="292"/>
        <source>Channels</source>
        <translation>Kanalen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="671"/>
        <source>Server connection</source>
        <translation>Server connectie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="679"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="720"/>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="754"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="706"/>
        <source>Mount</source>
        <translation>Monteren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="734"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="768"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="478"/>
        <source>Stream info</source>
        <translation>Stream-info</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="362"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="421"/>
        <source>Use static artist and title.</source>
        <translation>Gebruik statische artiest en titel.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="404"/>
        <source>Static title</source>
        <translation>Statische titel</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="387"/>
        <source>Static artist</source>
        <translation>Statische artiest</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="143"/>
        <source>Automatic reconnect</source>
        <translation>Automatisch opnieuw verbinden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="225"/>
        <source>Time to wait before the first reconnection attempt is made.</source>
        <translation>Wachttijd alvorens de eerste poging tot opnieuw verbinden is gemaakt.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="179"/>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="228"/>
        <source> seconds</source>
        <translation> seconden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="238"/>
        <source>Wait until first attempt</source>
        <translation>Wacht tot de eerste poging</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="169"/>
        <source>Reconnect period</source>
        <translation>Periode tot opnieuw verbinden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="176"/>
        <source>Time to wait between two reconnection attempts.</source>
        <translation>Wachttijd tussen twee pogingen tot opnieuw verbinden.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="218"/>
        <source>Limit number of reconnection attempts</source>
        <translation>Beperk het aantal pogingen tot herverbinding</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="208"/>
        <source>Maximum retries</source>
        <translation>Maximale pogingen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="151"/>
        <source>Reconnect if the connection to the streaming server is lost.</source>
        <translation>Herverbinden als de verbinding met de streaming server verbroken is.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefbroadcastdlg.ui" line="154"/>
        <source>Enable automatic reconnect</source>
        <translation>Automatisch opnieuw verbinden inschakelen</translation>
    </message>
</context>
<context>
    <name>DlgPrefController</name>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="117"/>
        <source>Apply device settings?</source>
        <translation>Apparaatinstellingen toepassen?</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="118"/>
        <source>Your settings must be applied before starting the learning wizard.
Apply settings and continue?</source>
        <translation>Uw instellingen moeten worden toegepast voordat u de leerassistent start.
Instellingen toepassen en doorgaan?</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="165"/>
        <source>None</source>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="170"/>
        <source>%1 by %2</source>
        <translation>%1 bij %2</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="187"/>
        <source>No Name</source>
        <translation>Geen Naam</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="196"/>
        <source>No Description</source>
        <translation>Geen Omschrijving</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="205"/>
        <source>No Author</source>
        <translation>Geen Auteur</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="442"/>
        <source>Troubleshooting</source>
        <translation>Probleemoplossen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="512"/>
        <source>Filename</source>
        <translation>Bestandsnaam</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="514"/>
        <source>Function Prefix</source>
        <translation>Functie voorvoegsel</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="516"/>
        <source>Built-in</source>
        <translation>Ingebouwd</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="592"/>
        <source>Clear Input Mappings</source>
        <translation>Wis Invoerkoppelingen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="593"/>
        <source>Are you sure you want to clear all input mappings?</source>
        <translation>Weet u het zeker dat u alle invoerkoppelingen wilt wissen?</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="633"/>
        <source>Clear Output Mappings</source>
        <translation>Wis Uitvoerkoppelingen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="634"/>
        <source>Are you sure you want to clear all output mappings?</source>
        <translation>Weet u het zeker dat u alle Uitvoerkoppelingen wikt wissen?</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="646"/>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="656"/>
        <source>Add Script</source>
        <translation>Script Toevoegen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="648"/>
        <source>Controller Script Files (*.js)</source>
        <translation>Script-bestanden controller (*.js)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="657"/>
        <source>Could not add script file: &apos;%s&apos;</source>
        <translation>Kan scriptbestand niet toevoegen: &apos;%s&apos;</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontroller.cpp" line="727"/>
        <source>Please select a script from the list to open.</source>
        <translation>Selecteer een script uit de lijst om te openen.</translation>
    </message>
</context>
<context>
    <name>DlgPrefControllerDlg</name>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="257"/>
        <source>(device category goes here)</source>
        <translation>(apparaatcategorie komt hier)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="238"/>
        <source>Controller Name</source>
        <translation>Controllernaam</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="267"/>
        <source>Enabled</source>
        <translation>Ingeschakeld</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="133"/>
        <source>Description:</source>
        <translation>Omschrijving:</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="299"/>
        <source>Load Preset:</source>
        <translation>Voorinstelling laden:</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="64"/>
        <source>Support:</source>
        <translation>Ondersteuning:</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="52"/>
        <source>Preset Info</source>
        <translation>Voorinstellingsinfo</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="332"/>
        <source>Input Mappings</source>
        <translation>Invoerkoppelingen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="360"/>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="432"/>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="485"/>
        <source>Add</source>
        <translation>Toevoegen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="370"/>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="439"/>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="492"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="312"/>
        <source>Click to start the Controller Learning wizard.</source>
        <translation>Klik om de Controller Leerassistent te starten.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="20"/>
        <source>Controller Preferences</source>
        <translation>Voorkeuren Controller</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="30"/>
        <source>Controller Setup</source>
        <translation>Controller Instellen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="193"/>
        <source>Author:</source>
        <translation>Auteur:</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="209"/>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="318"/>
        <source>Learning Wizard (MIDI Only)</source>
        <translation>Leerassistent (Enkel MIDI)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="396"/>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="459"/>
        <source>Clear All</source>
        <translation>Alles Wissen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="407"/>
        <source>Output Mappings</source>
        <translation>Uitvoerkoppelingen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="470"/>
        <source>Scripts</source>
        <translation>Scripts</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollerdlg.ui" line="512"/>
        <source>Open Selected File</source>
        <translation>Geselecteerd Bestand Openen</translation>
    </message>
</context>
<context>
    <name>DlgPrefControllersDlg</name>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="14"/>
        <source>Controller Preferences</source>
        <translation>Voorkeuren Controller</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="20"/>
        <source>Controllers</source>
        <translation>Controllers</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="66"/>
        <source>Mixxx did not detect any controllers. If you connected the controller while Mixxx was running you must restart Mixxx first.</source>
        <translation>Mixxx heeft geen controllers gedetecteerd. Als je de controller hebt aangesloten terwijl Mixxx actief was, dan moet je Mixxx eerst opnieuw opstarten.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="79"/>
        <source>Presets</source>
        <translation>Voorinstellingen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="110"/>
        <source>Open User Preset Folder</source>
        <translation>Open map met gebruikersvoorinstellingen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="136"/>
        <source>Resources</source>
        <translation>Bronnen</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="32"/>
        <source>Controllers are physical devices that send MIDI or HID signals to your computer over a USB connection. These allow you to control Mixxx in a more hands-on way than a keyboard and mouse. Attached controllers that Mixxx recognizes are shown in the &quot;Controllers&quot; section in the sidebar.</source>
        <translation>Controllers zijn fysieke apparaten die MIDI- of HID-signalen naar uw computer sturen via een USB-verbinding. Hiermee kunt u Mixxx op een meer praktische manier bedienen dan met een toetsenbord en muis. Aangesloten controllers die Mixxx herkent, worden weergegeven in de sectie &quot;Controllers&quot; in de zijbalk.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="91"/>
        <source>Mixxx uses &quot;presets&quot; to connect messages from your controller to controls in Mixxx. If you do not see a preset for your controller in the &quot;Load Preset&quot; menu when you click on your controller on the left sidebar, you may be able to download one online from the &lt;a href=&quot;http://www.mixxx.org/forums/viewforum.php?f=7&quot;&gt;Mixxx Forum&lt;/a&gt;. Place the XML (.xml) and Javascript (.js) file(s) in the &quot;User Preset Folder&quot; then restart Mixxx. If you download a mapping in a ZIP file, extract the XML and Javascript file(s) from the ZIP file to your &quot;User Preset Folder&quot; then restart Mixxx:</source>
        <translation>Mixxx gebruikt &quot;voorinstellingen&quot; om berichten van je controller te koppelen aan besturingselementen in Mixxx. Als je geen voorinstelling voor je controller ziet in het menu &quot;Voorinstellingen Laden&quot; wanneer je op je controller in de linkerzijbalk klikt, kun je er mogelijk één online downloaden van het &lt;a href=&quot;http://www.mixxx.org/forums/viewforum.php?f=7&quot;&gt;Mixxx Forum&lt;/a&gt;. Plaats de XML (.xml) en Javascript (.js) bestanden in de &quot;Map met gebruikersinstellingen&quot; en start Mixxx opnieuw op. Als u een afbeelding in een ZIP-bestand downloadt, extraheer dan de XML- en Javascript-bestanden uit het ZIP-bestand naar uw &quot;Map met Gebruikersvoorinstellingen&quot; en start Mixxx opnieuw op:</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="123"/>
        <source>You can create your own mapping by using the MIDI Learning Wizard when you select your controller in the sidebar. You can edit mappings by selecting the &quot;Input Mappings&quot; and &quot;Output Mappings&quot; tabs in the preference page for your controller. See the Resources below for more details on making mappings.</source>
        <translation>U kunt uw eigen koppeling maken door de MIDI Leerassistent te gebruiken wanneer u uw controller in de zijbalk selecteert. U kunt koppelingen bewerken door de tabbladen &quot;Invoerkoppelingen&quot; en &quot;Uitvoerkoppelingen&quot; te selecteren op de voorkeurenpagina van uw controller. Zie de onderstaande bronnen voor meer informatie over het maken van koppelingen.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="142"/>
        <source>&lt;a href=&quot;http://mixxx.org/wiki/doku.php/hardware_compatibility&quot;&gt;Mixxx DJ Hardware Guide&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://mixxx.org/wiki/doku.php/hardware_compatibility&quot;&gt;Mixxx DJ Hardware Gids&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="152"/>
        <source>&lt;a href=&quot;http://www.mixxx.org/forums/viewforum.php?f=7&quot;&gt;Mixxx Controller Forums&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://www.mixxx.org/forums/viewforum.php?f=7&quot;&gt;Mixxx Controller Forums&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="162"/>
        <source>&lt;a href=&quot;http://mixxx.org/wiki/doku.php/midi_controller_mapping_file_format&quot;&gt;MIDI Preset File Format&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://mixxx.org/wiki/doku.php/midi_controller_mapping_file_format&quot;&gt;MIDI Voorinstelling Bestandsformaat&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../src/controllers/dlgprefcontrollersdlg.ui" line="172"/>
        <source>&lt;a href=&quot;http://mixxx.org/wiki/doku.php/midi_scripting&quot;&gt;MIDI Scripting with Javascript&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://mixxx.org/wiki/doku.php/midi_scripting&quot;&gt;MIDI Scripting met Javascript&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>DlgPrefControlsDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="46"/>
        <source>Skin</source>
        <translation>Skin</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="138"/>
        <source>Tool tips</source>
        <translation>Tooltips</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="91"/>
        <source>Select from different color schemes of a skin if available.</source>
        <translation>Kies uit verschillende kleurenschema&apos;s van een skin indien beschikbaar.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="236"/>
        <source>Color scheme</source>
        <translation>Kleurenschema</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="108"/>
        <source>Locales determine country and language specific settings.</source>
        <translation>Regionale instellingen bepalen land- en taalspecifieke instellingen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="98"/>
        <source>Locale</source>
        <translation>Regio</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="14"/>
        <source>Interface Preferences</source>
        <translation>Voorkeuren Omgeving</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="20"/>
        <source>Interface options</source>
        <translation>Omgevingsopties</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="185"/>
        <source>HiDPI / Retina scaling</source>
        <translation>HiDPI / Retina schaling</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="195"/>
        <source>Change the size of text, buttons, and other items.</source>
        <translation>Wijzig de tekstgrootte, knoppen en andere items.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="217"/>
        <source>Adopt scale factor from the operating system</source>
        <translation>Neem schaalfactor over van het besturingssysteem</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="220"/>
        <source>Auto Scaling</source>
        <translation>Automatisch Schalen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="252"/>
        <source>Effect options</source>
        <translation>Effect opties</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="258"/>
        <source>Load behavior</source>
        <translation>Laadgedrag</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="267"/>
        <source>Keep metaknob position</source>
        <translation>Positie Metaknop behouden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="277"/>
        <source>Reset metaknob to effect default</source>
        <translation>Herstel metaknob naar standaard effect</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="33"/>
        <source>Screen saver</source>
        <translation>Screensaver</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="125"/>
        <source>Start in full-screen mode</source>
        <translation>Start in volledig scherm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="115"/>
        <source>Full-screen mode</source>
        <translation>Volledig-scherm modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="153"/>
        <source>Off</source>
        <translation>Uit</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="163"/>
        <source>Library only</source>
        <translation>Enkel bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterfacedlg.ui" line="173"/>
        <source>Library and Skin</source>
        <translation>Bibliotheek en Skin</translation>
    </message>
</context>
<context>
    <name>DlgPrefCrossfaderDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="20"/>
        <source>Crossfader Preferences</source>
        <translation>Voorkeuren Crossfader</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="26"/>
        <source>Crossfader Curve</source>
        <translation>Crossfader Curve</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="38"/>
        <source>Slow fade/Fast cut (additive)</source>
        <translation>Langzaam overvloeien/Snel kappen (toevoegen)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="45"/>
        <source>Constant power</source>
        <translation>Constant vermogen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="58"/>
        <source>Mixing</source>
        <translation>Mixen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="78"/>
        <source>Scratching</source>
        <translation>Scratchen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="117"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="143"/>
        <source>Logarithmic</source>
        <translation>Logaritmisch</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefcrossfaderdlg.ui" line="199"/>
        <source>Reverse crossfader (Hamster Style)</source>
        <translation>Omgekeerde cross-fader (Hamster-stijl)</translation>
    </message>
</context>
<context>
    <name>DlgPrefDeck</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="62"/>
        <source>Mixxx mode</source>
        <translation>Mixxx modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="63"/>
        <source>Mixxx mode (no blinking)</source>
        <translation>Mixxx-modus (zonder knipperen)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="64"/>
        <source>Pioneer mode</source>
        <translation>Pioneer modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="65"/>
        <source>Denon mode</source>
        <translation>Denon modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="66"/>
        <source>Numark mode</source>
        <translation>Numark modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="67"/>
        <source>CUP mode</source>
        <translation>CUP modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="131"/>
        <source>4%</source>
        <translation>4%</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="132"/>
        <source>6% (semitone)</source>
        <translation>6% (halve toon)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="133"/>
        <source>8% (Technics SL-1210)</source>
        <translation>8% (Technics SL-1210)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="134"/>
        <source>10%</source>
        <translation>10%</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="135"/>
        <source>16%</source>
        <translation>16%</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="136"/>
        <source>24%</source>
        <translation>24%</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="137"/>
        <source>50%</source>
        <translation>50%</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeck.cpp" line="138"/>
        <source>90%</source>
        <translation>90%</translation>
    </message>
</context>
<context>
    <name>DlgPrefDeckDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="14"/>
        <source>Deck Preferences</source>
        <translation>Speler Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="20"/>
        <source>Deck options</source>
        <translation>speler-opties</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="26"/>
        <source>Cue mode</source>
        <translation>Cue modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="39"/>
        <source>Mixxx mode:
- Cue button while pause at cue point = preview
- Cue button while pause not at cue point = set cue point
- Cue button while playing = pause at cue point
Mixxx mode (no blinking):
- Same as Mixxx mode but with no blinking indicators
Pioneer mode:
- Same as Mixxx mode with a flashing play button
Denon mode:
- Cue button at cue point = preview
- Cue button not at cue point = pause at cue point
- Play = set cue point
Numark mode:
- Same as Denon mode, but without a flashing play button
CUP mode:
- Cue button while pause at cue point = play after release
- Cue button while pause not at cue point = set cue point and play after release
- Cue button while playing = go to cue point and play after release
</source>
        <translation>Mixxx-modus:
- Cue-knop tijdens pauze op cue-punt = voorbeeld
- Cue-knop tijdens pauze niet op cue-punt = cue-punt instellen
- Cue-knop tijdens het spelen = pauze op cue-punt
Mixxx-modus (knippert niet):
- Hetzelfde als Mixxx-modus, maar zonder knipperende indicatoren
Pioneer-modus:
- Hetzelfde als Mixxx-modus met een knipperende afspeelknop
Denon-modus:
- Cue-knop op cue-punt = voorbeeld
- Cue-knop niet op cue-punt = pauze op cue-punt
- Spelen = cue-punt instellen
Numark-modus:
- Hetzelfde als Denon-modus, maar zonder een knipperende afspeelknop
CUP-modus:
- Cue-knop tijdens pauze op cue-punt = afspelen na loslaten
- Cue-knop tijdens pauze niet op cue-punt = cue-punt instellen en afspelen na loslaten
- Cue-knop tijdens het spelen = ga naar het cue-punt en speel na het loslaten
</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="70"/>
        <source>Track time display</source>
        <translation>Track tijdweergave</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="85"/>
        <source>Elapsed</source>
        <translation>Verstreken</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="95"/>
        <source>Remaining</source>
        <translation>Resterende</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="105"/>
        <source>Elapsed and Remaining</source>
        <translation>Verstreken en Resterende</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="117"/>
        <source>Auto cue</source>
        <translation>Auto cue</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="127"/>
        <source>Automatically seeks to the first saved cue point on track load.
            If none exists, seeks to the beginning of the track.</source>
        <translation>Zoekt automatisch naar het eerste opgeslagen cue-punt bij het laden van de track.
             Als er geen bestaat, zoekt u naar het begin van de track.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="131"/>
        <source>Jump to main cue point on track load</source>
        <translation>Spring naar het belangrijkste cue-punt bij het laden van de track</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="138"/>
        <source>Playing track protection</source>
        <translation>Trackbescherming tijdens afspelen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="148"/>
        <source>Do not load tracks into playing decks</source>
        <translation>Laad geen tracks in draaiende spelers</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="158"/>
        <source>Speed (Tempo) and Key (Pitch) options</source>
        <translation>Speed (Tempo) en Key (Pitch) opties</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="166"/>
        <source>Permanent rate change when left-clicking</source>
        <translation>Permanente snelheidsverandering bij links klikken</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="172"/>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="200"/>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="251"/>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="396"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="194"/>
        <source>Permanent rate change when right-clicking</source>
        <translation>Permanente snelheidsverandering bij rechts klikken</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="222"/>
        <source>Reset on track load</source>
        <translation>Reset bij laden van track</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="232"/>
        <source>Current key</source>
        <translation>Huidige key</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="245"/>
        <source>Temporary rate change when right-clicking</source>
        <translation>Tijdelijke snelheidsverandering bij rechts klikken</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="273"/>
        <source>Permanent</source>
        <translation>Permanent</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="310"/>
        <source>Value in milliseconds</source>
        <translation>Waarde in milliseconden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="334"/>
        <source>Temporary</source>
        <translation>Tijdelijk</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="344"/>
        <source>Keylock mode</source>
        <translation>Keylock modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="357"/>
        <source>Ramping sensitivity</source>
        <translation>Snelheid gevoeligheid</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="367"/>
        <source>Pitch bend behavior</source>
        <translation>Pitch vervormingsgedrag</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="377"/>
        <source>Original key</source>
        <translation>Originele key</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="390"/>
        <source>Temporary rate change when left-clicking</source>
        <translation>Permanente snelheidsverandering bij links klikken</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="418"/>
        <source>Speed/Tempo</source>
        <translation>Snelheid/Tempo</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="428"/>
        <source>Key/Pitch</source>
        <translation>Key/Pitch</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="438"/>
        <source>Adjustment buttons:</source>
        <translation>Aanpassing knoppen:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="454"/>
        <source>Coarse</source>
        <translation>Ruw</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="479"/>
        <source>Fine</source>
        <translation>Fijn</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="495"/>
        <source>Make the speed sliders work like those on DJ turntables and CDJs where moving downward increases the speed</source>
        <translation>Laat de snelheidsschuifregelaars werken zoals die op DJ-draaitafels en CDJ&apos;s waarbij neerwaarts bewegen de snelheid verhoogt</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="498"/>
        <source>Down increases speed</source>
        <translation>Omlaag verhoogt de snelheid</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="511"/>
        <source>Slider range</source>
        <translation>Schuifbereik</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="527"/>
        <source>Adjusts the range of the speed (Vinyl &quot;Pitch&quot;) slider.</source>
        <translation>Past het snelheidsbereik van de (Vinyl &quot;Pitch&quot;) schuifregelaar aan.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="534"/>
        <source>Abrupt jump</source>
        <translation>Abrupte sprong</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="544"/>
        <source>Smoothly adjusts deck speed when temporary change buttons are held down</source>
        <translation>Past de speler-snelheid soepel aan wanneer de knoppen voor tijdelijke aanpassing ingedrukt worden gehouden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="547"/>
        <source>Smooth ramping</source>
        <translation>Soepele toe of afname</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="557"/>
        <source>Keyunlock mode</source>
        <translation>Key ontgrendelingsmodus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="564"/>
        <source>Reset key</source>
        <translation>Reset key</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefdeckdlg.ui" line="574"/>
        <source>Keep key</source>
        <translation>Behoud key</translation>
    </message>
</context>
<context>
    <name>DlgPrefEQ</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeq.cpp" line="233"/>
        <location filename="../../src/preferences/dialog/dlgprefeq.cpp" line="263"/>
        <location filename="../../src/preferences/dialog/dlgprefeq.cpp" line="678"/>
        <source>None</source>
        <extracomment>Displayed when no effect is selected</extracomment>
        <translation>geen</translation>
    </message>
</context>
<context>
    <name>DlgPrefEQDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="14"/>
        <source>Equalizer Preferences</source>
        <translation>Equalizer Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="370"/>
        <source>Reset equalizers on track load</source>
        <translation>Stel equalizers opnieuw in bij het laden van de track</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="367"/>
        <source>Resets the equalizers to their default values when loading a track.</source>
        <translation>Stelt de equalizers opniuew in naar hun standaard waarden bij het laden van een track.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="20"/>
        <source>Equalizer Rack</source>
        <translation>Equalizer Rack</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="26"/>
        <source>Only allow EQ knobs to control EQ-specific effects</source>
        <translation>Sta enkel EQ-knoppen toe om EQ-specifieke effecten te besturen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="32"/>
        <source>Uncheck to allow any effect to be loaded into the EQ knobs.</source>
        <translation>Afvinken om toe te staan dat elk effect in de EQ-knoppen wordt geladen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="39"/>
        <source>Use the same EQ filter for all decks</source>
        <translation>Gebruik dezelfde EQ-filter voor alle spelers</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="45"/>
        <source>Uncheck to allow different decks to use different EQ effects.</source>
        <translation>Vink af om toe te staan dat verschillende speler andere EQ-effecten mogen gebruiken.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="77"/>
        <source>Equalizer Plugin</source>
        <translation>Equalizer Plugin</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="90"/>
        <source>Quick Effect</source>
        <translation>Snel Effect</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="103"/>
        <source>High Shelf EQ</source>
        <translation>High Shelf EQ</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="150"/>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="238"/>
        <source>16 Hz</source>
        <translation>16 Hz</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="305"/>
        <source>Master EQ</source>
        <translation>Hoofd EQ</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="316"/>
        <source>Reset Parameter</source>
        <translation>Parameter resetten</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="361"/>
        <source>Miscellaneous</source>
        <translation>Diversen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="377"/>
        <source>Resets the deck gain to unity when loading a track.</source>
        <translation>Stelt de gain van de speler opnieuw in naar de eenheid bij het laden van een track.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="380"/>
        <source>Reset gain on track load</source>
        <translation>Gain opnieuw instellen bij laden track</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="387"/>
        <source>Bypass EQ effect processing</source>
        <translation>Omzeil EQ-effectverwerking</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="390"/>
        <source>When checked, EQs are not processed, improving performance on slower computers.</source>
        <translation>Indien aangevinkt, worden EQ&apos;s niet verwerkt, wat de prestaties op langzamere computers verbetert.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="176"/>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="290"/>
        <source>20.05 kHz</source>
        <translation>20.05 kHz</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeqdlg.ui" line="202"/>
        <source>Low Shelf EQ</source>
        <translation>Low Shelf EQ</translation>
    </message>
</context>
<context>
    <name>DlgPrefEffectsDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeffectsdlg.ui" line="20"/>
        <source>Effects Preferences</source>
        <translation>Voorkeuren voor Effecten</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeffectsdlg.ui" line="42"/>
        <source>Available Effects</source>
        <translation>Beschikbare Effecten</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeffectsdlg.ui" line="91"/>
        <source>Effect Info</source>
        <translation>Effect Info</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeffectsdlg.ui" line="135"/>
        <source>Version:</source>
        <translation>Versie:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeffectsdlg.ui" line="183"/>
        <source>Description:</source>
        <translation>Omschrijving:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeffectsdlg.ui" line="199"/>
        <source>Author:</source>
        <translation>Auteur:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeffectsdlg.ui" line="228"/>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeffectsdlg.ui" line="244"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
</context>
<context>
    <name>DlgPrefInterface</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterface.cpp" line="69"/>
        <source>The minimum size of the selected skin is bigger than your screen resolution.</source>
        <translation>De minimale grootte van de geselecteerde skin is groter dan uw schermresolutie.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterface.cpp" line="157"/>
        <source>Allow screensaver to run</source>
        <translation>Toestaan om screensaver laten draaien</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterface.cpp" line="159"/>
        <source>Prevent screensaver from running</source>
        <translation>Voorkom dat screensaver wordt uitgevoerd</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterface.cpp" line="161"/>
        <source>Prevent screensaver while playing</source>
        <translation>Voorkom screensaver tijdens afspelen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterface.cpp" line="192"/>
        <source>This skin does not support color schemes</source>
        <translation>Deze skin ondersteunt geen kleurenschema&apos;s</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterface.cpp" line="304"/>
        <source>Information</source>
        <translation>Informatie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefinterface.cpp" line="305"/>
        <source>Mixxx must be restarted before the new locale setting will take effect.</source>
        <translation>Mixxx moet opnieuw worden gestart voordat de nieuwe locale-instelling van kracht wordt.</translation>
    </message>
</context>
<context>
    <name>DlgPrefKeyDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="14"/>
        <source>Key Notation Format Settings</source>
        <translation>Formaatinstellingen voor Kay Notation</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="20"/>
        <source>When key detection is enabled, Mixxx detects the musical key of your tracks 
and allows you to pitch adjust them for harmonic mixing.</source>
        <translation>Wanneer toonaard detectie is ingeschakeld, detecteert Mixxx de muzieksleutel van uw tracks
en stelt u in staat om de pitch aan te passen voor harmonisch mixen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="24"/>
        <source>Enable Key Detection</source>
        <translation>Schakel Toonaard Detectie in</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="31"/>
        <source>Choose Analyzer</source>
        <translation>Kies Analysator</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="52"/>
        <source>Key Analyzer:</source>
        <translation>Key Analysator:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="71"/>
        <source>Choose between different algorithms to detect keys.</source>
        <translation>Kies tussen verschillende algoritmen om toonaarden te detecteren.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="84"/>
        <source>Analyzer Settings</source>
        <translation>Instellingen Analysator</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="90"/>
        <source>Enable Fast Analysis (For slow computers, may be less accurate)</source>
        <translation>Snelle Analyse Inschakelen (voor langzame computers is dit mogelijk minder nauwkeurig)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="97"/>
        <source>Re-analyze keys when settings change or 3rd-party keys are present</source>
        <translation>Analyseer toonaarden opnieuw wanneer instellingen veranderen of toonaarden van derden aanwezig zijn</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="113"/>
        <source>Key Notation</source>
        <translation>Key Notatie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="121"/>
        <source>Lancelot</source>
        <translation>Lancelot</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="128"/>
        <source>OpenKey</source>
        <translation>OpenKey</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="135"/>
        <source>Traditional</source>
        <translation>Traditioneel</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="142"/>
        <source>Custom</source>
        <translation>Maatwerk</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="153"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="176"/>
        <source>Bb</source>
        <translation>Bb</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="199"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="222"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="245"/>
        <source>Db</source>
        <translation>Db</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="268"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="291"/>
        <source>Eb</source>
        <translation>Eb</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="314"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="337"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="360"/>
        <source>F#</source>
        <translation>F#</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="383"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="406"/>
        <source>Ab</source>
        <translation>Ab</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="429"/>
        <source>Am</source>
        <translation>Am</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="452"/>
        <source>Bbm</source>
        <translation>Bbm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="475"/>
        <source>Bm</source>
        <translation>Bm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="498"/>
        <source>Cm</source>
        <translation>Cm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="521"/>
        <source>C#m</source>
        <translation>C#m</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="544"/>
        <source>Dm</source>
        <translation>Dm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="567"/>
        <source>Ebm</source>
        <translation>Ebm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="590"/>
        <source>Em</source>
        <translation>Em</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="613"/>
        <source>Fm</source>
        <translation>Fm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="636"/>
        <source>F#m</source>
        <translation>F#m</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="659"/>
        <source>Gm</source>
        <translation>Gm</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefkeydlg.ui" line="682"/>
        <source>G#m</source>
        <translation>G#m</translation>
    </message>
</context>
<context>
    <name>DlgPrefLV2Dlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflv2dlg.ui" line="14"/>
        <source>Equalizer Preferences</source>
        <translation>Equalizer Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflv2dlg.ui" line="30"/>
        <source>Discovered LV2 effects</source>
        <translation>Ontdekte LV2 effects</translation>
    </message>
</context>
<context>
    <name>DlgPrefLibrary</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="107"/>
        <source>Music Directory Added</source>
        <translation>Muziekmap toegevoegd</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="108"/>
        <source>You added one or more music directories. The tracks in these directories won&apos;t be available until you rescan your library. Would you like to rescan now?</source>
        <translation>U hebt een of meer muziekmappen toegevoegd. De tracks in deze mappen zijn pas beschikbaar als u uw bibliotheek opnieuw scant. Wilt u nu opnieuw scannen?</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="112"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="214"/>
        <source>Choose a music directory</source>
        <translation>Kies een map voor muziek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="229"/>
        <source>Confirm Directory Removal</source>
        <translation>Bevestig Verwijdering Map</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="231"/>
        <source>Mixxx will no longer watch this directory for new tracks. What would you like to do with the tracks from this directory and subdirectories?&lt;ul&gt;&lt;li&gt;Hide all tracks from this directory and subdirectories.&lt;/li&gt;&lt;li&gt;Delete all metadata for these tracks from Mixxx permanently.&lt;/li&gt;&lt;li&gt;Leave the tracks unchanged in your library.&lt;/li&gt;&lt;/ul&gt;Hiding tracks saves their metadata in case you re-add them in the future.</source>
        <translation>Mixxx zal deze map niet langer in de gaten houden voor nieuwe tracks. Wat zou je willen doen met de tracks uit deze map en submappen?&lt;ul&gt;&lt;li&gt;Verberg alle tracks uit deze directory en submappen. &lt;/li&gt;&lt;li&gt;Verwijder permanent alle metadata van deze tracks uit Mixxx.&lt;/li&gt;&lt;li&gt;Laat de tracks ongemoeid in je bibliotheek.&lt;/li&gt;&lt;/ul&gt;Tracks verbergen zal wel de metadata blijven bewaren mocht je ze in de toekomst opnieuw willen toevoegen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="242"/>
        <source>Metadata means all track details (artist, title, playcount, etc.) as well as beatgrids, hotcues, and loops. This choice only affects the Mixxx library. No files on disk will be changed or deleted.</source>
        <translation>Metadata betekent alle trackdetails (artiest, titel, playcount, etc.) evenals beatgrids, hotcues en loops. Deze keuze heeft alleen betrekking op de Mixxx-bibliotheek. Er worden geen bestanden op schijf gewijzigd of verwijderd.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="250"/>
        <source>Hide Tracks</source>
        <translation>Verberg Tracks</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="252"/>
        <source>Delete Track Metadata</source>
        <translation>Verwijder Metadata</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="254"/>
        <source>Leave Tracks Unchanged</source>
        <translation>Laat tracks ongewijzigd</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="296"/>
        <source>Relink music directory to new location</source>
        <translation>Koppel de muziekmap opnieuw aan een nieuwe locatie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrary.cpp" line="373"/>
        <source>Select Library Font</source>
        <translation>Selecteer bibliotheeklettertype</translation>
    </message>
</context>
<context>
    <name>DlgPrefLibraryDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="20"/>
        <source>Library</source>
        <translation>Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="38"/>
        <source>Music Directories:</source>
        <translation>Muziekmappen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="54"/>
        <source>If removed, Mixxx will no longer watch this directory and its subdirectories for new tracks.</source>
        <translation>Indien verwijderd, zal Mixxx deze map en zijn submappen niet langer in de gaten houden voor nieuwe tracks.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="57"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="83"/>
        <source>Add a directory where your music is stored. Mixxx will watch this directory and its subdirectories for new tracks.</source>
        <translation>Voeg een map toe waar uw muziek is opgeslagen. Mixxx zal naar deze map en zijn submappen kijken voor nieuwe tracks.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="86"/>
        <source>Add</source>
        <translation>Toevoegen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="93"/>
        <source>If an existing music directory is moved, Mixxx doesn&apos;t know where to find the audio files in it. Choose Relink to select the music directory in its new location. &lt;br/&gt; This will re-establish the links to the audio files in the Mixxx library.</source>
        <translation>Als een bestaande muziekmap wordt verplaatst, weet Mixxx niet waar de audiobestanden te vinden zijn. Kies Opnieuw koppelen om de muziekmap op de nieuwe locatie te selecteren. &lt;br/&gt; Hierdoor worden de links naar de audiobestanden in de Mixxx-bibliotheek hersteld.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="96"/>
        <source>Relink</source>
        <extracomment>This will re-establish the links to the audio files in the Mixxx database if you move an music directory to a new location.</extracomment>
        <translation>Opnieuw koppelen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="106"/>
        <source>Audio File Formats</source>
        <translation>Audiobestandsindelingen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="115"/>
        <source>Additional Format Plugins:</source>
        <translation>Extra opmaak plug-ins:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="135"/>
        <source>Built-in</source>
        <translation>Ingebouwd</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="142"/>
        <source>Available Online...</source>
        <translation>Beschikbaar Online...</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="149"/>
        <source>Loaded Plugins:</source>
        <translation>Geladen Plugins:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="156"/>
        <source>None</source>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="166"/>
        <source>Track Metadata Synchronization</source>
        <translation>Track Metadata Synchronisatie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="172"/>
        <source>Export: Write modified track metadata from the library into file tags</source>
        <translation>Exporteren: schrijf gewijzigde track metadata uit de bibliotheek naar bestandslabels</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="182"/>
        <source>Miscellaneous</source>
        <translation>Diversen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="238"/>
        <source>Library Font:</source>
        <translation>Lettertype bibliotheek:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="212"/>
        <source>Library Row Height:</source>
        <translation>Rijhoogte Bibliotheek:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="188"/>
        <source>Rescan library on start-up</source>
        <translation>Scan bibliotheek opnieuw bij opstarten</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="308"/>
        <source>Add track to Auto DJ Queue (top)</source>
        <translation>Voeg track toe aan AutoDJ-wachtrij (bovenaan)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="301"/>
        <source>Add track to Auto DJ Queue (bottom)</source>
        <translation>Voeg track toe aan AutoDJ-wachtrij (onderaan)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="205"/>
        <source>Use relative paths for playlist export if possible</source>
        <translation>Gebruik indien mogelijk relatieve paden bij exporteren van afspeellijsten</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="255"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="222"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="198"/>
        <source>Edit metadata after clicking selected track</source>
        <translation>Bewerk metadata nadat u op het geselecteerde nummer hebt geklikt</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="262"/>
        <source>Search-as-you-type timeout:</source>
        <translation>Zoeken-bij-typen timeout:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="272"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="285"/>
        <source>Track Load Action</source>
        <extracomment>Sets default action when double-clicking a track in the library.</extracomment>
        <translation>Actie bij laden track</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="291"/>
        <source>Load track to next available deck</source>
        <translation>Laad track naar het volgende beschikbare speler</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="318"/>
        <source>External Libraries</source>
        <translation>Externe Bibliotheken</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="324"/>
        <source>You will need to restart Mixxx for these settings to take effect.</source>
        <translation>U moet Mixxx opnieuw opstarten om deze instellingen te activeren.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="331"/>
        <source>Show Rhythmbox Library</source>
        <translation>Toon Rhythmbox Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="341"/>
        <source>Show Banshee Library</source>
        <translation>Toon Banshee Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="351"/>
        <source>Show iTunes Library</source>
        <translation>Toon iTunes Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="361"/>
        <source>Show Traktor Library</source>
        <translation>Toon Traktor Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflibrarydlg.ui" line="371"/>
        <source>All external libraries shown are write protected.</source>
        <translation>Alle weergegeven externe bibliotheken zijn tegen schrijven beveiligd.</translation>
    </message>
</context>
<context>
    <name>DlgPrefModplug</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="14"/>
        <source>Modplug Preferences</source>
        <translation>Modplug Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="403"/>
        <source>All settings take effect on next track load. Currently loaded tracks are not affected. For an explanation of these settings, see the &lt;a href=&quot;http://wiki.openmpt.org/Manual:_Setup/Player&quot;&gt;OpenMPT manual&lt;/a&gt;.</source>
        <translation>Alle instellingen worden van kracht bij het laden van de volgende track. Reeds geladen tracks worden niet beïnvloed. Voor een uitleg van deze instellingen, zie de &lt;a href=&quot;http://wiki.openmpt.org/Manual:_Setup/Player&quot;&gt;OpenMPT handleiding&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="156"/>
        <source>Maximum Number of Mixing Channels:</source>
        <translation>Maximaal aantal mengkanalen:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="23"/>
        <source>Show Advanced Settings</source>
        <translation>Toon Geavanceerde Instellingen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="176"/>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="190"/>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="292"/>
        <source>Low</source>
        <translation>Laag</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="183"/>
        <source>Reverb Delay:</source>
        <translation>Reverb vertraging:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="197"/>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="271"/>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="313"/>
        <source>High</source>
        <translation>Hoog</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="220"/>
        <source>None</source>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="227"/>
        <source>Bass Expansion</source>
        <translation>Bas-uitbreiding</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="250"/>
        <source>Bass Range:</source>
        <translation>Bas-bereik:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="257"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="264"/>
        <source>Front/Rear Delay:</source>
        <translation>Voor/Achter Vertraging:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="278"/>
        <source>Pro-Logic Surround</source>
        <translation>Pro-Logic Surround</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="285"/>
        <source>Full</source>
        <translation>Volledig</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="299"/>
        <source>Reverb</source>
        <translation>Reverb</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="306"/>
        <source>Stereo separation</source>
        <translation>Stereo scheiding</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="320"/>
        <source>10Hz</source>
        <translation>10Hz</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="327"/>
        <source>10ms</source>
        <translation>10ms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="334"/>
        <source>256</source>
        <translation>256</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="341"/>
        <source>5ms</source>
        <translation>5ms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="348"/>
        <source>100Hz</source>
        <translation>100Hz</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="355"/>
        <source>250ms</source>
        <translation>250ms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="362"/>
        <source>50ms</source>
        <translation>50ms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="369"/>
        <source>Noise reduction</source>
        <translation>Ruisonderdrukking</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="387"/>
        <source>Hints</source>
        <translation>Tips</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="393"/>
        <source>Module files are decoded at once and kept in RAM to allow for seeking and smooth operation in Mixxx. About 10MB of RAM are required for 1 minute of audio.</source>
        <translation>Modulebestanden worden in één keer gedecodeerd en in het RAM-geheugen bewaard om zoeken en een soepele verwerking in Mixxx mogelijk te maken. Ongeveer 10 MB aan RAM is vereist voor 1 minuut audiogeluid.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="425"/>
        <source>Decoding options for libmodplug, a software library for loading and rendering module files (MOD music, tracker music).</source>
        <translation>Decoderingsopties voor libmodplug, een softwarebibliotheek voor het laden en renderen van modulebestanden (MOD-muziek, tracker-muziek).</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="428"/>
        <source>Decoding Options</source>
        <translation>Decodeer-opties</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="485"/>
        <source>Resampling mode (interpolation)</source>
        <translation>Resampling-modus (interpolatie)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="512"/>
        <source>Enable oversampling</source>
        <translation>Oversampling inschakelen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="462"/>
        <source>Nearest (very fast, extremely bad quality)</source>
        <translation>Dichtstbijzijnde (zeer snelle, extreem slechte kwaliteit)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="467"/>
        <source>Linear (fast, good quality)</source>
        <translation>Linear (snel, goede kwaliteit)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="472"/>
        <source>Cubic Spline (high quality)</source>
        <translation>Cubic Spline (hoge kwaliteit)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="477"/>
        <source>8-tap FIR (extremely high quality)</source>
        <translation>8-tap FIR (extreem hoge kwaliteit)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefmodplugdlg.ui" line="505"/>
        <source>Memory limit for single track (MB)</source>
        <translation>Geheugenlimiet voor enkele track (MB)</translation>
    </message>
</context>
<context>
    <name>DlgPrefNoVinylDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefnovinyldlg.ui" line="14"/>
        <source>Vinyl Control (Disabled) Preferences</source>
        <extracomment>Using a version with the Vinyl Control feature disabled, an alternative preferences window is displayed.</extracomment>
        <translation>Voorkeuren voor vinylbesturing (Uigeschakelde)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefnovinyldlg.ui" line="26"/>
        <source>&lt;b&gt;This version of Mixxx does not support vinyl control.&lt;/b&gt; &lt;br&gt; Please visit &lt;a href=&quot;http://mixxx.org&quot;&gt;Mixxx.org&lt;/a&gt; for more information.</source>
        <translation>&lt;b&gt;Deze versie van Mixxx ondersteund geen vinylbesturing.&lt;/b&gt; &lt;br&gt; Ga naar &lt;a href=&quot;http://mixxx.org&quot;&gt;Mixxx.org&lt;/a&gt; voor meer informatie.</translation>
    </message>
</context>
<context>
    <name>DlgPrefRecord</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecord.cpp" line="202"/>
        <source>Choose recordings directory</source>
        <translation>Kies de opnamemap</translation>
    </message>
</context>
<context>
    <name>DlgPrefRecordDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="14"/>
        <source>Recording Preferences</source>
        <translation>Opnamevoorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="33"/>
        <source>Recordings Directory</source>
        <translation>Opnamemap</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="51"/>
        <source>Recordings Directory:</source>
        <translation>Opnamemap:</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="80"/>
        <source>Browse...</source>
        <translation>Bladeren...</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="103"/>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="119"/>
        <source>Quality</source>
        <translation>Kwaliteit</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="277"/>
        <source>Tags</source>
        <translation>Labels</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="289"/>
        <source>Title</source>
        <translation>titel</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="311"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="333"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="233"/>
        <source>Miscellaneous</source>
        <translation>Diversen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="90"/>
        <source>Output File Format</source>
        <translation>Bestandsindeling voor Uitvoer</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="148"/>
        <source>Compression</source>
        <translation>Compressie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="161"/>
        <source>Lossy</source>
        <translation>Lossy</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="168"/>
        <source>Compression Level</source>
        <translation>Compressie Niveau</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="178"/>
        <source>Lossless</source>
        <translation>Lossless</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="239"/>
        <source>Create a CUE file</source>
        <translation>Maak een CUE-bestand</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="255"/>
        <source>File Splitting</source>
        <translation>Bestand splitsen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefrecorddlg.ui" line="261"/>
        <source>Split recordings at</source>
        <translation>Splits opname op</translation>
    </message>
</context>
<context>
    <name>DlgPrefReplayGain</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygain.cpp" line="137"/>
        <source>%1 LUFS (adjust by %2 dB)</source>
        <translation>%1 LUFS (aanpassen met %2 dB)</translation>
    </message>
</context>
<context>
    <name>DlgPrefReplayGainDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="14"/>
        <source>Normalization Preferences</source>
        <translation>Voorkeuren Normalisatie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="20"/>
        <source>ReplayGain Loudness Normalization</source>
        <translation>ReplayGain Loudness Normalisatie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="26"/>
        <source>Apply loudness normalization to loaded tracks.</source>
        <translation>Pas loudness normalisatie toe op geladen tracks.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="29"/>
        <source>Apply ReplayGain</source>
        <translation>Pas ReplayGain toe</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="91"/>
        <source>-30 LUFS</source>
        <translation>-30 LUFS</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="131"/>
        <source>-6 LUFS</source>
        <translation>-6 LUFS</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="140"/>
        <source>When ReplayGain is enabled, adjust tracks lacking ReplayGain information by this amount.</source>
        <translation>Als ReplayGain is ingeschakeld, past u de tracks zonder ReplayGain-informatie met deze hoeveelheid aan.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="143"/>
        <source>Initial boost without ReplayGain data</source>
        <translation>Initiële boost zonder ReplayGain-gegevens</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="36"/>
        <source>For tracks with ReplayGain, adjust the target loudness to this LUFS value (Loudness Units relative to Full Scale).</source>
        <translation>Pas voor tracks met ReplayGain het doelvolume aan naar deze LUFS-waarde (Loudness Units ten opzichte van Full Scale).</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="39"/>
        <source>Target loudness</source>
        <translation>Doelvolume</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="174"/>
        <source>-12 dB</source>
        <translation>-12 dB</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="226"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="232"/>
        <source>ReplayGain 2.0 (ITU-R BS.1770)</source>
        <translation>ReplayGain 2.0 (ITU-R BS.1770)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="239"/>
        <source>ReplayGain 1.0</source>
        <translation>ReplayGain 1.0</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="246"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="253"/>
        <source>Re-analyze and override an existing value </source>
        <translation>Analyseer opnieuw en overschrijf een bestaande waarde</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="269"/>
        <source>ReplayGain targets a reference loudness of -18 LUFS (Loudness Units relative to Full Scale). You may increase it if you find Mixxx is too quiet or reduce it if you find that your tracks are clipping. You may also want to decrease the volume of unanalyzed tracks if you find they are often louder than ReplayGained tracks. For podcasting a loudness of -16 LUFS is recommended.

The loudness target is approximate and assumes track pregain and master output level are unchanged.</source>
        <translation>ReplayGain richt zich op een referentie-volume van -18 LUFS (Loudness Units ten opzichte van Full Scale). Je kunt het verhogen als je vindt dat Mixxx te stil is of verlagen als je oversturing opmerkt in je tracks. Mogelijk wilt u ook het volume van niet-geanalyseerde tracks verlagen als u merkt dat ze vaak luider zijn dan ReplayGained-tracks. Voor podcasts wordt een referentie-volume van -16 LUFS aanbevolen.

Het doelvolume is bij benadering en gaat ervan uit dat track pregain en master output level onveranderd zijn.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="284"/>
        <source>When an unanalyzed track is playing, Mixxx will avoid an abrupt volume change by not applying a newly calculated ReplayGain value.</source>
        <translation>Wanneer een niet-geanalyseerd nummer wordt afgespeeld, zal Mixxx een abrupte volumewijziging vermijden door geen nieuw berekende ReplayGain-waarde toe te passen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="214"/>
        <source>+12 dB</source>
        <translation>+12 dB</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefreplaygaindlg.ui" line="263"/>
        <source>Hints</source>
        <translation>Tips</translation>
    </message>
</context>
<context>
    <name>DlgPrefSound</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="49"/>
        <source>None</source>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="59"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="70"/>
        <source>Default (long delay)</source>
        <translation>Standaard (lange vertraging)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="71"/>
        <source>Experimental (no delay)</source>
        <translation>Standaard (lange vertraging)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="72"/>
        <source>Disabled (short delay)</source>
        <translation>Uitgeschakeld (korte vertraging)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="78"/>
        <source>Soundcard Clock</source>
        <translation>Geluidskaart Klok</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="79"/>
        <source>Network Clock</source>
        <translation>Netwerk Klok</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="112"/>
        <source>Master output only</source>
        <translation>Hoofduitgang alleen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="114"/>
        <source>Master and booth outputs</source>
        <translation>Master- en booth-uitgangen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="116"/>
        <source>Direct monitor (recording and broadcasting only)</source>
        <translation>Directe monitor (enkel opnemen en uitzenden)</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="164"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="165"/>
        <source>Enabled</source>
        <translation>Ingeschakeld</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="172"/>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="173"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="701"/>
        <source>Microphone inputs are out of time in the record &amp; broadcast signal compared to what you hear.</source>
        <translation>Microfooningangen zijn niet synchroon met het opname- en uitzendsignaal in vergelijking met wat je hoort.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="702"/>
        <source>Measure round trip latency and enter it above for Microphone Latency Compensation to align microphone timing.</source>
        <translation>Meet de Round Trip Latency en voer het hierboven in voor Microphone Latency Compensation om de microfoon timing uit te lijnen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="703"/>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="710"/>
        <source>Refer to the Mixxx User Manual for details.</source>
        <translation>Raadpleeg de Mixxx-gebruikershandleiding voor meer informatie.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="708"/>
        <source>Configured latency has changed.</source>
        <translation>De geconfigureerde latency is gewijzigd.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="709"/>
        <source>Remeasure round trip latency and enter it above for Microphone Latency Compensation to align microphone timing.</source>
        <translation>Meet opnieuw de Round Trip Latency en voer het hierboven in voor Microphone Latency Compensation om de microfoon timing uit te lijnen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="187"/>
        <source>Realtime scheduling is enabled.</source>
        <translation>Realtime plannen is ingeschakeld.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="534"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsound.cpp" line="238"/>
        <source>Configuration error</source>
        <translation>Configuratiefout</translation>
    </message>
</context>
<context>
    <name>DlgPrefSoundDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="14"/>
        <source>Sound Hardware Preferences</source>
        <translation>Voorkeuren Geluidsapparatuur</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="22"/>
        <source>Sound API</source>
        <translation>Geluid API</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="35"/>
        <source>Sample Rate</source>
        <translation>Sample Rate</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="48"/>
        <source>Audio Buffer</source>
        <translation>Audio Buffer</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="71"/>
        <source>Engine Clock</source>
        <translation>Engine Klok</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="78"/>
        <source>Use soundcard clock for live audience setups and lowest latency.&lt;br&gt;Use network clock for broadcasting without a live audience.</source>
        <translation>Gebruik de geluidskaartklok voor live publieksopstellingen en de laagste latency.&lt;br&gt;Gebruik een netwerkklok voor uitzendingen zonder een live publiek.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="121"/>
        <source>Microphone Monitor Mode</source>
        <translation>Microfoon monitormodus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="128"/>
        <source>Microphone Latency Compensation</source>
        <translation>Compensatie voor microfoonvertraging</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="174"/>
        <source>Headphone Delay</source>
        <translation>Hoofdtelefoon Delay</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="135"/>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="158"/>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="181"/>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="204"/>
        <source> ms</source>
        <extracomment>milliseconds</extracomment>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="197"/>
        <source>Booth Delay</source>
        <translation>Booth Vertraging</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="345"/>
        <source>Enable Realtime scheduling (currently disabled), see the &lt;a href=&quot;http://mixxx.org/wiki/doku.php/adjusting_audio_latency&quot;&gt;Mixxx Wiki&lt;/a&gt;.</source>
        <translation>Schakel Realtime planning in (momenteel uitgeschakeld), zie de&lt;a href=&quot;http://mixxx.org/wiki/doku.php/adjusting_audio_latency&quot;&gt;Mixxx Wiki&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="388"/>
        <source>20 ms</source>
        <translation>20 ms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="395"/>
        <source>Buffer Underflow Count</source>
        <translation>Buffer Underflow Teller</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="405"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="151"/>
        <source>Master Delay</source>
        <translation>Master Delay</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="85"/>
        <source>Keylock/Pitch-Bending Engine</source>
        <translation>Keylock/Pitch-Bending Engine</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="61"/>
        <source>Multi-Soundcard Synchronization</source>
        <translation>Synchronisatie van meervoudige geluidskaarten</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="98"/>
        <source>Master Mix</source>
        <translation>Master Mix</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="111"/>
        <source>Master Output Mode</source>
        <translation>Master Uitvoer Modus</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="270"/>
        <source>Output</source>
        <translation>Uitvoer</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="293"/>
        <source>Input</source>
        <translation>Invoer</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="358"/>
        <source>The &lt;a href=&quot;http://mixxx.org/wiki/doku.php/hardware_compatibility&quot;&gt;Mixxx DJ Hardware Guide&lt;/a&gt; lists sound cards and controllers you may want to consider for using Mixxx.</source>
        <translation>De &lt;a href=&quot;http://mixxx.org/wiki/doku.php/hardware_compatibility&quot;&gt;Mixxx DJ Hardware Handleiding&lt;/a&gt; l bevat een lijst met geluidskaarten en controllers die u zou kunnen overwegen om te gebruiken met Mixxx.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="378"/>
        <source>System Reported Latency</source>
        <translation>Door systeem gerapporteerde Latency</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="325"/>
        <source>Enlarge your audio buffer if the underflow counter is increasing or you hear pops during playback.</source>
        <translation>Vergroot je geluidsbuffer als de teller voor bufferleegloop verhoogt of als je korte onderbrekingen hoort tijdens het afspelen.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="319"/>
        <source>Hints and Diagnostics</source>
        <translation>Tips en diagnostische gegevens</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="335"/>
        <source>Downsize your audio buffer to improve Mixxx&apos;s responsiveness.</source>
        <translation>Verklein uw audiobuffer om het reactievermogen van Mixxx te verbeteren.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounddlg.ui" line="238"/>
        <source>Query Devices</source>
        <translation>Vraag apparaten op</translation>
    </message>
</context>
<context>
    <name>DlgPrefSoundItem</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounditem.cpp" line="43"/>
        <source>None</source>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounditem.cpp" line="114"/>
        <source>Channel %1</source>
        <translation>Kanaal %1</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounditem.cpp" line="116"/>
        <source>Channels %1 - %2</source>
        <translation>Kanalen %1 - %2</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounditem.ui" line="14"/>
        <source>Sound Item Preferences</source>
        <extracomment>Constructs new sound items inside the Sound Hardware Preferences, representing an AudioPath and SoundDevice</extracomment>
        <translation>Voorkeuren voor geluidselement</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefsounditem.ui" line="23"/>
        <source>Type (#)</source>
        <translation>Type (#)</translation>
    </message>
</context>
<context>
    <name>DlgPrefVinylDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="592"/>
        <source>Input</source>
        <translation>Invoer</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="20"/>
        <source>Vinyl Configuration</source>
        <translation>Vinyl Configuratie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="38"/>
        <source>Deck 1 Vinyl Type</source>
        <translation>Vinyltype speler 1</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="177"/>
        <source>Deck 2 Vinyl Type</source>
        <translation>Vinyltype speler 2</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="316"/>
        <source>Deck 3 Vinyl Type</source>
        <translation>Vinyltype speler 3</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="455"/>
        <source>Deck 4 Vinyl Type</source>
        <translation>Vinyltype speler 4</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="582"/>
        <source>Show Signal Quality in Skin</source>
        <translation>Toon signaalkwaliteit in het thema</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="155"/>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="294"/>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="433"/>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="572"/>
        <source>seconds</source>
        <translation>seconden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="14"/>
        <source>Vinyl Control Preferences</source>
        <translation>Voorkeuren voor Vinylbediening</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="111"/>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="250"/>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="389"/>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="528"/>
        <source>Lead-in</source>
        <translation>Inleiden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="598"/>
        <source>Turntable Input Signal Boost</source>
        <translation>Verhoog Invoersignaal Platenspeler </translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="626"/>
        <source>0 dB</source>
        <translation>0 dB</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="675"/>
        <source>44 dB</source>
        <translation>44 dB</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="690"/>
        <source>Signal Quality</source>
        <translation>Signaalkwaliteit</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="718"/>
        <source>http://www.xwax.co.uk</source>
        <translation>http://www.xwax.co.uk</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="721"/>
        <source>Powered by xwax</source>
        <translation>Powered by xwax</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="731"/>
        <source>Hints</source>
        <translation>Tips</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefvinyldlg.ui" line="747"/>
        <source>Select sound devices for Vinyl Control in the Sound Hardware pane.</source>
        <translation>Selecteer geluidsapparaten voor vinylbesturing in het deelvenster Geluidsapparatuur.</translation>
    </message>
</context>
<context>
    <name>DlgPrefWaveform</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveform.cpp" line="19"/>
        <source>Filtered</source>
        <translation>Gefilterd</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveform.cpp" line="20"/>
        <source>HSV</source>
        <translation>HSV</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveform.cpp" line="21"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveform.cpp" line="95"/>
        <source>OpenGL not available</source>
        <translation>OpenGL niet beschikbaar</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveform.cpp" line="236"/>
        <source>dropped frames</source>
        <translation>verloren frames</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveform.cpp" line="269"/>
        <source>Cached waveforms occupy %1 MiB on disk.</source>
        <translation>Waveforms in cache gebruiken %1 Mb op schijf</translation>
    </message>
</context>
<context>
    <name>DlgPrefWaveformDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="14"/>
        <source>Waveform Preferences</source>
        <translation>Voorkeuren Waveforms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="112"/>
        <source>Frame rate</source>
        <translation>Frame-snelheid</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="70"/>
        <source>Displays which OpenGL version is supported by the current platform.</source>
        <translation>Geeft weer welke OpenGL-versie wordt ondersteund door het huidige platform.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="63"/>
        <source>Normalize waveform overview</source>
        <translation>Normaliseer Waveform-overzicht</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="102"/>
        <source>Average frame rate</source>
        <translation>Gemiddelde framesnelheid</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="22"/>
        <source>Visual gain</source>
        <translation>Visuele versterking</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="399"/>
        <source>Default zoom level</source>
        <extracomment>Waveform zoom</extracomment>
        <translation>Standaard zoomniveau</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="329"/>
        <source>Displays the actual frame rate.</source>
        <translation>Geeft de werkelijke framesnelheid weer.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="173"/>
        <source>Visual gain of the middle frequencies</source>
        <translation>Visuele versterking van de middenfrequenties</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="339"/>
        <source>End of track warning</source>
        <translation>Track-einde waarschuwing</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="139"/>
        <source>OpenGL status</source>
        <translation>OpenGL status</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="80"/>
        <source>Highlight the waveforms when the last seconds of a track remains.</source>
        <translation>Markeer de waveforms bij de laatste seconden van een track.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="83"/>
        <source> seconds</source>
        <translation> seconden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="269"/>
        <source>Low</source>
        <translation>Laag</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="205"/>
        <source>Middle</source>
        <translation>Midden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="218"/>
        <source>Global</source>
        <translation>Globaal</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="231"/>
        <source>Visual gain of the high frequencies</source>
        <translation>Visuele versterking van de hoge frequenties</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="250"/>
        <source>Visual gain of the low frequencies</source>
        <translation>Visuele versterking van de lage frequenties</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="192"/>
        <source>High</source>
        <translation>Hoog</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="158"/>
        <source>Waveform type</source>
        <translation>Waveform type</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="282"/>
        <source>Global visual gain</source>
        <translation>Globale visuele versterking</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="303"/>
        <source>The waveform overview shows the waveform envelope of the entire track.
Select from different types of displays for the waveform overview, which differ primarily in the level of detail shown in the waveform.</source>
        <translation>Het waveform overzicht toont de waveform enveloppe van de hele track.
Kies uit verschillende soorten weergaven voor het waveform overzicht, die voornamelijk verschillen in het detailniveau dat in de waveform wordt weergegeven.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="311"/>
        <source>The waveform shows the waveform envelope of the track near the current playback position.
Select from different types of displays for the waveform, which differ primarily in the level of detail shown in the waveform.</source>
        <translation>De waveform toont de waveform enveloppe van de track nabij de huidige afspeelpositie.
Kies uit verschillende soorten weergaven voor de waveform, die voornamelijk verschillen in het detailniveau dat in de waveform wordt weergegeven.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="322"/>
        <source>Waveform overview type</source>
        <translation>Waveform overzichtstype</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="383"/>
        <source> fps</source>
        <translation> fps</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="412"/>
        <source>Synchronize zoom level across all waveform displays.</source>
        <translation>Synchroniseer het zoomniveau over alle waveform weergaven.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="415"/>
        <source>Synchronize zoom level across all waveforms</source>
        <translation>Synchroniseer het zoomniveau over alle waveforms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="422"/>
        <source>Caching</source>
        <translation>Caching</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="441"/>
        <source>Mixxx caches the waveforms of your tracks on disk the first time you load a track. This reduces CPU usage when you are playing live but requires extra disk space.</source>
        <translation>Mixxx buffert de waveforms van je tracks in de cache op schijf wanneer een track voor de eerste keer laadt. Dit vermindert het CPU-gebruik tijdens het live spelen, maar vereist extra schijfruimte.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="461"/>
        <source>Enable waveform caching</source>
        <translation>Schakel waveform caching in</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="468"/>
        <source>Generate waveforms when analyzing library</source>
        <translation>Genereer waveforms bij het analyseren van de bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="483"/>
        <source>Beat grid opacity</source>
        <translation>Beat-raster opaciteit</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="506"/>
        <source>Set amount of opacity on beat grid lines.</source>
        <translation>Stel de opaciteitswaarde in voor beat-rasterlijnen</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="509"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="522"/>
        <source>Play marker position</source>
        <translation>Positie afspeelmarkering</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="529"/>
        <source>Moves the play marker position on the waveforms to the left, right or center (default).</source>
        <translation>Verplaatst de speelmarkeringspositie op de waveforms naar links, rechts of midden (standaard).</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefwaveformdlg.ui" line="434"/>
        <source>Clear Cached Waveforms</source>
        <translation>Wis gebufferde Waveforms</translation>
    </message>
</context>
<context>
    <name>DlgPreferences</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="181"/>
        <source>Sound Hardware</source>
        <translation>Geluidshardware</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="193"/>
        <source>Controllers</source>
        <translation>Controllers</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="187"/>
        <source>Library</source>
        <translation>Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="217"/>
        <source>Interface</source>
        <translation>Omgeving</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="223"/>
        <source>Waveforms</source>
        <translation>Waveforms</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="261"/>
        <source>Auto DJ</source>
        <translation>Auto DJ</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="235"/>
        <source>Equalizers</source>
        <translation>Equalizers</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="229"/>
        <source>Decks</source>
        <translation>Spelers</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="241"/>
        <source>Crossfader</source>
        <translation>Crossfader</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="247"/>
        <source>Effects</source>
        <translation>Effecten</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="254"/>
        <source>LV2 Plugins</source>
        <translation>LV2 Plugins</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="275"/>
        <source>Recording</source>
        <translation>Opname</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="282"/>
        <source>Beat Detection</source>
        <translation>Beat detectie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="288"/>
        <source>Key Detection</source>
        <translation>Key detectie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="295"/>
        <source>Normalization</source>
        <translation>Normalisatie</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="202"/>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="210"/>
        <source>Vinyl Control</source>
        <translation>Vinyl Control</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="268"/>
        <source>Live Broadcasting</source>
        <translation>Live uitzenden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferences.cpp" line="302"/>
        <source>Modplug Decoder</source>
        <translation>Modplug Decoder</translation>
    </message>
</context>
<context>
    <name>DlgPreferencesDlg</name>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferencesdlg.ui" line="14"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreferencesdlg.ui" line="48"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>DlgRecording</name>
    <message>
        <location filename="../../src/library/recording/dlgrecording.ui" line="14"/>
        <source>Recordings</source>
        <translation>Opnames</translation>
    </message>
    <message>
        <location filename="../../src/library/recording/dlgrecording.ui" line="65"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="../../src/library/recording/dlgrecording.ui" line="72"/>
        <location filename="../../src/library/recording/dlgrecording.cpp" line="133"/>
        <source>Start Recording</source>
        <translation>Opname Starten</translation>
    </message>
    <message>
        <location filename="../../src/library/recording/dlgrecording.cpp" line="130"/>
        <source>Stop Recording</source>
        <translation>Opname Stoppen</translation>
    </message>
    <message>
        <location filename="../../src/library/recording/dlgrecording.cpp" line="156"/>
        <source>Recording to file: %1 (%2 MiB written in %3)</source>
        <translation>Opname naar bestand: %1 (%2 MB geschreven in %3)</translation>
    </message>
</context>
<context>
    <name>DlgTagFetcher</name>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="14"/>
        <source>MusicBrainz</source>
        <translation>MusicBrainz</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="30"/>
        <source>Select best possible match</source>
        <translation>Selecteer best mogelijke overeenkomst</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="56"/>
        <location filename="../../src/library/dlgtagfetcher.ui" line="485"/>
        <source>Track</source>
        <translation>Track</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="61"/>
        <location filename="../../src/library/dlgtagfetcher.ui" line="490"/>
        <source>Year</source>
        <translation>Jaar</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="66"/>
        <source>Title</source>
        <translation>titel</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="71"/>
        <location filename="../../src/library/dlgtagfetcher.ui" line="500"/>
        <source>Artist</source>
        <translation>Artiest</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="76"/>
        <location filename="../../src/library/dlgtagfetcher.ui" line="505"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="116"/>
        <source>Fetching track data from the MusicBrainz database</source>
        <translation>Trackgegevens ophalen uit de MusicBrainz-database</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="391"/>
        <source>Mixxx could not find this track in the MusicBrainz database.</source>
        <translation>Mixxx kon dit nummer niet terugvinden in de MusicBrainz-database.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="442"/>
        <source>Get API-Key</source>
        <comment>To be able to submit audio fingerprints to the MusicBrainz database, a free application programming interface key (API key) is required.</comment>
        <translation>haal API-key op</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="475"/>
        <source>Submit</source>
        <comment>Submits audio fingerprints to the MusicBrainz database.</comment>
        <translation>Indienen</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="495"/>
        <source>New Column</source>
        <translation>Nieuwe kolom</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="510"/>
        <source>New Item</source>
        <translation>Nieuw Item</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="536"/>
        <source>&amp;Previous</source>
        <translation>&amp;Vorige</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="543"/>
        <source>&amp;Next</source>
        <translation>V&amp;olgende</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="563"/>
        <source>&amp;Apply</source>
        <translation>&amp;Pas toe</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.ui" line="570"/>
        <source>&amp;Close</source>
        <translation>&amp;Sluit</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.cpp" line="96"/>
        <source>Status: %1</source>
        <translation>Status: %1</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.cpp" line="116"/>
        <source>HTTP Status: %1</source>
        <translation>HTTP Status: %1</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.cpp" line="118"/>
        <source>Mixxx can&apos;t connect to %1 for an unknown reason.</source>
        <translation>Mixxx kan om onbekende reden geen verbinding maken met %1.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.cpp" line="120"/>
        <source>Mixxx can&apos;t connect to %1.</source>
        <translation>Mixxx kan geen verbinding maken met %1.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.cpp" line="144"/>
        <source>Original tags</source>
        <translation>Originele tags</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtagfetcher.cpp" line="147"/>
        <source>Suggested tags</source>
        <translation>Voorgestelde tags</translation>
    </message>
</context>
<context>
    <name>DlgTrackExport</name>
    <message>
        <location filename="../../src/library/export/dlgtrackexport.ui" line="32"/>
        <source>Export Tracks</source>
        <translation>Exporteer Tracks</translation>
    </message>
    <message>
        <location filename="../../src/library/export/dlgtrackexport.ui" line="47"/>
        <source>Exporting Tracks</source>
        <translation>Tracks aan het exporteren</translation>
    </message>
    <message>
        <location filename="../../src/library/export/dlgtrackexport.ui" line="84"/>
        <source>(status text)</source>
        <translation>(status tekst)</translation>
    </message>
    <message>
        <location filename="../../src/library/export/dlgtrackexport.ui" line="112"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleer</translation>
    </message>
</context>
<context>
    <name>DlgTrackInfo</name>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="20"/>
        <source>Track Editor</source>
        <translation>Track Verwerker</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="48"/>
        <source>Summary</source>
        <translation>Samenvatting</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="525"/>
        <source>Filetype:</source>
        <translation>Bestandstype:</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="434"/>
        <source>BPM:</source>
        <translation>BPM:</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="468"/>
        <source>Location:</source>
        <translation>Locatie:</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="488"/>
        <source>Bitrate:</source>
        <translation>Bitrate:</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="578"/>
        <source>Comments</source>
        <translation>Opmerkingen</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="588"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="671"/>
        <source>Sets the BPM to 75% of the current value.</source>
        <translation>Stelt de BPM in op 75% van de huidige waarde.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="674"/>
        <source>3/4 BPM</source>
        <translation>3/4 BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="687"/>
        <source>Sets the BPM to 50% of the current value.</source>
        <translation>Stelt de BPM in op 50% van de huidige waarde.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="598"/>
        <source>Displays the BPM of the selected track.</source>
        <translation>Geeft de BPM weer van de geselecteerde track.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="54"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="88"/>
        <source>Track #</source>
        <translation>Track #</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="117"/>
        <source>Album Artist</source>
        <translation>Album Artiest</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="127"/>
        <source>Composer</source>
        <translation>Componist</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="164"/>
        <source>Title</source>
        <translation>titel</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="174"/>
        <source>Grouping</source>
        <translation>Groeperen</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="216"/>
        <source>Key</source>
        <translation>Toonaard</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="239"/>
        <source>Year</source>
        <translation>Jaar</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="268"/>
        <source>Artist</source>
        <translation>Artiest</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="284"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="326"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="406"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="441"/>
        <source>ReplayGain:</source>
        <translation>ReplayGain:</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="621"/>
        <source>Converts beats detected by the analyzer into a fixed-tempo beatgrid. 
Use this setting if your tracks have a constant tempo (e.g. most electronic music). 
Often results in higher quality beatgrids, but will not do well on tracks that have tempo shifts.</source>
        <translation>Zet de door de analysator gedetecteerde beats om in een beat-raster met een vast tempo.
Gebruik deze instelling als uw tracks een constant tempo hebben (bijv. De meeste elektronische muziek).
Dit resulteert vaak in beat-rasters van hogere kwaliteit, maar zal het niet goed doen op tracks met tempoveranderingen.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="655"/>
        <source>Sets the BPM to 200% of the current value.</source>
        <translation>Stelt de BPM in op 200% van de huidige waarde.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="658"/>
        <source>Double BPM</source>
        <translation>Dubbele BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="690"/>
        <source>Halve BPM</source>
        <translation>Halve BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="729"/>
        <source>Clear BPM and Beatgrid</source>
        <translation>Wis BPM en Beat-raster</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="803"/>
        <source>Cuepoints</source>
        <translation>Cuepoints</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="894"/>
        <source>Move to the previous item.</source>
        <extracomment>&quot;Previous&quot; button</extracomment>
        <translation>Ga naar het vorige item.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="897"/>
        <source>&amp;Previous</source>
        <translation>&amp;Vorige</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="910"/>
        <source>Move to the next item.</source>
        <extracomment>&quot;Next&quot; button</extracomment>
        <translation>Ga naar het volgende item</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="913"/>
        <source>&amp;Next</source>
        <translation>V&amp;olgende</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="461"/>
        <source>Duration:</source>
        <translation>Duur:</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="351"/>
        <source>Import Metadata from MusicBrainz</source>
        <translation>Metadata importeren uit MusicBrainz</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="358"/>
        <source>Import Metadata from File</source>
        <translation>Importeer Metadata vanaf bestand</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="547"/>
        <source>Open in File Browser</source>
        <translation>Open bestand in Browser</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="614"/>
        <source>Track BPM: </source>
        <translation>Track BPM: </translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="626"/>
        <source>Assume constant tempo</source>
        <translation>Ga uit van een constant tempo</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="639"/>
        <source>Sets the BPM to 66% of the current value.</source>
        <translation>Stelt de BPM in op 66% van de huidige waarde.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="642"/>
        <source>2/3 BPM</source>
        <translation>2/3 BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="703"/>
        <source>Sets the BPM to 150% of the current value.</source>
        <translation>Stelt de BPM in op 150% van de huidige waarde.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="706"/>
        <source>3/2 BPM</source>
        <translation>3/2 BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="719"/>
        <source>Sets the BPM to 133% of the current value.</source>
        <translation>Stelt de BPM in op 133% van de huidige waarde.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="722"/>
        <source>4/3 BPM</source>
        <translation>4/3 BPM</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="750"/>
        <source>Tap with the beat to set the BPM to the speed you are tapping.</source>
        <translation>Tik op het ritme om de BPM in te stellen op je tik-snelheid.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="753"/>
        <source>Tap to Beat</source>
        <translation>Tik op maat</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="782"/>
        <source>Hint: Use the Library Analyze view to run BPM detection.</source>
        <translation>Tip: gebruik de weergave Bibliotheekanalyse om BPM-detectie uit te voeren.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="810"/>
        <source>Cue Id</source>
        <translation>Cue Id</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="815"/>
        <source>Position</source>
        <translation>Positie</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="820"/>
        <source>Hotcue</source>
        <translation>Hotcue</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="825"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="867"/>
        <source>Activate Cue</source>
        <translation>Activeer Cue</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="977"/>
        <source>Save changes and close the window.</source>
        <extracomment>&quot;OK&quot; button</extracomment>
        <translation>Sla wijzigingen op en sluit het venster.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="980"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="841"/>
        <source>Delete Cue</source>
        <translation>Verwijder Cue</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="939"/>
        <source>Discard changes and close the window.</source>
        <extracomment>&quot;Cancel&quot; button</extracomment>
        <translation>Negeer wijzigingen en sluit het venster.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="958"/>
        <source>Save changes and keep the window open.</source>
        <extracomment>&quot;Apply&quot; button</extracomment>
        <translation>Sla wijzigingen op en houd het venster open.</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="961"/>
        <source>&amp;Apply</source>
        <translation>&amp;Pas toe</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackinfo.ui" line="942"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleer</translation>
    </message>
</context>
<context>
    <name>EffectChainSlot</name>
    <message>
        <location filename="../../src/effects/effectchainslot.cpp" line="260"/>
        <source>Empty Chain</source>
        <extracomment>Name for an empty effect chain, that is created after eject</extracomment>
        <translation>Lege keten</translation>
    </message>
</context>
<context>
    <name>EffectParameterSlotBase</name>
    <message>
        <location filename="../../src/effects/effectparameterslotbase.cpp" line="44"/>
        <source>No effect loaded.</source>
        <translation>Geen effect geladen.</translation>
    </message>
</context>
<context>
    <name>EffectSettingsModel</name>
    <message>
        <location filename="../../src/preferences/effectsettingsmodel.cpp" line="102"/>
        <source>Visible</source>
        <translation>Zichtbaar</translation>
    </message>
    <message>
        <location filename="../../src/preferences/effectsettingsmodel.cpp" line="104"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../src/preferences/effectsettingsmodel.cpp" line="106"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
</context>
<context>
    <name>EffectsManager</name>
    <message>
        <location filename="../../src/effects/effectsmanager.cpp" line="353"/>
        <source>Flanger</source>
        <translation>Flanger</translation>
    </message>
    <message>
        <location filename="../../src/effects/effectsmanager.cpp" line="361"/>
        <source>BitCrusher</source>
        <translation>BitCrusher</translation>
    </message>
    <message>
        <location filename="../../src/effects/effectsmanager.cpp" line="368"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../../src/effects/effectsmanager.cpp" line="376"/>
        <source>Reverb</source>
        <translation>Reverb</translation>
    </message>
    <message>
        <location filename="../../src/effects/effectsmanager.cpp" line="384"/>
        <source>Echo</source>
        <translation>Echo</translation>
    </message>
    <message>
        <location filename="../../src/effects/effectsmanager.cpp" line="391"/>
        <source>AutoPan</source>
        <translation>AutoPan</translation>
    </message>
    <message>
        <location filename="../../src/effects/effectsmanager.cpp" line="398"/>
        <source>Tremolo</source>
        <translation>Tremolo</translation>
    </message>
</context>
<context>
    <name>EmptyWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/emptywaveformwidget.h" line="18"/>
        <source>Empty</source>
        <translation>Leeg</translation>
    </message>
</context>
<context>
    <name>EngineBuffer</name>
    <message>
        <location filename="../../src/engine/enginebuffer.h" line="165"/>
        <source>Soundtouch (faster)</source>
        <translation>Soundtouch (sneller)</translation>
    </message>
    <message>
        <location filename="../../src/engine/enginebuffer.h" line="167"/>
        <source>Rubberband (better)</source>
        <translation>Rubberband (beter)</translation>
    </message>
    <message>
        <location filename="../../src/engine/enginebuffer.h" line="169"/>
        <source>Unknown (bad value)</source>
        <translation>Onbekend (slechte waarde)</translation>
    </message>
</context>
<context>
    <name>ErrorDialogHandler</name>
    <message>
        <location filename="../../src/errordialoghandler.cpp" line="108"/>
        <source>Fatal error</source>
        <translation>Fatale fout</translation>
    </message>
    <message>
        <location filename="../../src/errordialoghandler.cpp" line="109"/>
        <source>Critical error</source>
        <translation>Kritieke fout</translation>
    </message>
    <message>
        <location filename="../../src/errordialoghandler.cpp" line="110"/>
        <source>Warning</source>
        <translation>Waarschuwing</translation>
    </message>
    <message>
        <location filename="../../src/errordialoghandler.cpp" line="111"/>
        <source>Information</source>
        <translation>Informatie</translation>
    </message>
    <message>
        <location filename="../../src/errordialoghandler.cpp" line="112"/>
        <source>Question</source>
        <translation>Vraag</translation>
    </message>
</context>
<context>
    <name>GLRGBWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/glrgbwaveformwidget.h" line="16"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
</context>
<context>
    <name>GLSLFilteredWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/glslwaveformwidget.h" line="39"/>
        <source>Filtered</source>
        <translation>Gefilterd</translation>
    </message>
</context>
<context>
    <name>GLSLRGBWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/glslwaveformwidget.h" line="54"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
</context>
<context>
    <name>GLSimpleWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/glsimplewaveformwidget.h" line="16"/>
        <source>Simple</source>
        <translation>Eenvoudig</translation>
    </message>
</context>
<context>
    <name>GLVSyncTestWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/glvsynctestwidget.h" line="16"/>
        <source>VSyncTest</source>
        <translation>VSyncTest</translation>
    </message>
</context>
<context>
    <name>GLWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/glwaveformwidget.h" line="16"/>
        <source>Filtered</source>
        <translation>Gefilterd</translation>
    </message>
</context>
<context>
    <name>HSVWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/hsvwaveformwidget.h" line="15"/>
        <source>HSV</source>
        <translation>HSV</translation>
    </message>
</context>
<context>
    <name>HidController</name>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="181"/>
        <source>Generic HID Mouse</source>
        <translation>Generieke HID Muis</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="182"/>
        <source>Generic HID Joystick</source>
        <translation>Generieke HID Joystick</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="183"/>
        <source>Generic HID Gamepad</source>
        <translation>Generieke HID Gamepad</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="184"/>
        <source>Generic HID Keyboard</source>
        <translation>Generieke HID Toetsenbord</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="185"/>
        <source>Generic HID Multiaxis Controller</source>
        <translation>Generieke HID multi-axis-controller</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="186"/>
        <source>Unknown HID Desktop Device</source>
        <translation>Onbekend HID Desktop Apparaat</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="193"/>
        <source>HID Infrared Control</source>
        <translation>HID Infraroodbediening</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="195"/>
        <source>Unknown Apple HID Device</source>
        <translation>Onbekend Apple HID-apparaat</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="200"/>
        <source>HID Unknown Device</source>
        <translation>HID Onbekend Apparaat</translation>
    </message>
    <message>
        <location filename="../../src/controllers/hid/hidcontroller.cpp" line="206"/>
        <source>HID Interface Number</source>
        <translation>HID interface-nummer</translation>
    </message>
</context>
<context>
    <name>ITunesFeature</name>
    <message>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="100"/>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="826"/>
        <source>iTunes</source>
        <translation>iTunes</translation>
    </message>
    <message>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="184"/>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="248"/>
        <source>Select your iTunes library</source>
        <translation>Selecteer je iTunes-bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="211"/>
        <source>(loading) iTunes</source>
        <translation>iTunes (aan het laden)</translation>
    </message>
    <message>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="236"/>
        <source>Use Default Library</source>
        <translation>Gebruik de standaardbibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="237"/>
        <source>Choose Library...</source>
        <translation>Kies bibliotheek...</translation>
    </message>
    <message>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="821"/>
        <source>Error Loading iTunes Library</source>
        <translation>Fout bij het laden van iTunes-bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/library/itunes/itunesfeature.cpp" line="822"/>
        <source>There was an error loading your iTunes library. Some of your iTunes tracks or playlists may not have loaded.</source>
        <translation>Er is een fout opgetreden bij het laden van je iTunes-bibliotheek. Sommige van uw iTunes-nummers of afspeellijsten zijn mogelijk niet geladen.</translation>
    </message>
</context>
<context>
    <name>LegacySkinParser</name>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="1153"/>
        <source>Safe Mode Enabled</source>
        <extracomment>Shown when Mixxx is running in safe mode.</extracomment>
        <translation>Veilige modus ingeschakeld</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="1163"/>
        <source>No OpenGL
support.</source>
        <extracomment>Shown when Spinny can not be displayed. Please keep 
 unchanged</extracomment>
        <translation>Geen OpenGL
ondersteuning.</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2179"/>
        <source>activate</source>
        <translation>Activeer</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2184"/>
        <source>toggle</source>
        <translation>Wisselen</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2194"/>
        <source>right</source>
        <translation>rechts</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2199"/>
        <source>left</source>
        <translation>links</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2204"/>
        <source>right small</source>
        <translation>rechts klein</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2209"/>
        <source>left small</source>
        <translation>links klein</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2214"/>
        <source>up</source>
        <translation>omhoog</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2219"/>
        <source>down</source>
        <translation>omlaag</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2224"/>
        <source>up small</source>
        <translation>omhoog klein</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2229"/>
        <source>down small</source>
        <translation>omlaag klein</translation>
    </message>
    <message>
        <location filename="../../src/skin/legacyskinparser.cpp" line="2262"/>
        <source>Shortcut</source>
        <translation>Snelkoppeling</translation>
    </message>
</context>
<context>
    <name>Library</name>
    <message>
        <location filename="../../src/library/library.cpp" line="363"/>
        <source>Add Directory to Library</source>
        <translation>Map aan bibliotheek toevoegen</translation>
    </message>
    <message>
        <location filename="../../src/library/library.cpp" line="364"/>
        <source>Could not add the directory to your library. Either this directory is already in your library or you are currently rescanning your library.</source>
        <translation>Kon de map niet toevoegen aan uw bibliotheek. Ofwel bevindt deze map zich al in uw bibliotheek of u scant momenteel uw bibliotheek opnieuw.</translation>
    </message>
</context>
<context>
    <name>LibraryFeature</name>
    <message>
        <location filename="../../src/library/libraryfeature.cpp" line="39"/>
        <source>Import Playlist</source>
        <translation>Importeer Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/libraryfeature.cpp" line="41"/>
        <source>Playlist Files (*.m3u *.m3u8 *.pls *.csv)</source>
        <translation>Afspeellijstbestanden (*.m3u *.m3u8 *.pls *.csv)</translation>
    </message>
    <message>
        <location filename="../../src/library/libraryfeature.cpp" line="81"/>
        <source>Overwrite File?</source>
        <translation>Bestand Overschrijven?</translation>
    </message>
    <message>
        <location filename="../../src/library/libraryfeature.cpp" line="82"/>
        <source>A playlist file with the name &quot;%1&quot; already exists.
The default &quot;m3u&quot; extension was added because none was specified.

Do you really want to overwrite it?</source>
        <translation>Er bestaat al een afspeellijstbestand met de naam &quot;%1&quot;.
De standaard &quot;m3u&quot; -extensie is toegevoegd omdat er geen is opgegeven.

Wil je het echt overschrijven?</translation>
    </message>
</context>
<context>
    <name>LibraryScannerDlg</name>
    <message>
        <location filename="../../src/library/scanner/libraryscannerdlg.cpp" line="33"/>
        <source>Library Scanner</source>
        <translation>Bibliotheekscanner</translation>
    </message>
    <message>
        <location filename="../../src/library/scanner/libraryscannerdlg.cpp" line="34"/>
        <source>It&apos;s taking Mixxx a minute to scan your music library, please wait...</source>
        <translation>Mixxx heeft ongeveer een minuut nodig om je muziekbibliotheek te scannen, even geduld...</translation>
    </message>
    <message>
        <location filename="../../src/library/scanner/libraryscannerdlg.cpp" line="37"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../../src/library/scanner/libraryscannerdlg.cpp" line="63"/>
        <source>Scanning: </source>
        <translation>Scannen:</translation>
    </message>
    <message>
        <location filename="../../src/library/scanner/libraryscannerdlg.cpp" line="76"/>
        <source>Scanning cover art (safe to cancel)</source>
        <translation>Hoezen aan het scannen (veilig om te annuleren)</translation>
    </message>
</context>
<context>
    <name>LibraryTableModel</name>
    <message>
        <location filename="../../src/library/librarytablemodel.cpp" line="57"/>
        <source>Sort items randomly</source>
        <translation>Sorteer items willekeurig</translation>
    </message>
</context>
<context>
    <name>MidiController</name>
    <message>
        <location filename="../../src/controllers/midi/midicontroller.cpp" line="22"/>
        <source>MIDI Controller</source>
        <translation>MIDI Controller</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midicontroller.cpp" line="136"/>
        <source>MixxxControl(s) not found</source>
        <translation>Mixxx bediening(en) niet gevonden</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midicontroller.cpp" line="137"/>
        <source>One or more MixxxControls specified in the outputs section of the loaded preset were invalid.</source>
        <translation>Een of meer MixxxControls gespecificeerd in de uitgangen sectie van de geladen preset waren ongeldig.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midicontroller.cpp" line="139"/>
        <source>Some LEDs or other feedback may not work correctly.</source>
        <translation>Sommige LED&apos;s of andere feedback werken mogelijk niet correct.</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midicontroller.cpp" line="140"/>
        <source>* Check to see that the MixxxControl names are spelled correctly in the mapping file (.xml)
</source>
        <translation>* Controleer of de MixxxControl-namen correct zijn gespeld in het koppelingsbestand (.xml)
</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midicontroller.cpp" line="143"/>
        <source>* Make sure the MixxxControls in question actually exist. Visit this wiki page for a complete list: </source>
        <translation>* Controleerr dat de MixxxControls in kwestie echt bestaan. Bezoek deze wikipagina voor een volledige lijst:</translation>
    </message>
</context>
<context>
    <name>MixxxDb</name>
    <message>
        <location filename="../../src/database/mixxxdb.cpp" line="45"/>
        <source>Click OK to exit.</source>
        <translation>Klik OK om te verlaten.</translation>
    </message>
    <message>
        <location filename="../../src/database/mixxxdb.cpp" line="46"/>
        <source>Cannot upgrade database schema</source>
        <translation>Kan databaseschema niet upgraden</translation>
    </message>
    <message>
        <location filename="../../src/database/mixxxdb.cpp" line="48"/>
        <source>Unable to upgrade your database schema to version %1</source>
        <translation>Onmogelijk om de databaseschema te upgraden naar versie %1</translation>
    </message>
    <message>
        <location filename="../../src/database/mixxxdb.cpp" line="50"/>
        <source>For help with database issues contact:</source>
        <translation>Voor hulp bij database problemen contacteer:</translation>
    </message>
    <message>
        <location filename="../../src/database/mixxxdb.cpp" line="62"/>
        <source>Your mixxxdb.sqlite file may be corrupt.</source>
        <translation>Uw mixxxdb.sqlite bestand is mogelijk corrupt.</translation>
    </message>
    <message>
        <location filename="../../src/database/mixxxdb.cpp" line="63"/>
        <source>Try renaming it and restarting Mixxx.</source>
        <translation>Probeer te hernoemen en Mixxx te herstarten.</translation>
    </message>
    <message>
        <location filename="../../src/database/mixxxdb.cpp" line="71"/>
        <source>Your mixxxdb.sqlite file was created by a newer version of Mixxx and is incompatible.</source>
        <translation>Uw mixxxdb.sqlite-bestand is gemaakt door een nieuwere versie van Mixxx en is niet compatibel.</translation>
    </message>
    <message>
        <location filename="../../src/database/mixxxdb.cpp" line="80"/>
        <source>The database schema file is invalid.</source>
        <translation>Het database-schemabestand is ongeldig.</translation>
    </message>
</context>
<context>
    <name>MixxxLibraryFeature</name>
    <message>
        <location filename="../../src/library/mixxxlibraryfeature.cpp" line="28"/>
        <source>Missing Tracks</source>
        <translation>Ontbrekende Tracks</translation>
    </message>
    <message>
        <location filename="../../src/library/mixxxlibraryfeature.cpp" line="29"/>
        <source>Hidden Tracks</source>
        <translation>Verborgen Tracks</translation>
    </message>
    <message>
        <location filename="../../src/library/mixxxlibraryfeature.cpp" line="140"/>
        <source>Tracks</source>
        <translation>Tracks</translation>
    </message>
</context>
<context>
    <name>MixxxMainWindow</name>
    <message>
        <location filename="../../src/mixxx.cpp" line="363"/>
        <source>Choose music library directory</source>
        <translation>Kies de map voor de muziekbibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="889"/>
        <source>Sound Device Busy</source>
        <translation>Geluisapparaat bezig</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="898"/>
        <source>&lt;b&gt;Retry&lt;/b&gt; after closing the other application or reconnecting a sound device</source>
        <translation>&lt;b&gt;Probeer opnieuw&lt;/b&gt; na het afsluiten van de andere applicatie of het opnieuw aansluiten van een geluidsapparaat</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="902"/>
        <location filename="../../src/mixxx.cpp" line="929"/>
        <location filename="../../src/mixxx.cpp" line="954"/>
        <source>&lt;b&gt;Reconfigure&lt;/b&gt; Mixxx&apos;s sound device settings.</source>
        <translation>&lt;b&gt;Configureer&lt;/b&gt; opnieuw de Mixxx instellingen van het  geluidsapparaat.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="905"/>
        <location filename="../../src/mixxx.cpp" line="932"/>
        <source>Get &lt;b&gt;Help&lt;/b&gt; from the Mixxx Wiki.</source>
        <translation>&lt;b&gt;Hulp&lt;/b&gt;zoeken in de Mixxx Wiki.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="908"/>
        <location filename="../../src/mixxx.cpp" line="935"/>
        <location filename="../../src/mixxx.cpp" line="957"/>
        <source>&lt;b&gt;Exit&lt;/b&gt; Mixxx.</source>
        <translation>&lt;b&gt;Verlaat&lt;/b&gt; Mixxx.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="848"/>
        <source>Retry</source>
        <translation>Opnieuw Proberen</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="757"/>
        <source>Cannot open database</source>
        <translation>Kan database niet openen</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="758"/>
        <source>Unable to establish a database connection.
Mixxx requires QT with SQLite support. Please read the Qt SQL driver documentation for information on how to build it.

Click OK to exit.</source>
        <translation>Kan geen databaseverbinding tot stand brengen.
Mixxx vereist QT met SQLite-ondersteuning. Lees de QT SQL-driver documentatie voor meer informatie over het opbouwen ervan.

Klik op OK om af te sluiten.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="850"/>
        <location filename="../../src/mixxx.cpp" line="965"/>
        <source>Reconfigure</source>
        <translation>Opnieuw configureren</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="852"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="854"/>
        <location filename="../../src/mixxx.cpp" line="967"/>
        <source>Exit</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="892"/>
        <location filename="../../src/mixxx.cpp" line="921"/>
        <source>Mixxx was unable to open all the configured sound devices.</source>
        <translation>Mixxx kon niet alle geconfigureerde geluidsapparaten openen.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="918"/>
        <source>Sound Device Error</source>
        <translation>Fout met geluidsapparaat</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="926"/>
        <source>&lt;b&gt;Retry&lt;/b&gt; after fixing an issue</source>
        <translation>&lt;b&gt;Probeer opnieuw&lt;/b&gt; na het oplossen van een probleem</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="945"/>
        <source>No Output Devices</source>
        <translation>Geen uitvoerapparaten</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="947"/>
        <source>Mixxx was configured without any output sound devices. Audio processing will be disabled without a configured output device.</source>
        <translation>Mixxx is ingesteld zonder uitvoerapparaten voor geluid. Geluidsverwerking wordt uitgeschakeld zonder een geconfigureerd uitvoerapparaat.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="951"/>
        <source>&lt;b&gt;Continue&lt;/b&gt; without any outputs.</source>
        <translation>&lt;b&gt;Ga door&lt;/b&gt; zonder uitvoer.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="963"/>
        <source>Continue</source>
        <translation>Verdergaan</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1103"/>
        <source>Load track to Deck %1</source>
        <translation>Laad track in Speler %1</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1104"/>
        <source>Deck %1 is currently playing a track.</source>
        <translation>Deck %1 speelt momenteel een track af.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1106"/>
        <source>Are you sure you want to load a new track?</source>
        <translation>Bent u zeker dat u een nieuwe track wil laden?</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1216"/>
        <source>There is no input device selected for this vinyl control.
Please select an input device in the sound hardware preferences first.</source>
        <translation>Er is geen invoerapparaat geselecteerd voor deze vinylbesturing.
Selecteer eerst een invoerapparaat in de voorkeuren voor geluidshardware.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1229"/>
        <source>There is no input device selected for this passthrough control.
Please select an input device in the sound hardware preferences first.</source>
        <translation>Er is geen invoerapparaat geselecteerd voor dit doorvoerapparaat.
Selecteer eerst een invoerapparaat in de voorkeuren voor geluidsapparatuur.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1242"/>
        <source>There is no input device selected for this microphone.
Please select an input device in the sound hardware preferences first.</source>
        <translation>Er is geen invoerapparaat geselecteerd voor deze microfoon.
Selecteer eerst een invoerapparaat in de voorkeuren voor geluidsapparatuur.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1319"/>
        <source>Error in skin file</source>
        <translation>Fout in themabestand</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1320"/>
        <source>The selected skin cannot be loaded.</source>
        <translation>Het geselecteerde thema kan niet worden geladen.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1420"/>
        <source>OpenGL Direct Rendering</source>
        <translation>OpenGL Direct Rendering</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1421"/>
        <source>Direct rendering is not enabled on your machine.&lt;br&gt;&lt;br&gt;This means that the waveform displays will be very&lt;br&gt;&lt;b&gt;slow and may tax your CPU heavily&lt;/b&gt;. Either update your&lt;br&gt;configuration to enable direct rendering, or disable&lt;br&gt;the waveform displays in the Mixxx preferences by selecting&lt;br&gt;&quot;Empty&quot; as the waveform display in the &apos;Interface&apos; section.</source>
        <translation>Directe weergave is niet ingeschakeld op uw computer.&lt;br&gt;&lt;br&gt; Dit betekent dat de weergave van de golfvormen erg&lt;br&gt;&lt;b&gt; traag zal zijn en uw CPU zwaar kan belasten&lt;/b&gt;. Werk uw &lt;br&gt; configuratie bij om directe weergave mogelijk te maken of schakel&lt;br&gt;de waveform weergaven uit in de Mixxx-voorkeuren door &quot;Leeg&quot; te selecteren&lt;br&gt; als de waveform weergave in het gedeelte &quot;Interface&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1452"/>
        <location filename="../../src/mixxx.cpp" line="1460"/>
        <location filename="../../src/mixxx.cpp" line="1469"/>
        <source>Confirm Exit</source>
        <translation>Afsluiten bevestigen</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1453"/>
        <source>A deck is currently playing. Exit Mixxx?</source>
        <translation>Er is momenteel een speler actief. Mixxx afsluiten?</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1461"/>
        <source>A sampler is currently playing. Exit Mixxx?</source>
        <translation>Er is momenteel een sampler actief. Mixxx afsluiten?</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1470"/>
        <source>The preferences window is still open.</source>
        <translation>Het scherm &quot;Voorkeuren&quot; staat nog open.</translation>
    </message>
    <message>
        <location filename="../../src/mixxx.cpp" line="1471"/>
        <source>Discard any changes and exit Mixxx?</source>
        <translation>Wijzigingen negeren en Mixxx afsluiten?</translation>
    </message>
</context>
<context>
    <name>ParserCsv</name>
    <message>
        <location filename="../../src/library/parsercsv.cpp" line="43"/>
        <source>Location</source>
        <translation>Plaats</translation>
    </message>
    <message>
        <location filename="../../src/library/parsercsv.cpp" line="130"/>
        <source>Playlist Export Failed</source>
        <translation>Afspeellijst exporteren mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/parsercsv.cpp" line="131"/>
        <location filename="../../src/library/parsercsv.cpp" line="203"/>
        <source>Could not create file</source>
        <translation>Kon bestand niet aanmaken</translation>
    </message>
    <message>
        <location filename="../../src/library/parsercsv.cpp" line="202"/>
        <source>Readable text Export Failed</source>
        <translation>Export leesbare tekst mislukt</translation>
    </message>
</context>
<context>
    <name>ParserM3u</name>
    <message>
        <location filename="../../src/library/parserm3u.cpp" line="146"/>
        <location filename="../../src/library/parserm3u.cpp" line="157"/>
        <source>Playlist Export Failed</source>
        <translation>Afspeellijst exporteren mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/parserm3u.cpp" line="147"/>
        <source>File path contains characters, not allowed in m3u playlists.
</source>
        <translation>Bestandspad bevat karakters die niet toegelaten zijn in m3u-afspeellijsten.</translation>
    </message>
    <message>
        <location filename="../../src/library/parserm3u.cpp" line="148"/>
        <source>Export a m3u8 playlist instead!
</source>
        <translation>Exporteer in plaats daarvan een m3u8 afspeellijst!
</translation>
    </message>
    <message>
        <location filename="../../src/library/parserm3u.cpp" line="158"/>
        <source>Could not create file</source>
        <translation>Kon bestand niet aanmaken</translation>
    </message>
</context>
<context>
    <name>ParserPls</name>
    <message>
        <location filename="../../src/library/parserpls.cpp" line="142"/>
        <source>Playlist Export Failed</source>
        <translation>Afspeellijst exporteren mislukt</translation>
    </message>
    <message>
        <location filename="../../src/library/parserpls.cpp" line="143"/>
        <source>Could not create file</source>
        <translation>Kon bestand niet aanmaken</translation>
    </message>
</context>
<context>
    <name>PlaylistFeature</name>
    <message>
        <location filename="../../src/library/playlistfeature.cpp" line="81"/>
        <source>Lock</source>
        <translation>Vergrendelen</translation>
    </message>
    <message>
        <location filename="../../src/library/playlistfeature.cpp" line="54"/>
        <location filename="../../src/library/playlistfeature.cpp" line="270"/>
        <source>Playlists</source>
        <translation>Speellijsten</translation>
    </message>
    <message>
        <location filename="../../src/library/playlistfeature.cpp" line="81"/>
        <source>Unlock</source>
        <translation>Ontgrendel</translation>
    </message>
    <message>
        <location filename="../../src/library/playlistfeature.cpp" line="271"/>
        <source>Playlists are ordered lists of songs that allow you to plan your DJ sets.</source>
        <translation>Afspeellijsten zijn geordende lijsten met nummers waarmee u uw DJ-sets kunt plannen.</translation>
    </message>
    <message>
        <location filename="../../src/library/playlistfeature.cpp" line="272"/>
        <source>Some DJs construct playlists before they perform live, but others prefer to build them on-the-fly.</source>
        <translation>Sommige DJ&apos;s maken afspeellijsten voordat ze live optreden, maar anderen bouwen ze liever on-the-fly tijdens hun optreden.</translation>
    </message>
    <message>
        <location filename="../../src/library/playlistfeature.cpp" line="273"/>
        <source>When using a playlist during a live DJ set, remember to always pay close attention to how your audience reacts to the music you&apos;ve chosen to play.</source>
        <translation>Wanneer u een afspeellijst gebruikt tijdens een live DJ-set, vergeet dan niet om goed op te letten hoe uw publiek reageert op de muziek die u hebt gekozen om te spelen.</translation>
    </message>
    <message>
        <location filename="../../src/library/playlistfeature.cpp" line="274"/>
        <source>It may be necessary to skip some songs in your prepared playlist or add some different songs in order to maintain the energy of your audience.</source>
        <translation>Het kan nodig zijn om sommige tracks over te slaan in je voorbereide afspeellijst of om een aantal tracks toe te voegen teneinde de energie van je publiek te behouden.</translation>
    </message>
    <message>
        <location filename="../../src/library/playlistfeature.cpp" line="275"/>
        <source>Create New Playlist</source>
        <translation>Nieuwe Afspeellijst aanmaken</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="436"/>
        <source>Upgrading Mixxx</source>
        <translation>Mixxx Upgraden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="437"/>
        <source>Mixxx now supports displaying cover art.
Do you want to scan your library for cover files now?</source>
        <translation>Mixxx ondersteunt nu het weergeven van albumhoezen.
Wilt u nu uw bibliotheek scannen op albumhoezen?</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="440"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="441"/>
        <source>Later</source>
        <translation>Later</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="450"/>
        <source>Upgrading Mixxx from v1.9.x/1.10.x.</source>
        <translation>Mixxx upgraden van v1.9.x/1.10.x.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="452"/>
        <source>Mixxx has a new and improved beat detector.</source>
        <translation>Mixxx bevat een nieuwe en verbeterde beat detector.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="453"/>
        <source>When you load tracks, Mixxx can re-analyze them and generate new, more accurate beatgrids. This will make automatic beatsync and looping more reliable.</source>
        <translation>Wanneer je tracks laadt, kan Mixxx ze opnieuw analyseren en nieuwe, nauwkeurigere beat-rasters genereren. Dit maakt automatische beatsync en looping betrouwbaarder.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="457"/>
        <source>This does not affect saved cues, hotcues, playlists, or crates.</source>
        <translation>Dit heeft geen invloed op opgeslagen cues, hotcues, afspeellijsten of kratten.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="459"/>
        <source>If you do not want Mixxx to re-analyze your tracks, choose &quot;Keep Current Beatgrids&quot;. You can change this setting at any time from the &quot;Beat Detection&quot; section of the Preferences.</source>
        <translation>Als je niet wilt dat Mixxx je tracks opnieuw analyseert, kies dan &quot;Huidige beat-rasters behouden&quot;. Je kunt deze instelling op elk moment wijzigen in het &quot;Beat Detectie&quot;  gedeelte in de Voorkeuren.</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="463"/>
        <source>Keep Current Beatgrids</source>
        <translation>Huidige beat-rasters behouden</translation>
    </message>
    <message>
        <location filename="../../src/preferences/upgrade.cpp" line="464"/>
        <source>Generate New Beatgrids</source>
        <translation>Nieuwe beat-rasters genereren</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="6"/>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="187"/>
        <source>Invalid</source>
        <translation>Ongeldig</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="10"/>
        <source>Note On</source>
        <translation>Note On</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="12"/>
        <source>Note Off</source>
        <translation>Note Uit</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="14"/>
        <source>CC</source>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="16"/>
        <source>Pitch Bend</source>
        <translation>Pitch Bend</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="18"/>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="64"/>
        <source>Unknown (0x%1)</source>
        <translation>Onbekend (0x%1)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="34"/>
        <source>Normal</source>
        <translation>Normaal</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="36"/>
        <source>Invert</source>
        <translation>Draai Om</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="38"/>
        <source>Rot64</source>
        <translation>Rot64</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="40"/>
        <source>Rot64Inv</source>
        <translation>Rot64Inv</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="42"/>
        <source>Rot64Fast</source>
        <translation>Rot64Fast</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="44"/>
        <source>Diff</source>
        <translation>Diff</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="46"/>
        <source>Button</source>
        <translation>Knop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="48"/>
        <source>Switch</source>
        <translation>Switch</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="50"/>
        <source>Spread64</source>
        <translation>Spread64</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="52"/>
        <source>HercJog</source>
        <translation>HercJog</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="54"/>
        <source>SelectKnob</source>
        <translation>Keuzeknop</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="56"/>
        <source>SoftTakeover</source>
        <translation>SoftTakeover</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="58"/>
        <source>Script</source>
        <translation>Script</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="60"/>
        <source>14-bit (LSB)</source>
        <translation>14-bit (LSB)</translation>
    </message>
    <message>
        <location filename="../../src/controllers/midi/midiutils.cpp" line="62"/>
        <source>14-bit (MSB)</source>
        <translation>14-bit (MSB)</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="189"/>
        <source>Master</source>
        <translation>Master</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="191"/>
        <source>Booth</source>
        <translation>Booth</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="193"/>
        <source>Headphones</source>
        <translation>Hoofdtelefoon</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="197"/>
        <source>Left Bus</source>
        <translation>Bus Links</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="199"/>
        <source>Center Bus</source>
        <translation>Bus Centraal</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="201"/>
        <source>Right Bus</source>
        <translation>Bus Rechts</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="203"/>
        <source>Invalid Bus</source>
        <translation>Ongeldinge Bus</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="206"/>
        <source>Deck</source>
        <translation>Deck</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="209"/>
        <source>Record/Broadcast</source>
        <translation>Opnemen/Uitzenden</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="211"/>
        <source>Vinyl Control</source>
        <translation>Vinyl Control</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="214"/>
        <source>Microphone</source>
        <translation>Microfoon</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="217"/>
        <source>Auxiliary</source>
        <translation>Auxiliary</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanagerutil.cpp" line="220"/>
        <source>Unknown path type %1</source>
        <translation>Ongekend pad-type %1</translation>
    </message>
    <message>
        <location filename="../../src/encoder/encodermp3.cpp" line="124"/>
        <location filename="../../src/encoder/encodermp3.cpp" line="232"/>
        <source>Encoder</source>
        <translation>Encoder</translation>
    </message>
    <message>
        <location filename="../../src/encoder/encodermp3.cpp" line="125"/>
        <source>&lt;html&gt;Mixxx cannot record or stream in MP3 without the MP3 encoder &amp;quot;lame&amp;quot;. Due to licensing issues, we cannot include this with Mixxx. To record or stream in MP3, you must download &lt;b&gt;libmp3lame&lt;/b&gt; and install it on your system. &lt;p&gt;See &lt;a href=&apos;http://mixxx.org/wiki/doku.php/internet_broadcasting#%1&apos;&gt;Mixxx Wiki&lt;/a&gt; for more information. &lt;/html&gt;</source>
        <translation>&lt;html&gt;Mixxx kan niet opnemen of streamen in MP3 zonder de MP3 encoder &amp;quot;lame&amp;quot;. Vanwege licentieproblemen kunnen we dit niet toevoegen in Mixxx. Om op te nemen of te streamen in MP3, moet u de&lt;b&gt;libmp3lame&lt;/b&gt; downloaden en installeren op uw systeem. &lt;p&gt;Zie &lt;a href=&apos;http://mixxx.org/wiki/doku.php/internet_broadcasting#%1&apos;&gt;Mixxx Wiki&lt;/a&gt; voor meer informatie. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/encoder/encodermp3.cpp" line="233"/>
        <source>&lt;html&gt;Mixxx has detected that you use a modified version of libmp3lame. See &lt;a href=&apos;http://mixxx.org/wiki/doku.php/internet_broadcasting&apos;&gt;Mixxx Wiki&lt;/a&gt; for more information.&lt;/html&gt;</source>
        <translation>&lt;html&gt;Mixxx heeft gedetecteerd dat je een aangepaste versie van libmp3lame gebruikt. Zie &lt;a href=&apos;http://mixxx.org/wiki/doku.php/internet_broadcasting&apos;&gt;Mixxx Wiki&lt;/a&gt; voor meer informatie.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/util/sandbox.cpp" line="87"/>
        <source>Mixxx Needs Access to: %1</source>
        <translation>Mixxx heeft toegang nodig tot: %1</translation>
    </message>
    <message>
        <location filename="../../src/util/sandbox.cpp" line="92"/>
        <source>Due to Mac Sandboxing, we need your permission to access this file:

%1

After clicking OK, you will see a file picker. To give Mixxx permission, you must select '%2' to proceed. If you do not want to grant Mixxx access click Cancel on the file picker. We're sorry for this inconvenience.

To abort this action, press Cancel on the file dialog.</source>
        <translation>Vanwege Mac Sandboxing hebben we uw toestemming nodig om dit bestand te openen:

% 1

Nadat u op OK heeft geklikt, ziet u een bestandskiezer. Om Mixxx toestemming te geven, moet je &apos;% 2&apos; selecteren om door te gaan. Als u Mixxx geen toegang wilt verlenen, klikt u op Annuleren in de bestandskiezer. Onze excuses voor het ongemak.

Om deze actie af te breken, drukt u op Annuleren in het bestandsdialoogvenster.</translation>
    </message>
    <message>
        <location filename="../../src/util/sandbox.cpp" line="131"/>
        <source>You selected the wrong file. To grant Mixxx access, please select the file &apos;%1&apos;. If you do not want to continue, press Cancel.</source>
        <translation>Je selecteerde het verkeerde bestand. Om Mixxx toegang te verlenen, selecteer het bestand% 1 . Als u niet wil doorgaan, druk op Annuleren.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="25"/>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="26"/>
        <source>Bit Depth</source>
        <translation>Bit Diepte</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="14"/>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="15"/>
        <source>Bitcrusher</source>
        <translation>Bitcrusher</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="18"/>
        <source>Adds noise by the reducing the bit depth and sample rate</source>
        <translation>Voegt ruis toe door de bitdiepte en samplefrequentie te reduceren</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="27"/>
        <source>The bit depth of the samples</source>
        <translation>De bitdiepte van de samples</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="43"/>
        <source>Downsampling</source>
        <translation>Downsampling</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="44"/>
        <source>Down</source>
        <translation>Omlaag</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bitcrushereffect.cpp" line="45"/>
        <source>The sample rate to which the signal is downsampled</source>
        <translation>De samplefrequentie waarop het signaal omlaag wordt gesampled</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="36"/>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="37"/>
        <source>Echo</source>
        <translation>Echo</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="46"/>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="47"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="74"/>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="75"/>
        <source>Ping Pong</source>
        <translation>Ping Pong</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="87"/>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="88"/>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="67"/>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="68"/>
        <source>Send</source>
        <translation>Verstuur</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="89"/>
        <source>How much of the signal to send into the delay buffer</source>
        <translation>Hoeveel van het signaal moet naar de vertragingsbuffer gestuurd worden</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="61"/>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="62"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="44"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="45"/>
        <source>Feedback</source>
        <translation>Feedback</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="40"/>
        <source>Stores the input signal in a temporary buffer and outputs it after a short time</source>
        <translation>Slaat het ingangssignaal op in een tijdelijke buffer en voert het na korte tijd uit</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="48"/>
        <source>Delay time
1/8 - 2 beats if tempo is detected
1/8 - 2 seconds if no tempo is detected</source>
        <translation>Vertragingstijd
1/8 - 2 tellen als tempo wordt gedetecteerd
1/8 - 2 seconden als er geen tempo wordt gedetecteerd</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="63"/>
        <source>Amount the echo fades each time it loops</source>
        <translation>Hoeveel keer de echo vervaagt elke keer deze een loop maakt</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="76"/>
        <source>How much the echoed sound bounces between the left and right sides of the stereo field</source>
        <translation>Hoe dikwijls het echogeluid weerkaatst tussen de linker- en rechterkant van het stereoveld</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="101"/>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="102"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="103"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="104"/>
        <source>Quantize</source>
        <translation>Quantize</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="103"/>
        <source>Round the Time parameter to the nearest 1/4 beat.</source>
        <translation>Rond de parameter Tijd af naar de dichtstbijzijnde 1/4 tel.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="114"/>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="115"/>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="105"/>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="106"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="97"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="98"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="117"/>
        <source>Triplets</source>
        <translation>Trio</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/echoeffect.cpp" line="116"/>
        <source>When the Quantize parameter is enabled, divide rounded 1/4 beats of Time parameter by 3.</source>
        <translation>Als de Quantize-parameter is ingeschakeld, deelt u de afgeronde 1/4 beats van de Tijd-parameter door 3.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="18"/>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="19"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="22"/>
        <source>Allows only high or low frequencies to play.</source>
        <translation>Laat enkel hoge of lage frequenties toe te spelen.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="28"/>
        <source>Low Pass Filter Cutoff</source>
        <translation>Lage Doorlaatfilter Cutoff</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="29"/>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="28"/>
        <source>LPF</source>
        <translation>LPF</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="30"/>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="29"/>
        <source>Corner frequency ratio of the low pass filter</source>
        <translation>Hoekfrequentieverhouding van het laagdoorlaatfilter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="44"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="45"/>
        <source>Resonance of the filters
Default: flat top</source>
        <translation>Resonantie van de filters
Standaard: flat top</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="57"/>
        <source>High Pass Filter Cutoff</source>
        <translation>Hoge Doorlaatfilter Cutoff</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="58"/>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="53"/>
        <source>HPF</source>
        <translation>HPF</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="59"/>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="54"/>
        <source>Corner frequency ratio of the high pass filter</source>
        <translation>Hoekfrequentieverhouding van het hoogdoorlaatfilter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="83"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="84"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="27"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="28"/>
        <source>Depth</source>
        <translation>Diepte</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="27"/>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="28"/>
        <source>Flanger</source>
        <translation>Flanger</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="37"/>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="38"/>
        <source>Speed</source>
        <translation>Snelheid</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="64"/>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="65"/>
        <source>Manual</source>
        <translation>Manueel</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="31"/>
        <source>Mixes the input with a delayed, pitch modulated copy of itself to create comb filtering</source>
        <translation>Mengt de invoer met een vertraagde, met pitch gemoduleerde kopie van zichzelf om kamfiltering te creëren</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="39"/>
        <source>Speed of the LFO (low frequency oscillator)
32 - 1/4 beats rounded to 1/2 beat per LFO cycle if tempo is detected
1/32 - 4 Hz if no tempo is detected</source>
        <translation>Snelheid van de LFO (laagfrequente oscillator)
32 - 1/4 beats afgerond op 1/2 beat per LFO-cyclus als het tempo wordt gedetecteerd
1/32 - 4 Hz als er geen tempo wordt gedetecteerd</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="53"/>
        <source>Delay amplitude of the LFO (low frequency oscillator)</source>
        <translation>Amplitudevertraging van de LFO (laagfrequente oscillator)</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="66"/>
        <source>Delay offset of the LFO (low frequency oscillator).
With width at zero, this allows for manually sweeping over the entire delay range.</source>
        <translation>Vertraag de offset van de LFO (laagfrequente oscillator).
Met een breedte van nul, maakt dit het mogelijk handmatig over het gehele vertragingsbereik te vegen.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="78"/>
        <source>Regeneration</source>
        <translation>Regeneratie</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="79"/>
        <source>Regen</source>
        <translation>Regen</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="80"/>
        <source>How much of the delay output is feed back into the input</source>
        <translation>Hoeveel van de vertraagde output wordt teruggevoerd naar de input</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="93"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="85"/>
        <source>Intensity of the effect</source>
        <translation>Intensiteit van het effect</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="107"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="99"/>
        <source>Divide rounded 1/2 beats of the Period parameter by 3.</source>
        <translation>Verdeel afgeronde 1/2 beats van de Period parameter door 3.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="91"/>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="92"/>
        <source>Mix</source>
        <translation>Mix</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="65"/>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="66"/>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="51"/>
        <location filename="../../src/effects/builtin/flangereffect.cpp" line="52"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="57"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="58"/>
        <source>Width</source>
        <translation>Breedte</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/metronomeeffect.cpp" line="20"/>
        <source>Metronome</source>
        <translation>Metronoom</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/metronomeeffect.cpp" line="23"/>
        <source>Adds a metronome click sound to the stream</source>
        <translation>Voegt het klikgeluid van een metronoom toe aan de stream</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/metronomeeffect.cpp" line="30"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/metronomeeffect.cpp" line="31"/>
        <source>Set the beats per minute value of the click sound</source>
        <translation>Stel de BPM waarde van het klikgeluid in</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/metronomeeffect.cpp" line="43"/>
        <source>Sync</source>
        <translation>Sync</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/metronomeeffect.cpp" line="44"/>
        <source>Synchronizes the BPM with the track if it can be retrieved</source>
        <translation>Synchroniseert de BPM met de track als deze kan worden opgehaald</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="31"/>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="32"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="29"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="30"/>
        <source>Period</source>
        <translation>Periode</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="21"/>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="22"/>
        <source>Autopan</source>
        <translation>Autopan</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="25"/>
        <source>Bounce the sound left and right across the stereo field</source>
        <translation>Kaats het geluid links en rechts over het stereoveld</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="33"/>
        <source>How fast the sound goes from one side to another
1/4 - 4 beats rounded to 1/2 beat if tempo is detected
1/4 - 4 seconds if no tempo is detected</source>
        <translation>Hoe snel gaat het geluid van de ene naar de andere kant
1/4 - 4 beats afgerond op 1/2 beat als het tempo wordt gedetecteerd
1/4 - 4 seconden als er geen tempo wordt gedetecteerd</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="48"/>
        <source>Smoothing</source>
        <translation>Effenen</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="49"/>
        <source>Smooth</source>
        <translation>Effen</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="50"/>
        <source>How smoothly the signal goes from one side to the other</source>
        <translation>Hoe vlot het signaal overgaat van de ene kant naar de andere</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/autopaneffect.cpp" line="67"/>
        <source>How far the signal goes to each side</source>
        <translation>Hoe ver het signaal reikt naar iedere kant.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="19"/>
        <source>Reverb</source>
        <translation>Reverb</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="22"/>
        <source>Emulates the sound of the signal bouncing off the walls of a room</source>
        <translation>Bootst het geluid van het signaal dat tegen de muren van een kamer weerkaatst na</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="27"/>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="28"/>
        <source>Decay</source>
        <translation>Verval</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="29"/>
        <source>Lower decay values cause reverberations to fade out more quickly.</source>
        <translation>Lagere vervalwaarden zorgen ervoor dat weerkaatsingen sneller vervagen.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="42"/>
        <source>Bandwidth of the low pass filter at the input.
Higher values result in less attenuation of high frequencies.</source>
        <translation>Bandbreedte van het laagdoorlaatfilter aan de ingang.
Hogere waarden resulteren in minder vervagen van hoge frequenties.

</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="69"/>
        <source>How much of the signal to send in to the effect</source>
        <translation>Hoeveel van het signaal er naar het effect moet worden gestuurd</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="40"/>
        <source>Bandwidth</source>
        <translation>Bandbreedte</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="41"/>
        <source>BW</source>
        <translation>BW</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="54"/>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="55"/>
        <source>Damping</source>
        <translation>Demping</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/reverbeffect.cpp" line="56"/>
        <source>Higher damping values cause high frequencies to decay more quickly than low frequencies.</source>
        <translation>Hogere dempingswaarden zorgen ervoor dat hoge frequenties sneller vervallen dan lage frequenties.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="22"/>
        <location filename="../../src/test/metaknob_link_test.cpp" line="49"/>
        <location filename="../../src/test/metaknob_link_test.cpp" line="215"/>
        <source>Low</source>
        <translation>Laag</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="23"/>
        <location filename="../../src/effects/builtin/graphiceqeffect.cpp" line="32"/>
        <location filename="../../src/test/metaknob_link_test.cpp" line="50"/>
        <source>Gain for Low Filter</source>
        <translation>Versterking voor lage filter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="34"/>
        <source>Kill Low</source>
        <translation>Demp Laag</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="35"/>
        <source>Kill the Low Filter</source>
        <translation>Lage Filter uitzetten</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="45"/>
        <source>Mid</source>
        <translation>Midden</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bessel4lvmixeqeffect.cpp" line="15"/>
        <source>Bessel4 LV-Mix Isolator</source>
        <translation>Bessel4 LV-Mix Isolator</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bessel4lvmixeqeffect.cpp" line="16"/>
        <source>Bessel4 ISO</source>
        <translation>Bessel4 ISO</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bessel4lvmixeqeffect.cpp" line="19"/>
        <source>A Bessel 4th-order filter isolator with Lipshitz and Vanderkooy mix (bit perfect unity, roll-off -24 dB/octave).</source>
        <translation>Een Bessel 4e-order filterisolator met Lipshitz en Vanderkooy mix (perfecte bit eenheid, roll-off -24 dB/octaaf).</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="46"/>
        <source>Gain for Mid Filter</source>
        <translation>Versterking voor middenfilter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="57"/>
        <source>Kill Mid</source>
        <translation>Midden uitzetten</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="58"/>
        <source>Kill the Mid Filter</source>
        <translation>Demp de middenfilter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="68"/>
        <source>High</source>
        <translation>Hoog</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="69"/>
        <location filename="../../src/effects/builtin/graphiceqeffect.cpp" line="68"/>
        <source>Gain for High Filter</source>
        <translation>Versterking voor hoge filter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="80"/>
        <source>Kill High</source>
        <translation>Hoog uitzetten</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="81"/>
        <source>Kill the High Filter</source>
        <translation>Demp de hoge filter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/equalizer_util.h" line="91"/>
        <source>To adjust frequency shelves, go to Preferences -&gt; Equalizers.</source>
        <translation>Ga naar Voorkeuren -&gt; Equalizers om frequentieschalen aan te passen.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/graphiceqeffect.cpp" line="15"/>
        <source>Graphic Equalizer</source>
        <translation>Graphische Equalizer</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/graphiceqeffect.cpp" line="16"/>
        <source>Graphic EQ</source>
        <translation>Grafische EQ</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/graphiceqeffect.cpp" line="19"/>
        <source>An 8-band graphic equalizer based on biquad filters</source>
        <translation>Een 8-bands grafische equalizer op basis van biquad-filters</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/graphiceqeffect.cpp" line="54"/>
        <source>Gain for Band Filter %1</source>
        <translation>Versterking voor Band filter %1</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="18"/>
        <source>Moog Ladder 4 Filter</source>
        <translation>Moog Ladder 4 Filter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="19"/>
        <source>Moog Filter</source>
        <translation>Moog Filter</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="22"/>
        <source>A 4-pole Moog ladder filter, based on Antti Houvilainen&apos;s non linear digital implementation</source>
        <translation>Een 4-polig Moog ladderfilter, gebaseerd op Antti Houvilainen&apos;s niet-lineaire digitale implementatie</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="42"/>
        <source>Res</source>
        <translation>Res</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/filtereffect.cpp" line="43"/>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="41"/>
        <source>Resonance</source>
        <translation>Resonantie</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/moogladder4filtereffect.cpp" line="43"/>
        <source>Resonance of the filters. 4 = self oscillating</source>
        <translation>Resonantie van de filters. 4 = zelfoscillerend</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeq.cpp" line="111"/>
        <source>Deck %1 EQ Effect</source>
        <translation>Speler %1 EQ Effect</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeq.cpp" line="136"/>
        <location filename="../../src/preferences/dialog/dlgprefeq.cpp" line="297"/>
        <source>EQ Effect</source>
        <translation>EQ Effect</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgprefeq.cpp" line="299"/>
        <source>Deck 1 EQ Effect</source>
        <translation>EQ Effect Speler 1</translation>
    </message>
    <message>
        <location filename="../../src/test/metaknob_link_test.cpp" line="216"/>
        <source>Gain for Low Filter (neutral at 1.0)</source>
        <translation>Versterking voor Lage filter (neutraal op 1.0)</translation>
    </message>
    <message>
        <location filename="../../src/soundio/sounddevicenetwork.cpp" line="51"/>
        <source>Network stream</source>
        <translation>Netwerkstream</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="18"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="19"/>
        <source>Phaser</source>
        <translation>Phaser</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="110"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="111"/>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="70"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="71"/>
        <source>Stages</source>
        <translation>Stadia</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="22"/>
        <source>Mixes the input signal with a copy passed through a series of all-pass filters to create comb filtering</source>
        <translation>Mengt het ingangssignaal met een kopie, die door een reeks all-pass filters gaat, om kamfiltering te creëren</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="31"/>
        <source>Period of the LFO (low frequency oscillator)
1/4 - 4 beats rounded to 1/2 beat if tempo is detected
1/4 - 4 seconds if no tempo is detected</source>
        <translation>Periode van de LFO (laagfrequente oscillator)
1/4 - 4 beats afgerond op 1/2 beat als het tempo wordt gedetecteerd
1/4 - 4 seconden als er geen tempo wordt gedetecteerd</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="46"/>
        <source>Controls how much of the output signal is looped</source>
        <translation>Bepaalt hoeveel van het uitgangssignaal wordt gelooped</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="57"/>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="58"/>
        <source>Range</source>
        <translation>Bereik</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="59"/>
        <source>Controls the frequency range across which the notches sweep.</source>
        <translation>Regelt het frequentiebereik dat door de inkepingen wordt geveegd.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="72"/>
        <source>Number of stages</source>
        <translation>Aantal stadia</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/phasereffect.cpp" line="112"/>
        <source>Sets the LFOs (low frequency oscillators) for the left and right channels out of phase with each others</source>
        <translation>Stelt de LFO&apos;s (laagfrequente oscillatoren) voor de linker- en rechterkanalen uit fase (out of phase) zijn met elkaar</translation>
    </message>
    <message>
        <location filename="../../src/widget/wbattery.cpp" line="81"/>
        <source>%1 minutes</source>
        <translation>%1 minuten</translation>
    </message>
    <message>
        <location filename="../../src/widget/wbattery.cpp" line="83"/>
        <source>%1:%2</source>
        <translation>%1:%2</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="27"/>
        <source>Ctrl+t</source>
        <translation>Ctrl+t</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="28"/>
        <source>Ctrl+y</source>
        <translation>Ctrl+y</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="29"/>
        <source>Ctrl+u</source>
        <translation>Ctrl+u</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="30"/>
        <source>Ctrl+i</source>
        <translation>Ctrl+i</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="37"/>
        <source>Ctrl+o</source>
        <translation>Ctrl+o</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="38"/>
        <source>Ctrl+Shift+O</source>
        <translation>Ctrl+Shift+O</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="45"/>
        <source>Ctrl+,</source>
        <translation>Ctrl+,</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="47"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="53"/>
        <source>Ctrl+Shift+F</source>
        <translation>Ctrl+Shift+F</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="55"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bessel8lvmixeqeffect.cpp" line="15"/>
        <source>Bessel8 LV-Mix Isolator</source>
        <translation>Bessel8 LV-Mix Isolator</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bessel8lvmixeqeffect.cpp" line="16"/>
        <source>Bessel8 ISO</source>
        <translation>Bessel8 ISO</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/bessel8lvmixeqeffect.cpp" line="19"/>
        <source>A Bessel 8th-order filter isolator with Lipshitz and Vanderkooy mix (bit perfect unity, roll-off -48 dB/octave).</source>
        <translation>Een Bessel 8ste-order filterisolator met Lipshitz en Vanderkooy mix (perfecte bit eenheid, roll-off -48 dB/octaaf).</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/linkwitzriley8eqeffect.cpp" line="19"/>
        <source>LinkwitzRiley8 Isolator</source>
        <translation>LinkwitzRiley8 Isolator</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/linkwitzriley8eqeffect.cpp" line="20"/>
        <source>LR8 ISO</source>
        <translation>LR8 ISO</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/linkwitzriley8eqeffect.cpp" line="23"/>
        <source>A Linkwitz-Riley 8th-order filter isolator (optimized crossover, constant phase shift, roll-off -48 dB/octave).</source>
        <translation>Een Linkwitz-Riley 8ste-order filterisolator (geoptimaliseerde crossover, constante faseverschuiving, roll-off -48 dB/octaaf).</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/threebandbiquadeqeffect.cpp" line="57"/>
        <source>Biquad Equalizer</source>
        <translation>Biquad Equalizer</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/threebandbiquadeqeffect.cpp" line="58"/>
        <source>BQ EQ</source>
        <translation>BQ EQ</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/threebandbiquadeqeffect.cpp" line="61"/>
        <source>A 3-band Equalizer with two biquad bell filters, a shelving high pass and kill switches.</source>
        <translation>Een 3-bands equalizer met twee biquad-belfilters, een hoge schaaldoorlaat en demp-schakelaars.</translation>
    </message>
    <message>
        <location filename="../../src/soundio/sounddevicenotfound.h" line="35"/>
        <source>Device not found</source>
        <translation>Apparaat niet gevonden</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/biquadfullkilleqeffect.cpp" line="59"/>
        <source>Biquad Full Kill Equalizer</source>
        <translation>Biquad Full Kill Equalizer</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/biquadfullkilleqeffect.cpp" line="60"/>
        <source>BQ EQ/ISO</source>
        <translation>BQ EQ/ISO</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/biquadfullkilleqeffect.cpp" line="63"/>
        <source>A 3-band Equalizer that combines an Equalizer and an Isolator circuit to offer gentle slopes and full kill.</source>
        <translation>Een 3-bands equalizer die een equalizer en een isolatorcircuit combineert voor zachte responscurven en volledige demping.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="26"/>
        <source>Loudness Contour</source>
        <translation>Geluidomtrek</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="27"/>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="37"/>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="38"/>
        <source>Loudness</source>
        <translation>Geluidsterkte</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="30"/>
        <source>Amplifies low and high frequencies at low volumes to compensate for reduced sensitivity of the human ear.</source>
        <translation>Versterkt lage en hoge frequenties bij lage volumes om de verminderde gevoeligheid van het menselijk oor te compenseren.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="39"/>
        <source>Set the gain of the applied loudness contour</source>
        <translation>Stel de versterking van de toegepaste geluidsterktecontour in</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="52"/>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="53"/>
        <source>Use Gain</source>
        <translation>Gebruik versterking</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/loudnesscontoureffect.cpp" line="54"/>
        <source>Follow Gain Knob</source>
        <translation>Volgknop versterking</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastprofile.cpp" line="78"/>
        <source>This stream is online for testing purposes!</source>
        <translation>Deze stream is enkel online voor testdoeleinden!</translation>
    </message>
    <message>
        <location filename="../../src/preferences/broadcastprofile.cpp" line="79"/>
        <source>Live Mix</source>
        <translation>Live Mix</translation>
    </message>
    <message>
        <location filename="../../src/encoder/encoderflacsettings.cpp" line="23"/>
        <location filename="../../src/encoder/encoderwavesettings.cpp" line="25"/>
        <source>16 bits</source>
        <translation>16 bits</translation>
    </message>
    <message>
        <location filename="../../src/encoder/encoderflacsettings.cpp" line="24"/>
        <location filename="../../src/encoder/encoderwavesettings.cpp" line="26"/>
        <source>24 bits</source>
        <translation>24 bits</translation>
    </message>
    <message>
        <location filename="../../src/encoder/encoderflacsettings.cpp" line="25"/>
        <location filename="../../src/encoder/encoderwavesettings.cpp" line="28"/>
        <source>Bit depth</source>
        <translation>Bit diepte</translation>
    </message>
    <message>
        <location filename="../../src/encoder/encodermp3settings.cpp" line="48"/>
        <source>Bitrate Mode</source>
        <translation>Bitrate Modus</translation>
    </message>
    <message>
        <location filename="../../src/encoder/encoderwavesettings.cpp" line="27"/>
        <source>32 bits float</source>
        <translation>32 bits float</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="21"/>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="30"/>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="31"/>
        <source>Balance</source>
        <translation>Balans</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="24"/>
        <source>Adjust the left/right balance and stereo width</source>
        <translation>Pas de balans links/rechts en de stereobreedte aan</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="32"/>
        <source>Adjust balance between left and right channels</source>
        <translation>Balans aanpassen tussen de linker en rechter kanalen</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="44"/>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="45"/>
        <source>Mid/Side</source>
        <translation>Miden/Zijkant</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="62"/>
        <source>Bypass Fr.</source>
        <translation>FR. omleiden</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="61"/>
        <source>Bypass Frequency</source>
        <translation>Frequentie omleiden</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="20"/>
        <source>Stereo Balance</source>
        <translation>Stereo Balans</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="46"/>
        <source>Adjust stereo width by changing balance between middle and side of the signal.
Fully left: mono
Fully right: only side ambiance
Center: does not change the original signal.</source>
        <translation>Pas de stereobreedte aan door de balans tussen het midden en de zijkant van het signaal te veranderen.
Volledig links: mono
Geheel rechts: enkel zijdelingse sfeer
Centrum: verandert het originele signaal niet.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/balanceeffect.cpp" line="63"/>
        <source>Frequencies below this cutoff are not adjusted in the stereo field</source>
        <translation>Frequenties onder deze cutoff worden niet aangepast in het stereoveld</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="19"/>
        <source>Parametric Equalizer</source>
        <translation>Parametrische Equalizer</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="20"/>
        <source>Param EQ</source>
        <translation>Param EQ</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="23"/>
        <source>An gentle 2-band parametric equalizer based on biquad filters.
It is designed as a complement to the steep mixing equalizers.</source>
        <translation>Een zachte 2-bands parametrische equalizer op basis van biquad-filters.
Het is ontworpen als aanvulling op de sterke meng-equalizers</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="31"/>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="32"/>
        <source>Gain 1</source>
        <translation>Versterking 1</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="33"/>
        <source>Gain for Filter 1</source>
        <translation>Versterking voor Filter 1</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="45"/>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="46"/>
        <source>Q 1</source>
        <translation>Q 1</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="47"/>
        <source>Controls the bandwidth of Filter 1.
A lower Q affects a wider band of frequencies,
a higher Q affects a narrower band of frequencies.</source>
        <translation>Regelt de bandbreedte van filter 1.
Een lagere Q beïnvloedt een bredere frequentieband,
een hogere Q beïnvloedt een smallere frequentieband.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="61"/>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="62"/>
        <source>Center 1</source>
        <translation>Center 1</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="63"/>
        <source>Center frequency for Filter 1, from 100 Hz to 14 kHz</source>
        <translation>Centrale frequentie voor Filter 1, van 100 Hz tot 14 kHz</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="75"/>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="76"/>
        <source>Gain 2</source>
        <translation>Versterking 2</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="77"/>
        <source>Gain for Filter 2</source>
        <translation>Versterking voor Filter 2</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="89"/>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="90"/>
        <source>Q 2</source>
        <translation>Q 2</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="91"/>
        <source>Controls the bandwidth of Filter 2.
A lower Q affects a wider band of frequencies,
a higher Q affects a narrower band of frequencies.</source>
        <translation>Regelt de bandbreedte van filter 2.
Een lagere Q beïnvloedt een bredere frequentieband,
een hogere Q beïnvloedt een smallere frequentieband.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="105"/>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="106"/>
        <source>Center 2</source>
        <translation>Center 2</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/parametriceqeffect.cpp" line="107"/>
        <source>Center frequency for Filter 2, from 100 Hz to 14 kHz</source>
        <translation>Centrale frequentie voor Filter 2, van 100 Hz tot 14 kHz</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="17"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="18"/>
        <source>Tremolo</source>
        <translation>Tremolo</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="21"/>
        <source>Cycles the volume up and down</source>
        <translation>Schakelt het volume omhoog en omlaag</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="29"/>
        <source>How much the effect changes the volume</source>
        <translation>Hoeveel het effect het volume verandert</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="41"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="42"/>
        <source>Rate</source>
        <translation>Snelheid</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="43"/>
        <source>Rate of the volume changes
4 beats - 1/8 beat if tempo is detected
1/4 Hz - 8 Hz if no tempo is detected</source>
        <translation>De snelheid van het volume verandert
4 beats - 1/8 beat als tempo wordt gedetecteerd
1/4 Hz - 8 Hz als er geen tempo wordt gedetecteerd</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="59"/>
        <source>Width of the volume peak
10% - 90% of the effect period</source>
        <translation>Breedte van de volumepiek
10% - 90% van de effectperiode</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="73"/>
        <source>Shape of the volume modulation wave
Fully left: Square wave
Fully right: Sine wave</source>
        <translation>Vorm van de volumemodulatiegolf
Volledig links: blokgolf
Helemaal rechts: sinusgolf</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="119"/>
        <source>When the Quantize parameter is enabled, divide the effect period by 3.</source>
        <translation>Wanneer de Quantize-parameter is ingeschakeld, deelt u de effectperiode door 3.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="71"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="72"/>
        <source>Waveform</source>
        <translation>Waveform</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="87"/>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="88"/>
        <source>Phase</source>
        <translation>Fase</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="89"/>
        <source>Shifts the position of the volume peak within the period
Fully left: beginning of the effect period
Fully right: end of the effect period</source>
        <translation>Verschuift de positie van de volumepiek binnen de periode
Volledig links: begin van de effectperiode
Volledig rechts: einde van de effectperiode</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="105"/>
        <source>Round the Rate parameter to the nearest whole division of a beat.</source>
        <translation>Rond de parametersnelheid af op de dichtstbijzijnde hele deling van een beat.</translation>
    </message>
    <message>
        <location filename="../../src/effects/builtin/tremoloeffect.cpp" line="118"/>
        <source>Triplet</source>
        <translation>Drievoudig</translation>
    </message>
    <message>
        <location filename="../../src/effects/effectmanifest.h" line="161"/>
        <source>Built-in</source>
        <extracomment>Used for effects that are built into Mixxx</extracomment>
        <translation>Ingebouwd</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflv2.cpp" line="45"/>
        <source>This plugin does not support stereo samples as input/output</source>
        <translation>Deze plug-in ondersteunt geen stereosamples als invoer / uitvoer</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflv2.cpp" line="49"/>
        <source>This plugin has features which are not yet supported</source>
        <translation>Deze plug-in heeft functies die nog niet worden ondersteund</translation>
    </message>
    <message>
        <location filename="../../src/preferences/dialog/dlgpreflv2.cpp" line="53"/>
        <source>Unknown status</source>
        <translation>Onbekende status</translation>
    </message>
</context>
<context>
    <name>QtSimpleWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/qtsimplewaveformwidget.h" line="17"/>
        <source>Simple</source>
        <translation>Eenvoudig</translation>
    </message>
</context>
<context>
    <name>QtWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/qtwaveformwidget.h" line="16"/>
        <source>Filtered</source>
        <translation>Gefilterd</translation>
    </message>
</context>
<context>
    <name>RGBWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/rgbwaveformwidget.h" line="15"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
</context>
<context>
    <name>RecordingFeature</name>
    <message>
        <location filename="../../src/library/recording/recordingfeature.cpp" line="32"/>
        <source>Recordings</source>
        <translation>Opnames</translation>
    </message>
</context>
<context>
    <name>RecordingManager</name>
    <message>
        <location filename="../../src/recording/recordingmanager.cpp" line="277"/>
        <source>Low Disk Space Warning</source>
        <translation>Waarschuwing voor onvoldoende schijfruimte</translation>
    </message>
    <message>
        <location filename="../../src/recording/recordingmanager.cpp" line="278"/>
        <source>There is less than 1 GiB of useable space in the recording folder</source>
        <translation>Er is minder dan 1 Gb bruikbare ruimte in de opnamemap</translation>
    </message>
    <message>
        <location filename="../../src/recording/recordingmanager.cpp" line="299"/>
        <source>Recording</source>
        <translation>Opname</translation>
    </message>
    <message>
        <location filename="../../src/recording/recordingmanager.cpp" line="300"/>
        <source>Could not create audio file for recording!</source>
        <translation>Kan geen audiobestand maken voor opname!</translation>
    </message>
    <message>
        <location filename="../../src/recording/recordingmanager.cpp" line="301"/>
        <source>Ensure there is enough free disk space and you have write permission for the Recordings folder.</source>
        <translation>Zorg ervoor dat er voldoende vrije schijfruimte is en dat u schrijfrechten heeft voor de map Opnamen.</translation>
    </message>
    <message>
        <location filename="../../src/recording/recordingmanager.cpp" line="302"/>
        <source>You can change the location of the Recordings folder in Preferences -&gt; Recording.</source>
        <translation>U kunt de locatie van de map Opnamen wijzigen in Voorkeuren -&gt; Opname.</translation>
    </message>
</context>
<context>
    <name>RecordingsView</name>
    <message>
        <location filename="../../src/dlgrecording.cpp" line="117"/>
        <source/>
        <comment>Message shown to user when recording an audio file. %1 is the file path and %2 is the current size of the recording in megabytes (MB)</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RhythmboxFeature</name>
    <message>
        <location filename="../../src/library/rhythmbox/rhythmboxfeature.cpp" line="59"/>
        <location filename="../../src/library/rhythmbox/rhythmboxfeature.cpp" line="450"/>
        <source>Rhythmbox</source>
        <translation>Rhythmbox</translation>
    </message>
</context>
<context>
    <name>SamplerBank</name>
    <message>
        <location filename="../../src/mixer/samplerbank.cpp" line="36"/>
        <source>Save Sampler Bank</source>
        <translation>Save Sampler opslaan</translation>
    </message>
    <message>
        <location filename="../../src/mixer/samplerbank.cpp" line="34"/>
        <location filename="../../src/mixer/samplerbank.cpp" line="38"/>
        <location filename="../../src/mixer/samplerbank.cpp" line="119"/>
        <source>Mixxx Sampler Banks (*.xml)</source>
        <translation>Mixxx Sampler Banks (*.xml)</translation>
    </message>
    <message>
        <location filename="../../src/mixer/samplerbank.cpp" line="56"/>
        <source>Error Saving Sampler Bank</source>
        <translation>Fout bij opslaan van Sampler Bank</translation>
    </message>
    <message>
        <location filename="../../src/mixer/samplerbank.cpp" line="57"/>
        <source>Could not write the sampler bank to &apos;%1&apos;.</source>
        <translation>Kan de sampler bank niet wegschrijven naar &apos;% 1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/mixer/samplerbank.cpp" line="117"/>
        <source>Load Sampler Bank</source>
        <translation>Sampler Bank laden</translation>
    </message>
    <message>
        <location filename="../../src/mixer/samplerbank.cpp" line="126"/>
        <source>Error Reading Sampler Bank</source>
        <translation>Fout bij lezen van de Sampler Bank</translation>
    </message>
    <message>
        <location filename="../../src/mixer/samplerbank.cpp" line="127"/>
        <source>Could not open the sampler bank file &apos;%1&apos;.</source>
        <translation>Het sampler bank-bestand %1 kon niet worden geopend.</translation>
    </message>
</context>
<context>
    <name>SetlogFeature</name>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="32"/>
        <source>Join with previous</source>
        <translation>Voeg toe aan vorige</translation>
    </message>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="36"/>
        <source>Create new history playlist</source>
        <translation>Maak een nieuwe historiekafspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="54"/>
        <location filename="../../src/library/setlogfeature.cpp" line="370"/>
        <source>History</source>
        <translation>Historiek</translation>
    </message>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="94"/>
        <source>Unlock</source>
        <translation>Ontgrendel</translation>
    </message>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="94"/>
        <source>Lock</source>
        <translation>Vergrendelen</translation>
    </message>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="371"/>
        <source>The history section automatically keeps a list of tracks you play in your DJ sets.</source>
        <translation>De geschiedenissectie houdt automatisch een lijst bij van de tracks die je in je dj-sets afspeelt.</translation>
    </message>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="372"/>
        <source>This is handy for remembering what worked in your DJ sets, posting set-lists, or reporting your plays to licensing organizations.</source>
        <translation>Dit is handig om te onthouden wat werkte in uw DJ-sets, set-lijsten te plaatsen of uw set-lijsten te rapporteren aan auteursrechtenorganisaties.</translation>
    </message>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="373"/>
        <source>Every time you start Mixxx, a new history section is created. You can export it as a playlist in various formats or play it again with Auto DJ.</source>
        <translation>Telkens wanneer je Mixxx start, wordt er een nieuwe geschiedenissectie aangemaakt. Je kan deze exporteren als afspeellijst in verschillende formaten of het opnieuw afspelen met Auto DJ.</translation>
    </message>
    <message>
        <location filename="../../src/library/setlogfeature.cpp" line="374"/>
        <source>You can join the current history session with a previous one by right-clicking and selecting &quot;Join with previous&quot;.</source>
        <translation>U kan de huidige geschiedenissessie samenvoegen met een vorige door met de rechtermuisknop te klikken en &quot;samenvoegen met vorige&quot; te selecteren.</translation>
    </message>
</context>
<context>
    <name>ShoutConnection</name>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="78"/>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="83"/>
        <source>Mixxx encountered a problem</source>
        <translation>Mixxx ondervond een probleem</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="79"/>
        <source>Could not allocate shout_t</source>
        <translation>Kon shout_t niet toewijzen</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="84"/>
        <source>Could not allocate shout_metadata_t</source>
        <translation>Kon  shout_metadata_t niet toewijzen</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="89"/>
        <source>Error setting non-blocking mode:</source>
        <translation>Fout bij instellen van niet-blokkerende modus:</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="98"/>
        <source>Error setting tls mode:</source>
        <translation>Fout bij instellen van de tls modus</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="285"/>
        <source>Error setting hostname!</source>
        <translation>Fout bij instellen hostname!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="292"/>
        <source>Error setting port!</source>
        <translation>Fout bij het instellen poort!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="298"/>
        <source>Error setting password!</source>
        <translation>Fout bij het instellen van paswoord!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="304"/>
        <source>Error setting mount!</source>
        <translation>Fout bij het instellen van koppeling!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="310"/>
        <source>Error setting username!</source>
        <translation>Fout bij het instellen van gebruikersnaam!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="315"/>
        <source>Error setting stream name!</source>
        <translation>Fout bij het instellen van stream-naam!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="320"/>
        <source>Error setting stream description!</source>
        <translation>Fout bij het instellen van de stream-omschrijving!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="325"/>
        <source>Error setting stream genre!</source>
        <translation>Fout bij het instellen van het stream-genre!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="330"/>
        <source>Error setting stream url!</source>
        <translation>Fout bij het instellen van de stream-url!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="336"/>
        <source>Error setting stream IRC!</source>
        <translation>Fout bij het instellen van het stream-IRC!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="343"/>
        <source>Error setting stream AIM!</source>
        <translation>Fout bij het instellen van de stream-AIM!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="350"/>
        <source>Error setting stream ICQ!</source>
        <translation>Fout bij het instellen van de stream-ICQ!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="356"/>
        <source>Error setting stream public!</source>
        <translation>Fout bij het instellen van de openbare stream!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="382"/>
        <source>Broadcasting at 96kHz with Ogg Vorbis is not currently supported. Please try a different sample-rate or switch to a different encoding.</source>
        <translation>Uitzending op 96kHz met Ogg Vorbis wordt momenteel niet ondersteund. Probeer een andere sample-rate of schakel over naar een andere codering.</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="385"/>
        <source>See https://bugs.launchpad.net/mixxx/+bug/686212 for more information.</source>
        <translation>Zie https://bugs.launchpad.net/mixxx/+bug/686212 voor meer informatie.</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="393"/>
        <source>Error setting bitrate</source>
        <translation>Fout bij het instellen van de bitrate</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="408"/>
        <source>Error: unknown server protocol!</source>
        <translation>Fout: onbekend serverprotocol!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="413"/>
        <source>Error: libshout only supports Shoutcast with MP3 format!</source>
        <translation>Fout: libshout ondersteunt alleen Shoutcast met MP3-formaat!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="419"/>
        <source>Error setting protocol!</source>
        <translation>Fout bij het instellen van het protocol!</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="647"/>
        <source>Network cache overflow</source>
        <translation>Netwerk cache overflow</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="861"/>
        <source>Connection error</source>
        <translation>Connectiefout</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="862"/>
        <source>One of the Live Broadcasting connections raised this error:&lt;br&gt;&lt;b&gt;Error with connection &apos;%1&apos;:&lt;/b&gt;&lt;br&gt;</source>
        <translation>Een van de Live Uitzending-verbindingen veroorzaakte deze fout:&lt;br&gt;&lt;b&gt;Fout met verbinding &apos;%1&apos;:&lt;/b&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="876"/>
        <source>Connection message</source>
        <translation>Connectiebericht</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="877"/>
        <source>&lt;b&gt;Message from Live Broadcasting connection &apos;%1&apos;:&lt;/b&gt;&lt;br&gt;</source>
        <translation>&lt;b&gt;Bericht van Live Uitzending-verbinding &apos;%1&apos;:&lt;/b&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="927"/>
        <source>Lost connection to streaming server and %1 attempts to reconnect have failed.</source>
        <translation>Verbinding met streaming-server is verbroken en %1 pogingen om opnieuw verbinding te maken, zijn mislukt.</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="930"/>
        <source>Lost connection to streaming server.</source>
        <translation>Verbinding met de streaming-server verbroken.</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="935"/>
        <source>Please check your connection to the Internet.</source>
        <translation>Controleer uw verbinding met internet.</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="970"/>
        <source>Can&apos;t connect to streaming server</source>
        <translation>Kan niet verbinden met de streaming-server</translation>
    </message>
    <message>
        <location filename="../../src/engine/sidechain/shoutconnection.cpp" line="972"/>
        <source>Please check your connection to the Internet and verify that your username and password are correct.</source>
        <translation>Controleer uw verbinding met internet en controleer of uw gebruikersnaam en wachtwoord correct zijn.</translation>
    </message>
</context>
<context>
    <name>SoftwareWaveformWidget</name>
    <message>
        <location filename="../../src/waveform/widgets/softwarewaveformwidget.h" line="15"/>
        <source>Filtered</source>
        <translation>Gefilterd</translation>
    </message>
</context>
<context>
    <name>SoundManager</name>
    <message>
        <location filename="../../src/soundio/soundmanager.cpp" line="548"/>
        <location filename="../../src/soundio/soundmanager.cpp" line="553"/>
        <source>a device</source>
        <translation>Een apparaat</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanager.cpp" line="554"/>
        <source>An unknown error occurred</source>
        <translation>Er is een onbekende fout opgetreden</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanager.cpp" line="562"/>
        <source>Two outputs cannot share channels on &quot;%1&quot;</source>
        <translation>Twee uitgangen kunnen geen kanalen delen op %1</translation>
    </message>
    <message>
        <location filename="../../src/soundio/soundmanager.cpp" line="565"/>
        <source>Error opening &quot;%1&quot;</source>
        <translation>Fout bij het openen van &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>StatModel</name>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="9"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="10"/>
        <source>Count</source>
        <translation>Tellen</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="11"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="12"/>
        <source>Units</source>
        <translation>Eenheden</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="13"/>
        <source>Sum</source>
        <translation>Som</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="14"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="15"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="16"/>
        <source>Mean</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="17"/>
        <source>Variance</source>
        <translation>Variantie</translation>
    </message>
    <message>
        <location filename="../../src/util/statmodel.cpp" line="18"/>
        <source>Standard Deviation</source>
        <translation>Standaard omleiding</translation>
    </message>
</context>
<context>
    <name>TagFetcher</name>
    <message>
        <location filename="../../src/musicbrainz/tagfetcher.cpp" line="54"/>
        <source>Fingerprinting track</source>
        <translation>Vingerafdruk maken van track</translation>
    </message>
    <message>
        <location filename="../../src/musicbrainz/tagfetcher.cpp" line="86"/>
        <source>Identifying track</source>
        <translation>Track identificeren</translation>
    </message>
    <message>
        <location filename="../../src/musicbrainz/tagfetcher.cpp" line="103"/>
        <source>Downloading Metadata</source>
        <translation>Downloaden van Metadata</translation>
    </message>
</context>
<context>
    <name>Tooltips</name>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="29"/>
        <source>Reset to default value.</source>
        <translation>Reset naar standaardwaarde.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="30"/>
        <source>Left-click</source>
        <translation>Linkerklik</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="31"/>
        <source>Right-click</source>
        <translation>Rechterklik</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="32"/>
        <source>Scroll-wheel</source>
        <translation>Scrollwiel</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="33"/>
        <source>loop active</source>
        <translation>loop actief</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="34"/>
        <source>loop inactive</source>
        <translation>loop inactief</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="35"/>
        <source>Effects within the chain must be enabled to hear them.</source>
        <translation>Effecten binnen de keten moeten ingeschakeld zijn om ze te kunnen horen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="38"/>
        <source>Waveform Overview</source>
        <translation>Waveform Overzicht</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="39"/>
        <source>Shows information about the track currently loaded in this channel.</source>
        <translation>Toont informatie over de track die momenteel in dit kanaal is geladen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="43"/>
        <source>Use the mouse to scratch, spin-back or throw tracks.</source>
        <translation>Gebruik de muis om te scratchen, te backenspinnen of de track in je set te gooien.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="45"/>
        <source>Waveform Display</source>
        <translation>Waveform weergave</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="46"/>
        <source>Shows the loaded track&apos;s waveform near the playback position.</source>
        <translation>Toont de waveform van de geladen track nabij de afspeelpositie.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="48"/>
        <source>Drag with mouse to make temporary pitch adjustments.</source>
        <translation>Sleep met de muis om de toonhoogte tijdelijk aan te passen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="49"/>
        <source>Scroll to change the waveform zoom level.</source>
        <translation>Scroll om het zoomniveau van de waveform te wijzigen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="53"/>
        <source>Waveform Zoom Out</source>
        <translation>Waveform Uitzoomen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="56"/>
        <source>Waveform Zoom In</source>
        <translation>Waveform Inzoomen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="59"/>
        <source>Waveform Zoom</source>
        <translation>Waveform Zoom</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="63"/>
        <location filename="../../src/skin/tooltips.cpp" line="383"/>
        <source>Spinning Vinyl</source>
        <translation>Spinnend Vinyl</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="64"/>
        <source>Rotates during playback and shows the position of a track.</source>
        <translation>Draait tijdens het afspelen en toont de positie van een track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="66"/>
        <source>Right click to show cover art of loaded track.</source>
        <translation>Klik rechtermuisknop om de albumhoes van het geladen nummer weer te geven.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="71"/>
        <source>Gain</source>
        <translation>Versterken</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="72"/>
        <source>Adjusts the pre-fader gain of the track (to avoid clipping).</source>
        <translation>Past de pre-fader versterking van de track aan (om clipping te voorkomen).</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="75"/>
        <source>(too loud for the hardware and is being distorted).</source>
        <translation>(te luid voor de hardware en wordt vervormd).</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="78"/>
        <source>Indicates when the signal on the channel is clipping,</source>
        <translation>Geeft aan wanneer het signaal op het kanaal wordt verknipt,</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="92"/>
        <source>Master Peak Indicator</source>
        <translation>Master Peak Indicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="93"/>
        <source>Indicates when the signal on the master output is clipping,</source>
        <translation>Geeft aan wanneer het signaal op de hoofduitgang wordt verknipt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="107"/>
        <source>Channel Volume Meter</source>
        <translation>Volumemeter van het kanaal</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="108"/>
        <source>Shows the current channel volume.</source>
        <translation>Toont het huidige kanaalvolume</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="119"/>
        <source>Microphone Volume Meter</source>
        <translation>Microfoon Volume Meter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="120"/>
        <source>Shows the current microphone volume.</source>
        <translation>Toont het huidige microfoonvolume.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="147"/>
        <source>Shows the current master volume for the left channel.</source>
        <translation>Toont het huidige hoofdvolume voor het linker kanaal.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="150"/>
        <source>Master Channel R Volume Meter</source>
        <translation>Volumemeter voor Master-kanaal R</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="154"/>
        <source>Volume Control</source>
        <translation>Volumeregelaar</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="155"/>
        <source>Adjusts the volume of the selected channel.</source>
        <translation>Past het volume van het geselecteerd kanaal aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="160"/>
        <source>Master Volume</source>
        <translation>Hoofdvolume</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="161"/>
        <source>Adjusts the master output volume.</source>
        <translation>Past het volume van de masteruitgang aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="165"/>
        <source>Master Gain</source>
        <translation>Hoofduitgang Versterking</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="166"/>
        <source>Adjusts the master output gain.</source>
        <translation>Past de versterking van de hoofduitgang aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="170"/>
        <source>Booth Gain</source>
        <translation>Booth Versterking</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="171"/>
        <source>Adjusts the booth output gain.</source>
        <translation>Pas de booth uitgang aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="175"/>
        <source>Crossfader</source>
        <translation>Crossfader</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="176"/>
        <source>Determines the master output by fading between the left and right channels.</source>
        <translation>Bepaalt wat de hoofduitgang is door te faden tussen de linker- en rechterkanalen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="181"/>
        <source>Balance</source>
        <translation>Balans</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="182"/>
        <source>Adjusts the left/right channel balance on the master output.</source>
        <translation>Past de balans van het linker/rechter kanaal op de hoofduitgang aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="187"/>
        <source>Headphone Volume</source>
        <translation>Volume Hoofdtelefoon</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="188"/>
        <source>Adjusts the headphone output volume.</source>
        <translation>Past het volume van de hoofdtelefoon aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="192"/>
        <source>Headphone Gain</source>
        <translation>Hoofdtelefoon Versterking</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="193"/>
        <source>Adjusts the headphone output gain.</source>
        <translation>Past de versterking van de hoofdtelefoonuitgang aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="197"/>
        <source>Headphone Mix</source>
        <translation>Hoofdtelefoon Mix</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="202"/>
        <source>Headphone Split Cue</source>
        <translation>Hoofdtelefoon Split Cue</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="203"/>
        <source>If activated, the master signal plays in the right channel, while the cueing signal plays in the left channel.</source>
        <translation>Indien geactiveerd, speelt het mastersignaal in het rechterkanaal, terwijl het cueing-signaal in het linkerkanaal speelt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="205"/>
        <source>Adjust the Headphone Mix so in the left channel is not the pure cueing signal.</source>
        <translation>Pas de hoofdtelefoonmix zo aan dat in het linkerkanaal niet het pure cueing-signaal word weergegeven.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="213"/>
        <source>Microphone</source>
        <translation>Microfoon</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="214"/>
        <source>Show/hide the Microphone section.</source>
        <translation>Toon/verberg de microfoonsectie.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="217"/>
        <source>Sampler</source>
        <translation>Sampler</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="218"/>
        <source>Show/hide the Sampler section.</source>
        <translation>Toon/verberd de Samplersectie.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="221"/>
        <source>Vinyl Control</source>
        <translation>Vinyl Control</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="222"/>
        <source>Show/hide the Vinyl Control section.</source>
        <translation>Toon/verberg de sectie Vinylbesturing.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="226"/>
        <source>Preview Deck</source>
        <translation>Preview Deck</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="227"/>
        <source>Show/hide the Preview deck.</source>
        <translation>Toon/verberg de Preview speler.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="230"/>
        <location filename="../../src/skin/tooltips.cpp" line="726"/>
        <source>Cover Art</source>
        <translation>Cover Art</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="231"/>
        <source>Show/hide Cover Art.</source>
        <translation>Toon/verberg Albumhoezen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="234"/>
        <source>Toggle 4 Decks</source>
        <translation>Wissel naar 4 Spelers </translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="235"/>
        <source>Switches between showing 2 decks and 4 decks.</source>
        <translation>Schakelt tussen weergave van 2 spelers en 4 spelers.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="238"/>
        <source>Show Library</source>
        <translation>Toon bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="239"/>
        <source>Show or hide the track library.</source>
        <translation>Toon of verberg de track bibliotheek.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="242"/>
        <source>Show Effects</source>
        <translation>Toon Effecten</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="243"/>
        <source>Show or hide the effects.</source>
        <translation>Toon of verberg de effecten.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="250"/>
        <source>Toggle Mixer</source>
        <translation>Schakel Mixer</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="251"/>
        <source>Show or hide the mixer.</source>
        <translation>Toon of verberg de mixer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="254"/>
        <source>Microphone Volume</source>
        <translation>Microfoonvolume</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="255"/>
        <source>Adjusts the microphone volume.</source>
        <translation>Past het volume voor de microfoon aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="259"/>
        <source>Microphone Gain</source>
        <translation>Microfoon versterking</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="260"/>
        <source>Adjusts the pre-fader microphone gain.</source>
        <translation>Past de pre-fader versterking van de microfoon aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="264"/>
        <source>Microphone Talk-Over</source>
        <translation>Microfoon Talk-Over</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="265"/>
        <source>Hold-to-talk or short click for latching to</source>
        <translation>Vasthouden voor praten of kort klikken voor vergrendeling om</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="266"/>
        <source>mix microphone input into the master output.</source>
        <translation>mix de microfooninvoer naar de hoofduitgang</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="269"/>
        <source>Microphone Talkover Mode</source>
        <translation>Microfoon Talkover Modus</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="270"/>
        <source>Off: Do not reduce music volume</source>
        <translation>Uit: verlaag het muziekvolume niet</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="271"/>
        <source>Auto: Automatically reduce music volume when microphones are in use. Adjust the amount the music volume is reduced with the Strength knob.</source>
        <translation>Auto: verlaagt automatisch het muziekvolume wanneer microfoons worden gebruikt. Aanpassen van het muziekvolume wordt verlaagd met de knop voor dempingssterkte.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="272"/>
        <source>Manual: Reduce music volume by a fixed amount set by the Strength knob.</source>
        <translation>Manueel: Verlaag het muziekvolume met een vaste waarde, die wordt ingesteld met de knop voor dempingssterkte.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="276"/>
        <source>Behavior depends on Microphone Talkover Mode:</source>
        <translation>Het gedrag is afhankelijk van de Talkover-modus van de microfoon:</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="277"/>
        <source>Off: Does nothing</source>
        <translation>Uit: Doet niets</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="278"/>
        <source>Auto: Sets how much to reduce the music volume when microphones are in use.</source>
        <translation>Auto: hiermee stelt u in hoeveel het muziekvolume moet worden verlaagd wanneer microfoons worden gebruikt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="279"/>
        <source>Manual: Sets how much to reduce the music volume, regardless of volume of microphone inputs.</source>
        <translation>Manueel: Stelt in hoeveel het muziekvolume moet worden verlaagd, ongeacht het volume van de microfooningangen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="283"/>
        <source>Raise Pitch</source>
        <translation>Verhoog de toon</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="284"/>
        <source>Sets the pitch higher.</source>
        <translation>Stelt de toon hoger in</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="285"/>
        <source>Sets the pitch higher in small steps.</source>
        <translation>Toon hoger instellen in kleine stappen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="289"/>
        <source>Lower Pitch</source>
        <translation>Verlaag toon</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="290"/>
        <source>Sets the pitch lower.</source>
        <translation>Stelt de toon lager in.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="291"/>
        <source>Sets the pitch lower in small steps.</source>
        <translation>Zet de toon lager in kleine stappen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="295"/>
        <source>Raise Pitch Temporary (Nudge)</source>
        <translation>Verhoog toon tijdelijk (aanduwen)</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="296"/>
        <source>Holds the pitch higher while active.</source>
        <translation>Houdt de toonhoogte hoger terwijl deze actief is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="297"/>
        <source>Holds the pitch higher (small amount) while active.</source>
        <translation>Houdt de toonhoogte hoger terwijl deze actief is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="301"/>
        <source>Lower Pitch Temporary (Nudge)</source>
        <translation>Verlaag toon tijdelijk (aanduwen)</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="302"/>
        <source>Holds the pitch lower while active.</source>
        <translation>Houdt de toonhoogte lager terwijl deze actief is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="303"/>
        <source>Holds the pitch lower (small amount) while active.</source>
        <translation>Houdt de toonhoogte lager (kleine hoeveelheid) terwijl deze actief is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="307"/>
        <source>Low EQ</source>
        <translation>Low EQ</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="308"/>
        <source>Adjusts the gain of the low EQ filter.</source>
        <translation>Past de versterking van het lage EQ-filter aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="312"/>
        <source>Mid EQ</source>
        <translation>Mid EQ</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="313"/>
        <source>Adjusts the gain of the mid EQ filter.</source>
        <translation>Past de versterking van het midden EQ-filter aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="317"/>
        <source>High EQ</source>
        <translation>High EQ</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="318"/>
        <source>Adjusts the gain of the high EQ filter.</source>
        <translation>Past de versterking van het hoge EQ-filter aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="321"/>
        <source>Hold-to-kill or short click for latching.</source>
        <translation>Vasthouden om te dempen of kort klikken om te vergrendelen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="323"/>
        <source>High EQ Kill</source>
        <translation>Demp Hoge EQ</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="324"/>
        <source>Holds the gain of the high EQ to zero while active.</source>
        <translation>Houdt de versterking van de hoge EQ op nul terwijl deze actief is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="328"/>
        <source>Mid EQ Kill</source>
        <translation>Demp Midden EQ</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="329"/>
        <source>Holds the gain of the mid EQ to zero while active.</source>
        <translation>Houdt de versterking van de midden EQ op nul terwijl deze actief is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="333"/>
        <source>Low EQ Kill</source>
        <translation>Demp Lage EQ</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="334"/>
        <source>Holds the gain of the low EQ to zero while active.</source>
        <translation>Houdt de versterking van de lage EQ op nul terwijl deze actief is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="337"/>
        <source>Displays the tempo of the loaded track in BPM (beats per minute).</source>
        <translation>Toont het tempo van het geladen track in BPM (slagen per minuut).</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="339"/>
        <source>Tempo</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="344"/>
        <source>Key</source>
        <translation>Toonaard</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="348"/>
        <source>BPM Tap</source>
        <translation>BPM Tik</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="349"/>
        <location filename="../../src/skin/tooltips.cpp" line="380"/>
        <source>When tapped repeatedly, adjusts the BPM to match the tapped BPM.</source>
        <translation>Bij herhaaldelijk tikken, wordt de BPM aangepast zodat deze overeenkomt met de getikte BPM.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="352"/>
        <source>Adjust BPM Down</source>
        <translation>Pas BPM aan naar omlaag</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="353"/>
        <source>When tapped, adjusts the average BPM down by a small amount.</source>
        <translation>Wanneer erop wordt getikt, wordt de gemiddelde BPM met een kleine waarde verlaagd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="356"/>
        <source>Adjust BPM Up</source>
        <translation>Pas BPM aan naar omhoog</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="357"/>
        <source>When tapped, adjusts the average BPM up by a small amount.</source>
        <translation>Wanneer erop wordt getikt, wordt de gemiddelde BPM met een kleine waarde verhoogd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="360"/>
        <source>Adjust Beats Earlier</source>
        <translation>Verschuift de beats naar voor</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="361"/>
        <source>When tapped, moves the beatgrid left by a small amount.</source>
        <translation>Wanneer erop getikt wordt, zal het beat-raster naar links verplaatst worden met een kleine waarde.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="364"/>
        <source>Adjust Beats Later</source>
        <translation>Verschuift de beats naar achter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="365"/>
        <source>When tapped, moves the beatgrid right by a small amount.</source>
        <translation>Wanneer erop getikt wordt, zal het beat-raster naar rechts verplaatst worden met een kleine waarde.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="378"/>
        <source>Tempo and BPM Tap</source>
        <translation>Tempo en BPM Tap</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="384"/>
        <source>Show/hide the spinning vinyl section.</source>
        <translation>Toon/verberg de draaiende vinylsectie</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="387"/>
        <source>Keylock</source>
        <translation>Keylock</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="389"/>
        <source>Toggling keylock during playback may result in a momentary audio glitch.</source>
        <translation>Het omschakelen van de toetsvergrendeling tijdens het afspelen kan resulteren in een kortstondige audio-storing.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="396"/>
        <source>Toggle visibility of Rate Control</source>
        <translation>Omschakeling van de zichtbaarheid van Rate Control</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="419"/>
        <source>Places a cue point at the current position on the waveform.</source>
        <translation>Plaatst een cue-punt op de huidige positie op de waveform.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="427"/>
        <source>Stops track at cue point, OR go to cue point and play after release (CUP mode).</source>
        <translation>Stopt track op cue point, OF ga naar cue point en speel na loslaten (CUP-modus).</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="428"/>
        <source>Set cue point (Pioneer/Mixxx/Numark mode), set cue point and play after release (CUP mode) OR preview from it (Denon mode).</source>
        <translation>Stel het cue-punt in (Pioneer/Mixxx/Numark-modus), stel het cue-punt in en speel na het loslaten (CUP-modus) OF bekijk er een voorbeeld van (Denon-modus).</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="437"/>
        <source>Seeks the track to the cue point and stops.</source>
        <translation>Doorzoekt de track naar het cue-punt en stopt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="439"/>
        <source>Play</source>
        <translation>Speel</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="440"/>
        <source>Plays track from the cue point.</source>
        <translation>Speelt de track vanaf het cuepunt</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="498"/>
        <source>Changes the track playback speed (affects both the tempo and the pitch). If keylock is enabled, only the tempo is affected.</source>
        <translation>Wijzigt de afspeelsnelheid van de track (beïnvloedt zowel het tempo als de toonhoogte). Als keylock is ingeschakeld, wordt alleen het tempo beïnvloed.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="550"/>
        <source>Recording Duration</source>
        <translation>Opnameduur</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="551"/>
        <source>Displays the duration of the running recording.</source>
        <translation>Geeft de duur van de lopende opname weer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="594"/>
        <source>Sets the track Loop-In Marker to the current play position.</source>
        <translation>Stelt de track Loop-In Marker in op de huidige afspeelpositie.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="597"/>
        <source>Press and hold to move Loop-In Marker.</source>
        <translation>Houd ingedrukt om Loop-In Marker te verplaatsen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="598"/>
        <source>Jump to Loop-In Marker.</source>
        <translation>Spring naar Loop-In Marker.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="603"/>
        <source>Sets the track Loop-Out Marker to the current play position.</source>
        <translation>Stelt de track Loop-Out Marker in op de huidige afspeelpositie.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="606"/>
        <source>Press and hold to move Loop-Out Marker.</source>
        <translation>Houd ingedrukt om Loop-Out Marker te verplaatsen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="607"/>
        <source>Jump to Loop-Out Marker.</source>
        <translation>Spring naar Loop-Out Marker.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="619"/>
        <source>Beatloop Size</source>
        <translation>Beatloop maat</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="620"/>
        <source>Select the size of the loop in beats to set with the Beatloop button.</source>
        <translation>Selecteer de maat van de loop in beats om de Beatloop-knop  in te stellen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="621"/>
        <source>Changing this resizes the loop if the loop already matches this size.</source>
        <translation>Als u dit wijzigt, wordt de maat van de loop aangepast als de loop al overeenkomt met deze grootte.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="624"/>
        <source>Halve the size of an existing beatloop, or halve the size of the next beatloop set with the Beatloop button.</source>
        <translation>Halveer de maat van een bestaande beatloop of halveer de maat van de volgende beatloop die ingesteld is met de Beatloop-knop.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="627"/>
        <source>Double the size of an existing beatloop, or double the size of the next beatloop set with the Beatloop button.</source>
        <translation>Verdubbel de maat van een bestaande beatloop of verdubbel de maat van de volgende beatloop die ingesteld is met de Beatloop-knop.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="632"/>
        <source>Start a loop over the set number of beats.</source>
        <translation>Start een loop over het ingestelde aantal beats.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="634"/>
        <source>Temporarily enable a rolling loop over the set number of beats.</source>
        <translation>Schakel tijdelijk een loop-roll in over het ingestelde aantal beats.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="638"/>
        <source>Beatjump/Loop Move Size</source>
        <translation>Beatjump / Loop Move Maat</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="639"/>
        <source>Select the number of beats to jump or move the loop with the Beatjump Forward/Backward buttons.</source>
        <translation>Selecteer het aantal beats om te verspringen of de loop te verplaatsen met de Beatjump Vooruit/Achteruit knoppen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="642"/>
        <source>Beatjump Forward</source>
        <translation>Beatjump voorwaarts</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="643"/>
        <source>Jump forward by the set number of beats.</source>
        <translation>Spring vooruit met het ingestelde aantal beats.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="644"/>
        <source>Move the loop forward by the set number of beats.</source>
        <translation>Verplaats de loop vooruit met het ingestelde aantal beats.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="645"/>
        <source>Jump forward by 1 beat.</source>
        <translation>Spring 1 beat vooruit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="646"/>
        <source>Move the loop forward by 1 beat.</source>
        <translation>Verplaats de loop met 1 beat vooruit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="649"/>
        <source>Beatjump Backward</source>
        <translation>Beatjump achteruit</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="650"/>
        <source>Jump backward by the set number of beats.</source>
        <translation>Spring achteruit met het ingestelde aantal beats.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="651"/>
        <source>Move the loop backward by the set number of beats.</source>
        <translation>Verplaats de loop achteruit met het ingestelde aantal beats.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="652"/>
        <source>Jump backward by 1 beat.</source>
        <translation>Spring 1 beat achteruit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="653"/>
        <source>Move the loop backward by 1 beat.</source>
        <translation>Verplaats de loop 1 beat achteruit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="661"/>
        <source>Reloop</source>
        <translation>Reloop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="663"/>
        <source>If the loop is ahead of the current position, looping will start when the loop is reached.</source>
        <translation>Als de loop voor de huidige positie ligt, begint het loopen wanneer de loop is bereikt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="664"/>
        <source>Works only if Loop-In and Loop-Out Marker are set.</source>
        <translation>Werkt alleen als Loop-In en Loop-Out Markeerpunten zijn ingesteld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="665"/>
        <source>Enable loop, jump to Loop-In Marker, and stop playback.</source>
        <translation>Schakel loop in, spring naar het Loop-In Markeerpunt en stop het afspelen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="674"/>
        <source>Displays the elapsed and/or remaining time of the track loaded.</source>
        <translation>Geeft de verstreken en/of resterende tijd van de geladen track weer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="675"/>
        <source>Click to toggle between time elapsed/remaining time/both.</source>
        <translation>Klik om te wisselen tussen verstreken/resterende tijd of beide.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="747"/>
        <source>Mix</source>
        <translation>Mix</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="748"/>
        <source>Adjust the mixing of the dry (input) signal with the wet (output) signal of the effect unit</source>
        <translation>Pas de mix van het onbewerkte (droge) ingangssignaal aan met het verwerkte (natte) uitgangssignaal van de effecteenheid</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="749"/>
        <source>D/W mode: Crossfade between dry and wet</source>
        <translation>D/W-modus: overgang tussen onbewerkt en verwerkt</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="750"/>
        <source>D+W mode: Add wet to dry</source>
        <translation>D+W modus: voeg verwerkt (wet) toe aan onbewerkt (dry)</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="754"/>
        <source>Mix Mode</source>
        <translation>Mix Modus</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="755"/>
        <source>Adjust how the dry (input) signal is mixed with the wet (output) signal of the effect unit</source>
        <translation>Pas aan hoe het onbewerkte (invoer) signaal wordt gemengd met het bewerkte (uitvoer) signaal van de effecteenheid</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="756"/>
        <source>Dry/Wet mode (crossed lines): Mix knob crossfades between dry and wet
Use this to change the sound of the track with EQ and filter effects.</source>
        <translation>Dry/Wet-modus (gekruiste lijnen): Mix-knop vloeit over tussen bewerkt en onbewerkt
Gebruik dit om het geluid van de track te veranderen met EQ en filtereffecten.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="758"/>
        <source>Dry+Wet mode (flat dry line): Mix knob adds wet to dry
Use this to change only the effected (wet) signal with EQ and filter effects.</source>
        <translation>Dry + Wet Mode (Flat Dry Line): Mix-knop voegt Wet to Dry toe
Gebruik deze optie om alleen het verwerkte (Wet) signaal met EQ en filtereffecten te wijzigen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="795"/>
        <source>Route the left crossfader bus through this effect unit.</source>
        <translation>Leid de linker crossfaderbus door dit effectapparaat.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="800"/>
        <source>Route the right crossfader bus through this effect unit.</source>
        <translation>Leid de rechter crossfaderbus door dit effectapparaat.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="866"/>
        <source>Right side active: parameter moves with right half of Meta Knob turn</source>
        <translation>Rechterkant actief: parameter beweegt met de rechterhelft van de Meta-knop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="903"/>
        <source>Skin Settings Menu</source>
        <translation>Menu Skin-instellingen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="904"/>
        <source>Show/hide skin settings menu</source>
        <translation>Toon/verberg menu Skin-instellingen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="908"/>
        <source>Save Sampler Bank</source>
        <translation>Save Sampler opslaan</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="909"/>
        <source>Save the collection of samples loaded in the samplers.</source>
        <translation>Sla de collectie van samples op die in de samplers zijn geladen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="912"/>
        <source>Load Sampler Bank</source>
        <translation>Sampler Bank laden</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="913"/>
        <source>Load a previously saved collection of samples into the samplers.</source>
        <translation>Laad een eerder opgeslagen collectie samples op in de samplers.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="740"/>
        <source>Show Effect Parameters</source>
        <translation>Toon Effect Parameters</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="829"/>
        <source>Enable Effect</source>
        <translation>Effect inschakelen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="861"/>
        <source>Meta Knob Link</source>
        <translation>Meta Knob Link</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="862"/>
        <source>Set how this parameter is linked to the effect&apos;s Meta Knob.</source>
        <translation>Stel in hoe deze parameter wordt gekoppeld aan het effect van de metaknop.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="870"/>
        <source>Meta Knob Link Inversion</source>
        <translation>Meta Knob Link Inversie</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="871"/>
        <source>Inverts the direction this parameter moves when turning the effect&apos;s Meta Knob.</source>
        <translation>Keert de richting om die deze parameter beweegt, wanneer u aan de metaknop van het effect draait.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="762"/>
        <source>Super Knob</source>
        <translation>Super knop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="767"/>
        <source>Next Chain</source>
        <translation>Volgende Schakel</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="771"/>
        <source>Previous Chain</source>
        <translation>Vorige keten</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="775"/>
        <source>Next/Previous Chain</source>
        <translation>Volgende/Vorige Keten</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="825"/>
        <source>Clear</source>
        <translation>Wissen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="826"/>
        <source>Clear the current effect.</source>
        <translation>Wis het huidige effect.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="886"/>
        <source>Toggle</source>
        <translation>Omschakelen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="887"/>
        <source>Toggle the current effect.</source>
        <translation>Schakel het huidige effect om.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="833"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="736"/>
        <source>Clear Unit</source>
        <translation>Wis Eenheid</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="737"/>
        <source>Clear effect unit.</source>
        <translation>Wis de effect-eenheid</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="741"/>
        <source>Show/hide parameters for effects in this unit.</source>
        <translation>Toon/verberg parameters voor effecten in deze unit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="744"/>
        <source>Toggle Unit</source>
        <translation>Wissel Eenheid</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="745"/>
        <source>Enable or disable this whole effect unit.</source>
        <translation>In of uitschakelen van deze hele effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="763"/>
        <source>Controls the Meta Knob of all effects in this unit together.</source>
        <translation>Bestuurt de metaknop van alle effecten samen in deze eenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="768"/>
        <source>Load next effect chain preset into this effect unit.</source>
        <translation>Laad de volgende vooraf ingestelde effectketen in deze effecteenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="772"/>
        <source>Load previous effect chain preset into this effect unit.</source>
        <translation>Laad vorige vooraf ingestelde effectenketen in deze effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="776"/>
        <source>Load next or previous effect chain preset into this effect unit.</source>
        <translation>Laad vorige of volgende vooraf ingestelde effectenketen in deze effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="779"/>
        <location filename="../../src/skin/tooltips.cpp" line="784"/>
        <location filename="../../src/skin/tooltips.cpp" line="789"/>
        <location filename="../../src/skin/tooltips.cpp" line="794"/>
        <location filename="../../src/skin/tooltips.cpp" line="799"/>
        <location filename="../../src/skin/tooltips.cpp" line="804"/>
        <location filename="../../src/skin/tooltips.cpp" line="809"/>
        <location filename="../../src/skin/tooltips.cpp" line="814"/>
        <location filename="../../src/skin/tooltips.cpp" line="819"/>
        <source>Assign Effect Unit</source>
        <translation>Wijs effecteneenheid toe</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="780"/>
        <source>Assign this effect unit to the channel output.</source>
        <translation>Wijs deze effecteneenheid toe aan de kanaaluitvoer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="785"/>
        <source>Route the headphone channel through this effect unit.</source>
        <translation>Stuur het hoofdtelefoonkanaal door deze effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="790"/>
        <source>Route the master mix through this effect unit.</source>
        <translation>Stuur de hoofd-mix door deze effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="805"/>
        <source>Route this deck through the indicated effect unit.</source>
        <translation>Stuur deze spelen door de aangegeven effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="810"/>
        <source>Route this sampler through the indicated effect unit.</source>
        <translation>Stuur deze sampler door de aangegeven effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="815"/>
        <source>Route this microphone through the indicated effect unit.</source>
        <translation>Stuur de microfoon door de aangegeven effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="820"/>
        <source>Route this auxiliary input through the indicated effect unit.</source>
        <translation>Stuur de auxiliary ingang door de aangegeven effecteneenheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="830"/>
        <source>The effect unit must also be assigned to a deck or other sound source to hear the effect.</source>
        <translation>De effecteenheid moet ook zijn toegewezen aan een speler of andere geluidsbron om het effect te horen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="834"/>
        <source>Switch to the next effect.</source>
        <translation>Schakel over naar het volgende effect.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="837"/>
        <source>Previous</source>
        <translation>Vorige</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="838"/>
        <source>Switch to the previous effect.</source>
        <translation>Schakel over naar het vorige effect.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="841"/>
        <source>Next or Previous</source>
        <translation>Volgende of Vorige</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="842"/>
        <source>Switch to either the next or previous effect.</source>
        <translation>Schakel over naar het volgende of vorige effect.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="845"/>
        <source>Meta Knob</source>
        <translation>Meta Knop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="846"/>
        <source>Controls linked parameters of this effect</source>
        <translation>Bestuurt gekoppelde parameters van dit effect</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="850"/>
        <source>Effect Focus Button</source>
        <translation>Effect Focus knop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="851"/>
        <source>Focuses this effect.</source>
        <translation>Focust op dit effect</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="852"/>
        <source>Unfocuses this effect.</source>
        <translation>Neemt de focus weg van dit effect.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="853"/>
        <source>Refer to the web page on the Mixxx wiki for your controller for more information.</source>
        <translation>Raadpleeg voor meer informatie de webpagina op de Mixxx-wiki voor je controller .</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="856"/>
        <source>Effect Parameter</source>
        <translation>Effect Parameter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="857"/>
        <source>Adjusts a parameter of the effect.</source>
        <translation>Past een parameter van het effect aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="863"/>
        <source>Inactive: parameter not linked</source>
        <translation>Inactief: parameter niet gekoppeld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="864"/>
        <source>Active: parameter moves with Meta Knob</source>
        <translation>Actief: parameter beweegt met metaknop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="865"/>
        <source>Left side active: parameter moves with left half of Meta Knob turn</source>
        <translation>Linkerkant actief: parameter beweegt met linkerhelft van de metaknopdraai</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="867"/>
        <source>Left and right side active: parameter moves across range with half of Meta Knob turn and back with the other half</source>
        <translation>Linker- en rechterkant actief: parameter beweegt over bereik met de helft van de metaknopdraai en terug met de andere helft</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="874"/>
        <location filename="../../src/skin/tooltips.cpp" line="898"/>
        <source>Equalizer Parameter Kill</source>
        <translation>Demp Equalizer Parameter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="875"/>
        <location filename="../../src/skin/tooltips.cpp" line="899"/>
        <source>Holds the gain of the EQ to zero while active.</source>
        <translation>Houdt de versterking van de EQ op nul terwijl deze actief is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="880"/>
        <source>Quick Effect Super Knob</source>
        <translation>Superknop Quick Effect</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="881"/>
        <source>Quick Effect Super Knob (control linked effect parameters).</source>
        <translation>Superknop Quick Effect ( controle op gekoppelde effectparameters).</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="883"/>
        <source>Hint: Change the default Quick Effect mode in Preferences -&gt; Equalizers.</source>
        <translation>Hint: Wijzig de standaard Quick Effect-modus in Voorkeuren -&gt; Equalizers.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="892"/>
        <source>Equalizer Parameter</source>
        <translation>Equalizer Parameter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="893"/>
        <source>Adjusts the gain of the EQ filter.</source>
        <translation>Pas de versterking van het EQ filter aan.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="895"/>
        <source>Hint: Change the default EQ mode in Preferences -&gt; Equalizers.</source>
        <translation>Hint: Wijzig de standaard EQ-modus in Voorkeuren -&gt; Equalizers.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="368"/>
        <location filename="../../src/skin/tooltips.cpp" line="373"/>
        <source>Adjust Beatgrid</source>
        <translation>Pas Beat-raster aan</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="369"/>
        <source>Adjust beatgrid so the closest beat is aligned with the current play position.</source>
        <translation>Pas het beat-raster aan zodat de dichtstbijzijnde beat uitgelijnd is met de huidige afspeelpositie.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="370"/>
        <location filename="../../src/skin/tooltips.cpp" line="374"/>
        <source>Adjust beatgrid to match another playing deck.</source>
        <translation>Pas Beat-raster overeenkomstig aan andere speler</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="399"/>
        <source>If quantize is enabled, snaps to the nearest beat.</source>
        <translation>Als quantize is ingeschakeld, springt het naar de dichtstbijzijnde tel.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="401"/>
        <source>Quantize</source>
        <translation>Quantize</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="402"/>
        <source>Toggles quantization.</source>
        <translation>Schakelt quantisatie in/uit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="403"/>
        <source>Loops and cues snap to the nearest beat when quantization is enabled.</source>
        <translation>Loops en cues springen naar de dichtstbijzijnde beat wanneer kwantisering is ingeschakeld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="407"/>
        <source>Reverse</source>
        <translation>Omkeren</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="408"/>
        <source>Reverses track playback during regular playback.</source>
        <translation>Keert het afspelen van de track om tijdens normaal afspelen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="409"/>
        <source>Puts a track into reverse while being held (Censor).</source>
        <translation>Keert een track om wanneer deze wordt vastgehouden (Censor).</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="410"/>
        <source>Playback continues where the track would have been if it had not been temporarily reversed.</source>
        <translation>Het afspelen gaat verder waar het nummer zou zijn geweest als het niet tijdelijk was omgekeerd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="414"/>
        <location filename="../../src/skin/tooltips.cpp" line="421"/>
        <source>Play/Pause</source>
        <translation>Speel/Pauze</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="458"/>
        <source>Jumps to the beginning of the track.</source>
        <translation>Spring naar het begin van de track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="469"/>
        <source>Syncs the tempo (BPM) and phase to that of the other track, if BPM is detected on both.</source>
        <translation>Synchroniseert het tempo (BPM) en fase met dat van de andere track, als BPM op beide wordt gedetecteerd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="471"/>
        <source>Syncs the tempo (BPM) to that of the other track, if BPM is detected on both.</source>
        <translation>Synchroniseert het tempo (BPM) met dat van de andere track, als BPM op beide wordt gedetecteerd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="487"/>
        <source>Sync and Reset Key</source>
        <translation>Synchroniseer en herstel Key</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="509"/>
        <source>Increases the pitch by one semitone.</source>
        <translation>Verhoogt de toonhoogte met een halve toon.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="515"/>
        <source>Decreases the pitch by one semitone.</source>
        <translation>Verlaagt de toonhoogte met een halve toon.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="567"/>
        <source>Enable Vinyl Control</source>
        <translation>Schakel Vinylbediening in</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="568"/>
        <source>When disabled, the track is controlled by Mixxx playback controls.</source>
        <translation>Indien uitgeschakeld, wordt de track bestuurd door Mixxx-afspeelknoppen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="569"/>
        <source>When enabled, the track responds to external vinyl control.</source>
        <translation>Indien ingeschakeld, reageert de track op externe vinylbediening.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="563"/>
        <source>Enable Passthrough</source>
        <translation>Passthrough inschakelen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="723"/>
        <source>Indicates that the audio buffer is too small to do all audio processing.</source>
        <translation>Geeft aan dat de audiobuffer te klein is om alle audiobewerking uit te voeren.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="727"/>
        <source>Displays cover artwork of the loaded track.</source>
        <translation>Toont hoes van de geladen track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="728"/>
        <source>Displays options for editing cover artwork.</source>
        <translation>Toont de bewerkopties  voor de hoes.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="731"/>
        <source>Star Rating</source>
        <translation>Sterwaardering</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="732"/>
        <source>Assign ratings to individual tracks by clicking the stars.</source>
        <translation>Ken waarderingen toe aan individuele tracks door op de sterren te klikken.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="77"/>
        <source>Channel Peak Indicator</source>
        <translation>Kanaal piekindicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="82"/>
        <source>Channel L Peak Indicator</source>
        <translation>Kanaal L piekindicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="83"/>
        <source>Indicates when the left signal on the channel is clipping,</source>
        <translation>Geeft aan wanneer het signaal op de hoofduitgang wordt vervormd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="87"/>
        <source>Channel R Peak Indicator</source>
        <translation>Kanaal R piekindicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="88"/>
        <source>Indicates when the right signal on the channel is clipping,</source>
        <translation>Geeft aan wanneer het rechtersignaal op het kanaal wordt vervormd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="97"/>
        <source>Master L Peak Indicator</source>
        <translation>Master L piekindicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="98"/>
        <source>Indicates when the left signal on the master output is clipping,</source>
        <translation>Geeft aan wanneer het linkersignaal op de hoofduitgang wordt  vervormd,</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="102"/>
        <source>Master R Peak Indicator</source>
        <translation>Master R piekindicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="103"/>
        <source>Indicates when the right signal on the master output is clipping,</source>
        <translation>Geeft aan wanneer het rechtersignaal op de hoofduitgang wordt vervormd,</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="111"/>
        <source>Channel L Volume Meter</source>
        <translation>Kanaal L volumemeter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="112"/>
        <source>Shows the current channel volume for the left channel.</source>
        <translation>Toont het huidige kanaalvolume voor het linkerkanaal.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="115"/>
        <source>Channel R Volume Meter</source>
        <translation>Kanaal R volumemeter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="116"/>
        <source>Shows the current channel volume for the right channel.</source>
        <translation>Toont het huidige kanaalvolume voor het rechterkanaal.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="123"/>
        <source>Microphone Peak Indicator</source>
        <translation>Microfoon piekindicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="124"/>
        <source>Indicates when the signal on the microphone is clipping,</source>
        <translation>Geeft aan wanneer het signaal op de microfoon wordt vervormd,</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="128"/>
        <source>Sampler Volume Meter</source>
        <translation>Sampler Volumemeter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="129"/>
        <source>Shows the current sampler volume.</source>
        <translation>Toont het huidige sampler-volume</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="132"/>
        <source>Sampler Peak Indicator</source>
        <translation>Sampler piekindicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="133"/>
        <source>Indicates when the signal on the sampler is clipping,</source>
        <translation>Geeft aan wanneer het signaal op de sampler wordt vervormd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="137"/>
        <source>Preview Deck Volume Meter</source>
        <translation>Volumemeter voorbeluistering</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="138"/>
        <source>Shows the current Preview Deck volume.</source>
        <translation>Toont het huidige voorbeluisteringsvolume</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="141"/>
        <source>Preview Deck Peak Indicator</source>
        <translation>Voorbeluistering piekindicator</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="142"/>
        <source>Indicates when the signal on the Preview Deck is clipping,</source>
        <translation>Geeft aan wanneer het signaal op de voorbeluistering wordt vervormd,</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="146"/>
        <source>Master Channel L Volume Meter</source>
        <translation>Hoofdkanaal L volumemeter</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="246"/>
        <source>Maximize Library</source>
        <translation>Maximaliseer bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="247"/>
        <source>Maximize the track library to take up all the available screen space.</source>
        <translation>Maximaliseer de trackbibliotheek in de beschikbare schermruimte.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="275"/>
        <source>Microphone Talkover Ducking Strength</source>
        <translation>Dempsterkte microfoon Talkover</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="388"/>
        <source>Prevents the pitch from changing when the rate changes.</source>
        <translation>Voorkomt dat de toonhoogte verandert wanneer de snelheid verandert.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="392"/>
        <source>Changes the number of hotcue buttons displayed in the deck</source>
        <translation>Wijzigt het aantal hotcue-knoppen dat in de speler wordt weergegeven</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="415"/>
        <source>Starts playing from the beginning of the track.</source>
        <translation>Begint te spelen vanaf het begin van de track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="416"/>
        <source>Jumps to the beginning of the track and stops.</source>
        <translation>Sprint naar het begin van de track en stopt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="422"/>
        <source>Plays or pauses the track.</source>
        <translation>Speelt of pauzeert de track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="425"/>
        <source>(while playing)</source>
        <translation>(tijdens het spelen)</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="426"/>
        <source>(while stopped)</source>
        <translation>(terwijl gestopt)</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="432"/>
        <source>Cue</source>
        <translation>Verwijzing</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="447"/>
        <source>Headphone</source>
        <translation>Hoofdtelefoon</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="452"/>
        <source>Mute</source>
        <translation>Dempen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="453"/>
        <source>Mutes the selected channel&apos;s audio in the master output.</source>
        <translation>Dempt de audio van het geselecteerde kanaal in de masteruitgang.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="467"/>
        <source>Old Synchronize</source>
        <translation>Oude synchronisatie</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="468"/>
        <source>(This skin should be updated to use Master Sync!)</source>
        <translation>(Deze skin moet worden bijgewerkt om Master Sync te gebruiken!)</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="473"/>
        <source>Syncs to the first deck (in numerical order) that is playing a track and has a BPM.</source>
        <translation>Synchroniseert met het eerste speler (in numerieke volgorde) dat een track afspeelt en een BPM heeft.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="474"/>
        <source>If no deck is playing, syncs to the first deck that has a BPM.</source>
        <translation>Als er geen speler speelt, wordt het gesynchroniseerd met het eerste deck met een BPM.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="475"/>
        <source>Decks can&apos;t sync to samplers and samplers can only sync to decks.</source>
        <translation>Spelers kunnen niet worden gesynchroniseerd met samplers en samplers kunnen alleen worden gesynchroniseerd met spelers.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="479"/>
        <source>Enable Master Sync</source>
        <translation>Schakel Master Sync in</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="480"/>
        <source>Tap to sync the tempo to other playing tracks or the master clock.</source>
        <translation>Tik om het tempo te synchroniseren met andere tracks die worden afgespeeld of met de hoofdklok.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="481"/>
        <source>Hold for at least a second to enable sync lock for this deck.</source>
        <translation>Houd minimaal één seconde ingedrukt om het synchronisatieslot voor deze speler in te schakelen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="482"/>
        <source>Decks with sync locked will all play at the same tempo, and decks that also have quantize enabled will always have their beats lined up.</source>
        <translation>Spelers met synchronisatievergrendeling, spelen allemaal in hetzelfde tempo, en spelers waarvoor ook quantisering is ingeschakeld, hebben altijd hun uitgelijnde beats.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="490"/>
        <source>Resets the key to the original track key.</source>
        <translation>Herstelt de toonaard naar de originele track-toonaard.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="493"/>
        <source>Enable Sync Clock Master</source>
        <translation>Schakel Sync Clock Master in</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="494"/>
        <source>When enabled, this device will serve as the master clock for all other decks.</source>
        <translation>Indien ingeschakeld, zal dit apparaat dienen als de masterklok voor alle andere spelers.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="497"/>
        <source>Speed Control</source>
        <translation>Snelheidsbediening</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="503"/>
        <location filename="../../src/skin/tooltips.cpp" line="508"/>
        <location filename="../../src/skin/tooltips.cpp" line="514"/>
        <source>Changes the track pitch independent of the tempo.</source>
        <translation>Verandert de track toonhoogte onafhankelijk van het tempo.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="510"/>
        <source>Increases the pitch by 10 cents.</source>
        <translation>Verhoogt de toonhoogte met 10 cent.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="516"/>
        <source>Decreases the pitch by 10 cents.</source>
        <translation>Verlaagt de toonhoogte met 10 cent.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="519"/>
        <source>Pitch Adjust</source>
        <translation>Aanpassen Pitch</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="520"/>
        <source>Adjust the pitch in addition to the speed slider pitch.</source>
        <translation>Pas de toonhoogte als aanvulling bij de snelheid van de schuifregelaar.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="545"/>
        <source>Record Mix</source>
        <translation>Record Mix</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="546"/>
        <source>Toggle mix recording.</source>
        <translation>Mix opname omschakelen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="556"/>
        <source>Enable Live Broadcasting</source>
        <translation>Live-uitzenden inschakelen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="557"/>
        <source>Stream your mix over the Internet.</source>
        <translation>Stream je mix over het Internet.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="558"/>
        <source>Provides visual feedback for Live Broadcasting status:</source>
        <translation>Voorziet visuele feedback voor de status van live-uitzendingen:</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="559"/>
        <source>disabled, connecting, connected, failure.</source>
        <translation>uitgeschakeld, verbinden, verbonden, fout.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="564"/>
        <source>When enabled, the deck directly plays the audio arriving on the vinyl input.</source>
        <translation>Indien ingeschakeld, speelt de speler direct de audio af die op de vinyl-invoer binnenkomt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="576"/>
        <source>Blue for passthrough enabled.</source>
        <translation>Blauw voor doorvoer ingeschakeld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="635"/>
        <source>Playback will resume where the track would have been if it had not entered the loop.</source>
        <translation>Het afspelen wordt hervat waar de track zou zijn geweest als deze niet in de loop was terechtgekomen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="656"/>
        <source>Loop Exit</source>
        <translation>Verlaat Loop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="657"/>
        <source>Turns the current loop off.</source>
        <translation>Schakelt de huidige loop uit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="668"/>
        <source>Slip Mode</source>
        <translation>Slip Modus</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="669"/>
        <source>When active, the playback continues muted in the background during a loop, reverse, scratch etc.</source>
        <translation>Indien actief, gaat het afspelen op de achtergrond gedempt door tijdens een loop, achteruitspelen, scratch etc.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="670"/>
        <source>Once disabled, the audible playback will resume where the track would have been.</source>
        <translation>Eenmaal uitgeschakeld, wordt het hoorbare afspelen hervat waar de track zou zijn geweest.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="701"/>
        <source>Track Key</source>
        <translation>Track toonaard</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="702"/>
        <source>Displays the musical key of the loaded track.</source>
        <translation>Geeft de muziekale toonaard van het geladen nummer weer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="712"/>
        <source>Clock</source>
        <translation>Klok</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="713"/>
        <source>Displays the current time.</source>
        <translation>Geeft de huidige tijd weer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="716"/>
        <source>Audio Latency Usage Meter</source>
        <translation>Gebruiksmeter voor Audio Latency</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="717"/>
        <source>Displays the fraction of latency used for audio processing.</source>
        <translation>Geeft het deel van de latentie weer dat wordt gebruikt voor audioverwerking.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="718"/>
        <source>A high value indicates that audible glitches are likely.</source>
        <translation>Een hoge waarde geeft aan dat hoorbare storingen waarschijnlijk zijn.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="719"/>
        <source>Do not enable keylock, effects or additional decks in this situation.</source>
        <translation>Schakel in deze situatie geen keylock, effecten of extra spelers in.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="722"/>
        <source>Audio Latency Overload Indicator</source>
        <translation>indicator voor overbelasting van de Audio Latency</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="68"/>
        <source>If Vinyl control is enabled, displays time-coded vinyl signal quality (see Preferences -&gt; Vinyl Control).</source>
        <translation>Als Vinylbediening is ingeschakeld, wordt de tijd-gecodeerde vinylsignaalkwaliteit weergegeven (zie Voorkeuren -&gt; Vinylcontrole).</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="28"/>
        <source>Drop tracks from library, external file manager, or other decks/samplers here.</source>
        <translation>Zet tracks van de bibliotheek, extern bestandsbeheer of andere spelers/samplers hier neer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="40"/>
        <source>Jump around in the track by clicking anywhere on the waveform.</source>
        <translation>Spring door de track door ergens op de golfvorm te klikken.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="151"/>
        <source>Shows the current master volume for the right channel.</source>
        <translation>Toont het huidige mastervolume voor het rechterkanaal.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="178"/>
        <source>Change the crossfader curve in Preferences -&gt; Crossfader</source>
        <translation>Wijzig de crossfader-curve in Voorkeuren -&gt; Crossfader</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="198"/>
        <source>Crossfades the headphone output between the master mix and cueing (PFL or Pre-Fader Listening) signal.</source>
        <translation>Crossfade de hoofdtelefoonuitgang tussen de mastermix en cueing (PFL of Pre-Fader Listening) signaal.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="208"/>
        <source>Crossfader Orientation</source>
        <translation>Crossfader Orientatie</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="209"/>
        <source>Set the channel&apos;s crossfader orientation.</source>
        <translation>Stel de oriëntatie van de crossfader van het kanaal in.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="210"/>
        <source>Either to the left side of crossfader, to the right side or to the center (unaffected by crossfader)</source>
        <translation>Ofwel aan de linkerkant van de crossfader, aan de rechterkant of naar het midden (niet beïnvloed door crossfader)</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="223"/>
        <source>Activate Vinyl Control from the Menu -&gt; Options.</source>
        <translation>Activeer Vinylbediening vanuit het Menu -&gt; Opties.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="281"/>
        <source>Change the step-size in the Preferences -&gt; Interface menu.</source>
        <translation>Wijzig de stapgrootte in het menu Voorkeuren -&gt; Interface.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="345"/>
        <source>Displays the current musical key of the loaded track after pitch shifting.</source>
        <translation>Toont de huidige muzikale toonaard van de geladen track na het veranderen van de toonhoogte.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="448"/>
        <source>Sends the selected channel&apos;s audio to the headphone output,</source>
        <translation>Stuurt de audio van het geselecteerde kanaal naar de hoofdtelefoonuitgang,</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="449"/>
        <source>selected in Preferences -&gt; Sound Hardware.</source>
        <translation>geselecteerd in Voorkeuren -&gt; Geluidshardware.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="456"/>
        <source>Fast Rewind</source>
        <translation>Snel Terugspoelen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="457"/>
        <source>Fast rewind through the track.</source>
        <translation>Snel terugspoelen in de track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="461"/>
        <source>Fast Forward</source>
        <translation>Snel Voorwaarts</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="462"/>
        <source>Fast forward through the track.</source>
        <translation>Snel doorspoelen in de track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="463"/>
        <source>Jumps to the end of the track.</source>
        <translation>Sprint naar het einde van de track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="488"/>
        <source>Sets the pitch to a key that allows a harmonic transition from the other track. Requires a detected key on both involved decks.</source>
        <translation>Stelt de toonhoogte in op een toonaard die een harmonische overgang van de andere track mogelijk maakt. Vereist een gedetecteerde toonaard op beide betrokken dekken.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="502"/>
        <location filename="../../src/skin/tooltips.cpp" line="507"/>
        <location filename="../../src/skin/tooltips.cpp" line="513"/>
        <source>Pitch Control</source>
        <translation>Toonhoogte regeling</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="525"/>
        <source>Pitch Rate</source>
        <translation>Toonhoogtesnelheid</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="526"/>
        <source>Displays the current playback rate of the track.</source>
        <translation>Geeft de huidige afspeelsnelheid van de track weer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="529"/>
        <source>Repeat</source>
        <translation>Herhaal</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="530"/>
        <source>When active the track will repeat if you go past the end or reverse before the start.</source>
        <translation>Indien actief, wordt de track herhaald als u voorbij het einde gaat of wordt achteruit gespeeld vóór de start achteruit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="533"/>
        <source>Eject</source>
        <translation>Uitwerpen</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="534"/>
        <source>Ejects track from the player.</source>
        <translation>Werpt de track uit de speler.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="537"/>
        <source>Hotcue</source>
        <translation>Hotcue</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="538"/>
        <source>If hotcue is set, jumps to the hotcue.</source>
        <translation>Springt naar de Hotcue als er een Hotcue is ingesteld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="539"/>
        <source>If hotcue is not set, sets the hotcue to the current play position.</source>
        <translation>Stelt de hotcue in op de huidige afspeelpositie als er nog geen hotcue is ingesteld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="541"/>
        <source>If hotcue is set, clears the hotcue.</source>
        <translation>Verwijdert de hotcue als er een hotcue is ingesteld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="579"/>
        <source>Vinyl Control Mode</source>
        <translation>Vinyl Control Modus</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="580"/>
        <source>Absolute mode - track position equals needle position and speed.</source>
        <translation>Absolute modus - trackpositie is gelijk aan naaldpositie en snelheid.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="581"/>
        <source>Relative mode - track speed equals needle speed regardless of needle position.</source>
        <translation>Relatieve modus - tracksnelheid is gelijk aan naaldsnelheid, ongeacht de naaldpositie.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="582"/>
        <source>Constant mode - track speed equals last known-steady speed regardless of needle input.</source>
        <translation>Constante modus - tracksnelheid is gelijk aan laatst bekende constante snelheid, ongeacht de naaldinvoer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="572"/>
        <source>Vinyl Status</source>
        <translation>Vinyl Status</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="573"/>
        <source>Provides visual feedback for vinyl control status:</source>
        <translation>Biedt visuele feedback over de vinylstatus:</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="574"/>
        <source>Green for control enabled.</source>
        <translation>Groen als de besturing is ingeschakeld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="575"/>
        <source>Blinking yellow for when the needle reaches the end of the record.</source>
        <translation>Geel knipperend wanneer de naald het einde van de plaat bereikt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="592"/>
        <source>Loop-In Marker</source>
        <translation>Loop-In Markering</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="601"/>
        <source>Loop-Out Marker</source>
        <translation>Loop-Out Markering</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="610"/>
        <source>Loop Halve</source>
        <translation>Halve Loop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="611"/>
        <source>Halves the current loop&apos;s length by moving the end marker.</source>
        <translation>Halveert de lengte van de huidige loop door de eindmarkering te verplaatsen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="612"/>
        <source>Deck immediately loops if past the new endpoint.</source>
        <translation>De speler zal onmiddellijk in een loop gaan als het voorbij het nieuwe eindpunt is.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="615"/>
        <source>Loop Double</source>
        <translation>Dubbele Loop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="616"/>
        <source>Doubles the current loop&apos;s length by moving the end marker.</source>
        <translation>Verdubbelt de lengte van de huidige loop door de eindmarkering te verplaatsen.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="631"/>
        <source>Beatloop</source>
        <translation>Beatloop</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="662"/>
        <source>Toggles the current loop on or off.</source>
        <translation>Schakelt de huidige loop in of uit.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="658"/>
        <source>Works only if Loop-In and Loop-Out marker are set.</source>
        <translation>Werkt alleen als de Loop-In- en Loop-Out-markeringen zijn ingesteld.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="430"/>
        <source>Hint: Change the default cue mode in Preferences -&gt; Interface.</source>
        <translation>Tip: Wijzig de standaard cue-modus in Voorkeuren -&gt; Interface.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="585"/>
        <source>Vinyl Cueing Mode</source>
        <translation>Vinyl Cueing Modus</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="586"/>
        <source>Determines how cue points are treated in vinyl control Relative mode:</source>
        <translation>Bepaalt hoe cue-punten worden behandeld in Relatieve modus met vinylbediening:</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="587"/>
        <source>Off - Cue points ignored.</source>
        <translation>Off - Cue punten genegeerd.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="588"/>
        <source>One Cue - If needle is dropped after the cue point, track will seek to that cue point.</source>
        <translation>One Cue - Als de naald na het cue-punt valt, zoekt de track naar dat cue-punt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="589"/>
        <source>Hot Cue - Track will seek to nearest previous hot cue point.</source>
        <translation>Hot Cue - Track zoekt naar het dichtstbijzijnde vorige hot cue punt.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="673"/>
        <source>Track Time</source>
        <translation>Track-tijd</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="678"/>
        <source>Track Duration</source>
        <translation>Track-tijd</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="679"/>
        <source>Displays the duration of the loaded track.</source>
        <translation>Geeft de duur van de geladen track weer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="681"/>
        <source>Information is loaded from the track&apos;s metadata tags.</source>
        <translation>Informatie wordt geladen uit de metadata-tags van de track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="683"/>
        <source>Track Artist</source>
        <translation>Track Artiest</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="684"/>
        <source>Displays the artist of the loaded track.</source>
        <translation>Geeft de artiest van de geladen track weer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="689"/>
        <source>Track Title</source>
        <translation>Track Titel</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="690"/>
        <source>Displays the title of the loaded track.</source>
        <translation>Geeft de titel van de geladen track weer.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="695"/>
        <source>Track Album</source>
        <translation>Track Album</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="696"/>
        <source>Displays the album name of the loaded track.</source>
        <translation>Toont de albumnaam van de geladen track.</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="706"/>
        <source>Track Artist/Title</source>
        <translation>Track Artiest/Titel</translation>
    </message>
    <message>
        <location filename="../../src/skin/tooltips.cpp" line="707"/>
        <source>Displays the artist and title of the loaded track.</source>
        <translation>Geeft de artiest en titel van de geladen track weer.</translation>
    </message>
</context>
<context>
    <name>TrackCollection</name>
    <message>
        <location filename="../../src/library/trackcollection.cpp" line="159"/>
        <source>Hiding tracks</source>
        <translation>Tracks verbergen</translation>
    </message>
    <message>
        <location filename="../../src/library/trackcollection.cpp" line="160"/>
        <source>The selected tracks are in the following playlists:%1Hiding them will remove them from these playlists. Continue?</source>
        <translation>De geselecteerde tracks staan in de volgende afspeellijsten:% 1 Door ze te verbergen, worden ze uit deze afspeellijsten verwijderd. Verdergaan?</translation>
    </message>
</context>
<context>
    <name>TrackExportDlg</name>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="47"/>
        <source>Export finished</source>
        <translation>Export voltooid</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="50"/>
        <source>Exporting %1</source>
        <translation>%1 exporteren</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="62"/>
        <source>Overwrite Existing File?</source>
        <translation>Bestaande file overschrijven?</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="63"/>
        <source>&quot;%1&quot; already exists, overwrite?</source>
        <translation>&quot;%1&quot; bestaat reeds, overschrijven?</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="67"/>
        <source>&amp;Overwrite</source>
        <translation>&amp;Overschrijven</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="68"/>
        <source>Over&amp;write All</source>
        <translation>Over&amp;Schrijf Alles</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="69"/>
        <source>&amp;Skip</source>
        <translation>&amp;Overslaan</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="70"/>
        <source>Skip &amp;All</source>
        <translation>Sla &amp;Alles Over</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportdlg.cpp" line="101"/>
        <source>Export Error</source>
        <translation>Fout bij exporteren</translation>
    </message>
</context>
<context>
    <name>TrackExportWizard</name>
    <message>
        <location filename="../../src/library/export/trackexportwizard.cpp" line="24"/>
        <source>Export Track Files To</source>
        <translation>Exporteer trackbestanden Naar</translation>
    </message>
</context>
<context>
    <name>TrackExportWorker</name>
    <message>
        <location filename="../../src/library/export/trackexportworker.cpp" line="111"/>
        <location filename="../../src/library/export/trackexportworker.cpp" line="185"/>
        <source>Export process was canceled</source>
        <translation>Het exportproces werd geannuleerd</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportworker.cpp" line="126"/>
        <source>Error removing file %1: %2. Stopping.</source>
        <translation>Fout bij het verwijderen van bestand %1:%2. Stoppen.</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportworker.cpp" line="139"/>
        <source>Error exporting track %1 to %2: %3. Stopping.</source>
        <translation>Fout bij het exporteren van bestand %1 naar %2: %3. Stoppen.</translation>
    </message>
    <message>
        <location filename="../../src/library/export/trackexportworker.cpp" line="170"/>
        <source>Error exporting tracks</source>
        <translation>Fout bij exporteren van tracks</translation>
    </message>
</context>
<context>
    <name>TraktorFeature</name>
    <message>
        <location filename="../../src/library/traktor/traktorfeature.cpp" line="91"/>
        <location filename="../../src/library/traktor/traktorfeature.cpp" line="620"/>
        <source>Traktor</source>
        <translation>Traktor</translation>
    </message>
    <message>
        <location filename="../../src/library/traktor/traktorfeature.cpp" line="156"/>
        <source>(loading) Traktor</source>
        <translation>(laden) Tractor</translation>
    </message>
    <message>
        <location filename="../../src/library/traktor/traktorfeature.cpp" line="614"/>
        <source>Error Loading Traktor Library</source>
        <translation>Fout bij het laden van de Traktor-bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/library/traktor/traktorfeature.cpp" line="615"/>
        <source>There was an error loading your Traktor library. Some of your Traktor tracks or playlists may not have loaded.</source>
        <translation>Er is een fout opgetreden bij het laden van uw Traktor-bibliotheek. Sommige van uw Traktor-tracks of afspeellijsten zijn mogelijk niet geladen.</translation>
    </message>
</context>
<context>
    <name>VSyncThread</name>
    <message>
        <location filename="../../src/waveform/vsyncthread.cpp" line="156"/>
        <source>Timer (Fallback)</source>
        <translation>Timer (Fallback)</translation>
    </message>
    <message>
        <location filename="../../src/waveform/vsyncthread.cpp" line="159"/>
        <source>MESA vblank_mode = 1</source>
        <translation>MESA vblank_mode = 1</translation>
    </message>
    <message>
        <location filename="../../src/waveform/vsyncthread.cpp" line="162"/>
        <source>Wait for Video sync</source>
        <translation>Wacht op Video synchronisatie</translation>
    </message>
    <message>
        <location filename="../../src/waveform/vsyncthread.cpp" line="165"/>
        <source>Sync Control</source>
        <translation>Synchronisatiebediening</translation>
    </message>
    <message>
        <location filename="../../src/waveform/vsyncthread.cpp" line="168"/>
        <source>Free + 1 ms (for benchmark only)</source>
        <translation>Vrij + 1 ms (enkel voor benchmark)</translation>
    </message>
</context>
<context>
    <name>WBattery</name>
    <message>
        <location filename="../../src/widget/wbattery.cpp" line="109"/>
        <source>Time until charged unknown.</source>
        <translation>Tijd tot opgeladen onbekend.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wbattery.cpp" line="111"/>
        <source>Time until charged: %1</source>
        <translation>Tijd tot opladen: %1</translation>
    </message>
    <message>
        <location filename="../../src/widget/wbattery.cpp" line="122"/>
        <source>Time left unknown.</source>
        <translation>Resterende tijd onbekend.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wbattery.cpp" line="124"/>
        <source>Time left: %1</source>
        <translation>Resterende tijd: %1</translation>
    </message>
    <message>
        <location filename="../../src/widget/wbattery.cpp" line="130"/>
        <source>Battery fully charged.</source>
        <translation>Batterij volledig opgeladen.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wbattery.cpp" line="135"/>
        <source>Battery status unknown.</source>
        <translation>Batterij-status onbekend.</translation>
    </message>
</context>
<context>
    <name>WCoverArtMenu</name>
    <message>
        <location filename="../../src/widget/wcoverartmenu.cpp" line="20"/>
        <source>Choose new cover</source>
        <comment>change cover art location</comment>
        <translation>Kies nieuwe hoes</translation>
    </message>
    <message>
        <location filename="../../src/widget/wcoverartmenu.cpp" line="25"/>
        <source>Clear cover</source>
        <comment>clears the set cover art -- does not touch files on disk</comment>
        <translation>Verwijder hoes</translation>
    </message>
    <message>
        <location filename="../../src/widget/wcoverartmenu.cpp" line="30"/>
        <source>Reload from file/folder</source>
        <comment>reload cover art from file metadata or folder</comment>
        <translation>Herlaad van bestand/map</translation>
    </message>
    <message>
        <location filename="../../src/widget/wcoverartmenu.cpp" line="60"/>
        <source>Image Files</source>
        <translation>Afbeeldingen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wcoverartmenu.cpp" line="65"/>
        <source>Change Cover Art</source>
        <translation>Wijzig hoesafbeelding</translation>
    </message>
</context>
<context>
    <name>WEffect</name>
    <message>
        <location filename="../../src/widget/weffect.cpp" line="49"/>
        <source>%1: %2</source>
        <extracomment>%1 = effect name; %2 = effect description</extracomment>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../../src/widget/weffect.cpp" line="52"/>
        <source>None</source>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/widget/weffect.cpp" line="53"/>
        <source>No effect loaded.</source>
        <translation>Geen effect geladen.</translation>
    </message>
</context>
<context>
    <name>WEffectChain</name>
    <message>
        <location filename="../../src/widget/weffectchain.cpp" line="38"/>
        <source>None</source>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/widget/weffectchain.cpp" line="39"/>
        <source>No effect chain loaded.</source>
        <translation>Geen effectketen geladen.</translation>
    </message>
</context>
<context>
    <name>WEffectParameterBase</name>
    <message>
        <location filename="../../src/widget/weffectparameterbase.cpp" line="33"/>
        <source>None</source>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/widget/weffectparameterbase.cpp" line="34"/>
        <source>No effect loaded.</source>
        <translation>Geen effect geladen.</translation>
    </message>
</context>
<context>
    <name>WEffectSelector</name>
    <message>
        <location filename="../../src/widget/weffectselector.cpp" line="64"/>
        <source>%1: %2</source>
        <extracomment>%1 = effect name; %2 = effect description</extracomment>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../../src/widget/weffectselector.cpp" line="72"/>
        <source>None</source>
        <extracomment>Displayed when no effect is loaded</extracomment>
        <translation>geen</translation>
    </message>
    <message>
        <location filename="../../src/widget/weffectselector.cpp" line="73"/>
        <source>No effect loaded.</source>
        <translation>Geen effect geladen.</translation>
    </message>
</context>
<context>
    <name>WMainMenuBar</name>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="77"/>
        <source>&amp;File</source>
        <translation>&amp;Bestand</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="79"/>
        <source>Load Track to Deck &amp;%1</source>
        <translation>Laad Track in Speler %1</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="80"/>
        <source>Loads a track in deck %1</source>
        <translation>Laad een track in speler %1</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="81"/>
        <source>Open</source>
        <translation>Openen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="109"/>
        <source>&amp;Exit</source>
        <translation>&amp;Afsluiten</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="110"/>
        <source>Quits Mixxx</source>
        <translation>Sluit Mixxx af</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="114"/>
        <source>Ctrl+q</source>
        <translation>Ctrl+q</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="125"/>
        <source>&amp;Library</source>
        <translation>&amp;Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="127"/>
        <source>&amp;Rescan Library</source>
        <translation>Bibliotheek &amp;Herscannen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="128"/>
        <source>Rescans library folders for changes to tracks.</source>
        <translation>Bibliotheekmappen worden opnieuw gescand op wijzigingen in tracks.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="142"/>
        <source>Create &amp;New Playlist</source>
        <translation>Maan &amp;Nieuwe speellijst</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="143"/>
        <source>Create a new playlist</source>
        <translation>Maak een nieuwe afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="148"/>
        <source>Ctrl+n</source>
        <translation>Ctrl+n</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="156"/>
        <source>Create New &amp;Crate</source>
        <translation>Maak nieuwe &amp;Krat</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="157"/>
        <source>Create a new crate</source>
        <translation>Maak een nieuwe krat</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="162"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="173"/>
        <source>&amp;View</source>
        <translation>&amp;Bekijk</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="176"/>
        <source>May not be supported on all skins.</source>
        <translation>Wordt mogelijk niet op alle thema&apos;s ondersteund.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="177"/>
        <source>Show Skin Settings Menu</source>
        <translation>Menu thema-instellingen weergeven</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="178"/>
        <source>Show the Skin Settings Menu of the currently selected Skin</source>
        <translation>Toon het menu thema-instellingen van de huidig geselecteerde thema</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="185"/>
        <source>Ctrl+1</source>
        <comment>Menubar|View|Show Skin Settings</comment>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="192"/>
        <source>Show Microphone Section</source>
        <translation>Toon Microfoonsectie</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="193"/>
        <source>Show the microphone section of the Mixxx interface.</source>
        <translation>Toon het microfoonsectie van de Mixxx-interface.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="200"/>
        <source>Ctrl+2</source>
        <comment>Menubar|View|Show Microphone Section</comment>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="207"/>
        <source>Show Vinyl Control Section</source>
        <translation>Toon sectie Vinylbediening</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="208"/>
        <source>Show the vinyl control section of the Mixxx interface.</source>
        <translation>Toon de sectie vinylbediening van de Mixxx-interface.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="215"/>
        <source>Ctrl+3</source>
        <comment>Menubar|View|Show Vinyl Control Section</comment>
        <translation>Ctrl+3</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="222"/>
        <source>Show Preview Deck</source>
        <translation>Toon voorbeluisterspeler</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="223"/>
        <source>Show the preview deck in the Mixxx interface.</source>
        <translation>Toon de voorbeluisterspeler in de mixxx-interface.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="230"/>
        <source>Ctrl+4</source>
        <comment>Menubar|View|Show Preview Deck</comment>
        <translation>Ctrl+4</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="237"/>
        <source>Show Cover Art</source>
        <translation>Toon hoesafbeeldingen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="238"/>
        <source>Show cover art in the Mixxx interface.</source>
        <translation>Toon hoesafbeeldingen in de Mixxx-interface.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="245"/>
        <source>Ctrl+6</source>
        <comment>Menubar|View|Show Cover Art</comment>
        <translation>Ctrl+6</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="252"/>
        <source>Maximize Library</source>
        <translation>Maximaliseer bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="253"/>
        <source>Maximize the track library to take up all the available screen space.</source>
        <translation>Maximaliseer de trackbibliotheek in de beschikbare schermruimte.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="260"/>
        <source>Space</source>
        <comment>Menubar|View|Maximize Library</comment>
        <translation>Ruimte</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="270"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Volledig scherm</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="271"/>
        <source>Display Mixxx using the full screen</source>
        <translation>Geef Mixxx weer in volledig scherm</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="291"/>
        <source>&amp;Options</source>
        <translation>&amp;Opties</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="294"/>
        <source>&amp;Vinyl Control</source>
        <translation>&amp;Vinylbediening</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="295"/>
        <source>Use timecoded vinyls on external turntables to control Mixxx</source>
        <translation>Gebruik tijdgecodeerde vinyl op externe draaitafels om Mixxx te bedienen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="299"/>
        <source>Enable Vinyl Control &amp;%1</source>
        <translation>Activeer vinylbediening &amp;%1</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="332"/>
        <source>&amp;Record Mix</source>
        <translation>Mix &amp;Opnemen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="333"/>
        <source>Record your mix to a file</source>
        <translation>Neem je mix op naar een bestand</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="338"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="350"/>
        <source>Enable Live &amp;Broadcasting</source>
        <translation>Schakel Live &amp;Uitzenden in</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="351"/>
        <source>Stream your mixes to a shoutcast or icecast server</source>
        <translation>Stream je mixen naar een Shoutcast- of Icecast-server</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="357"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="372"/>
        <source>Enable &amp;Keyboard Shortcuts</source>
        <translation>Schakel &amp;Sneltoetsen in</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="373"/>
        <source>Toggles keyboard shortcuts on or off</source>
        <translation>Schakel sneltoetsen in of uit</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="380"/>
        <source>Ctrl+`</source>
        <translation>Ctrl+`</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="393"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="394"/>
        <source>Change Mixxx settings (e.g. playback, MIDI, controls)</source>
        <translation>Mixxx-instellingen wijzigen (bijv. Afspelen, MIDI, bedieningselementen)</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="412"/>
        <source>&amp;Developer</source>
        <translation>&amp;Ontwikkelaar</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="414"/>
        <source>&amp;Reload Skin</source>
        <translation>&amp;Herlaad skin</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="415"/>
        <source>Reload the skin</source>
        <translation>Herlaad de skin</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="420"/>
        <source>Ctrl+Shift+R</source>
        <translation>Ctrl+Shift+R</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="428"/>
        <source>Developer &amp;Tools</source>
        <translation>Developer &amp;Tools</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="429"/>
        <source>Opens the developer tools dialog</source>
        <translation>Opent het dialoogvenster hulpprogramma&apos;s voor ontwikkelaars</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="434"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="446"/>
        <source>Stats: &amp;Experiment Bucket</source>
        <translation>Statistieken: &amp;Experiment Bucket</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="447"/>
        <source>Enables experiment mode. Collects stats in the EXPERIMENT tracking bucket.</source>
        <translation>Schakel experiment-modus in. Verzamelt statistieken in de EXPERIMENT-tracking bucket.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="453"/>
        <source>Ctrl+Shift+E</source>
        <translation>Ctrl+Shift+E</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="464"/>
        <source>Stats: &amp;Base Bucket</source>
        <translation>Statistieken: &amp;Base Bucket</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="465"/>
        <source>Enables base mode. Collects stats in the BASE tracking bucket.</source>
        <translation>Schakel base modus in. Verz</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="471"/>
        <source>Ctrl+Shift+B</source>
        <translation>Ctrl+Shift+B</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="483"/>
        <source>Deb&amp;ugger Enabled</source>
        <translation>Deb&amp;ugger ingeschakeld</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="484"/>
        <source>Enables the debugger during skin parsing</source>
        <translation>Activeert de debugger tijdens het ontleden van het thema</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="491"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="507"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="511"/>
        <source>&amp;Community Support</source>
        <translation>&amp;Community Support</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="512"/>
        <source>Get help with Mixxx</source>
        <translation>Zoek hulp bij Mixxx</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="542"/>
        <source>&amp;User Manual</source>
        <translation>&amp;Handleiding</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="543"/>
        <source>Read the Mixxx user manual.</source>
        <translation>Lees de Mixxx gebruikershandleiding</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="551"/>
        <source>&amp;Keyboard Shortcuts</source>
        <translation>&amp;Sneltoetsen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="552"/>
        <source>Speed up your workflow with keyboard shortcuts.</source>
        <translation>Versnel je werktempo met sneltoetsen.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="560"/>
        <source>Send Us &amp;Feedback</source>
        <translation>Stuur ons &amp;Feedback</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="561"/>
        <source>Send feedback to the Mixxx team.</source>
        <translation>Feedback sturen naar het Mixxx team.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="569"/>
        <source>&amp;Translate This Application</source>
        <translation>&amp;Vertaal deze Applicatie</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="570"/>
        <source>Help translate this application into your language.</source>
        <translation>Help ons de applicatie te vertalen naar uw taal.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="580"/>
        <source>&amp;About</source>
        <translation>&amp;Over</translation>
    </message>
    <message>
        <location filename="../../src/widget/wmainmenubar.cpp" line="581"/>
        <source>About the application</source>
        <translation>Over de applicatie</translation>
    </message>
</context>
<context>
    <name>WOverview</name>
    <message>
        <location filename="../../src/widget/woverview.cpp" line="371"/>
        <source>Ready to play, analyzing ..</source>
        <extracomment>Text on waveform overview when file is playable but no waveform is visible</extracomment>
        <translation>Gereed om af te spelen, analyzeren ..</translation>
    </message>
    <message>
        <location filename="../../src/widget/woverview.cpp" line="374"/>
        <location filename="../../src/widget/woverview.cpp" line="384"/>
        <source>Loading track ..</source>
        <extracomment>Text on waveform overview when file is cached from source</extracomment>
        <translation>Track wordt geladen ..</translation>
    </message>
    <message>
        <location filename="../../src/widget/woverview.cpp" line="378"/>
        <source>Finalizing ..</source>
        <extracomment>Text on waveform overview during finalizing of waveform analysis</extracomment>
        <translation>Finaliseren ..</translation>
    </message>
</context>
<context>
    <name>WSearchLineEdit</name>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="69"/>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="215"/>
        <source>Clear input</source>
        <comment>Clear the search bar input field</comment>
        <translation>Invoer wissen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="75"/>
        <source>Ctrl+F</source>
        <comment>Search|Focus</comment>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="210"/>
        <source>Search...</source>
        <comment>noun</comment>
        <translation>Zoeken...</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="212"/>
        <source>Search</source>
        <comment>noun</comment>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="212"/>
        <source>Enter a string to search for</source>
        <translation>Voer een tekenreeks in om naar te zoeken</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="213"/>
        <source>Shortcut</source>
        <translation>Snelkoppeling</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="213"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="214"/>
        <source>Focus</source>
        <comment>Give search bar input focus</comment>
        <translation>Focus</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="214"/>
        <source>Ctrl+Backspace</source>
        <translation>Ctrl+Backspace</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="215"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../../src/widget/wsearchlineedit.cpp" line="216"/>
        <source>Exit search</source>
        <comment>Exit search bar and leave focus</comment>
        <translation>Zoeken afsluiten</translation>
    </message>
</context>
<context>
    <name>WTrackTableView</name>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="83"/>
        <source>Add to Playlist</source>
        <translation>Voeg toe aan Speellijst</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="99"/>
        <source>Reset</source>
        <extracomment>Reset metadata in right click track context menu in library</extracomment>
        <translation>Resetten</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="102"/>
        <source>Cover Art</source>
        <translation>Cover Art</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="130"/>
        <source>ESC</source>
        <comment>Focus</comment>
        <translation>ESC</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="429"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="432"/>
        <source>Remove from Playlist</source>
        <translation>Verwijderen uit Speellijst</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="435"/>
        <source>Remove from Crate</source>
        <translation>Verwijder uit krat</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="438"/>
        <source>Hide from Library</source>
        <translation>Verbergen voor Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="441"/>
        <source>Unhide from Library</source>
        <translation>Zichtbaar maken voor bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="444"/>
        <source>Purge from Library</source>
        <translation>Wissen uit Bibliotheek</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="447"/>
        <source>Properties</source>
        <translation>Eigenschappen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="451"/>
        <source>Open in File Browser</source>
        <translation>Open bestand in Browser</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="533"/>
        <source>4/3 BPM</source>
        <translation>4/3 BPM</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="534"/>
        <source>3/2 BPM</source>
        <translation>3/2 BPM</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="1576"/>
        <location filename="../../src/widget/wtracktableview.cpp" line="1599"/>
        <source>Create New Playlist</source>
        <translation>Nieuwe Afspeellijst aanmaken</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="1688"/>
        <source>Create New Crate</source>
        <translation>Maak nieuwe krat</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="1600"/>
        <source>Enter name for new playlist:</source>
        <translation>Voer naam in voor nieuwe afspeellijst:</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="76"/>
        <source>Load to</source>
        <translation>Laad naar</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="78"/>
        <source>Deck</source>
        <translation>Deck</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="80"/>
        <source>Sampler</source>
        <translation>Sampler</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="87"/>
        <source>Crates</source>
        <translation>Platenbakken</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="95"/>
        <source>Change BPM</source>
        <translation>BPM wijzigen</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="455"/>
        <source>Add to Auto DJ Queue (Bottom)</source>
        <translation>Voeg toe aan Auto DJ wachtrij (Einde)</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="459"/>
        <source>Add to Auto DJ Queue (Top)</source>
        <translation>Voeg toe aan Auto DJ wachtrij (Begin)</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="463"/>
        <source>Add to Auto DJ Queue (Replace)</source>
        <translation>Voeg toe aan Auto DJ wachtrij (Vervang)</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="467"/>
        <source>Import From File Tags</source>
        <translation>Importeer van bestandslabels</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="471"/>
        <source>Import From MusicBrainz</source>
        <translation>Importeer van MusicBrainz</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="475"/>
        <source>Export To File Tags</source>
        <translation>Exporteer naar bestandslabels</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="479"/>
        <source>Preview Deck</source>
        <translation>Preview Deck</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="488"/>
        <source>BPM and Beatgrid</source>
        <translation>BPM en Beat-raster</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="492"/>
        <source>Play Count</source>
        <translation>Play Count</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="496"/>
        <source>Cue Point</source>
        <translation>Cue Punt</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="500"/>
        <source>Hotcues</source>
        <translation>Hotcues</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="504"/>
        <source>Loop</source>
        <translation>Loop</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="508"/>
        <source>ReplayGain</source>
        <translation>ReplayGain</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="512"/>
        <source>Waveform</source>
        <translation>Waveform</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="516"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="852"/>
        <source>Deck %1</source>
        <translation>Deck %1</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="1602"/>
        <source>New Playlist</source>
        <translation>Nieuwe Afspeellijst</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="1609"/>
        <location filename="../../src/widget/wtracktableview.cpp" line="1613"/>
        <location filename="../../src/widget/wtracktableview.cpp" line="1622"/>
        <source>Playlist Creation Failed</source>
        <translation>Aanmaak van afspeellijst mislukt</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="1610"/>
        <source>A playlist by that name already exists.</source>
        <translation>Een afspeellijst met deze naam bestaan reeds.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="1614"/>
        <source>A playlist cannot have a blank name.</source>
        <translation>Een Afspeellijst kan geen blanco naam bevatten.</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="1623"/>
        <source>An unknown error occurred while creating playlist: </source>
        <translation>Er is een onbekende fout opgetreden tijdens het maken van de afspeellijst:</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="521"/>
        <source>Lock BPM</source>
        <translation>Vergrendel BPM</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="522"/>
        <source>Unlock BPM</source>
        <translation>Ontgrendel BPM</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="529"/>
        <source>Double BPM</source>
        <translation>Dubbele BPM</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="530"/>
        <source>Halve BPM</source>
        <translation>Halve BPM</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="531"/>
        <source>2/3 BPM</source>
        <translation>2/3 BPM</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="532"/>
        <source>3/4 BPM</source>
        <translation>3/4 BPM</translation>
    </message>
    <message>
        <location filename="../../src/widget/wtracktableview.cpp" line="872"/>
        <source>Sampler %1</source>
        <translation>Sampler %1</translation>
    </message>
</context>
<context>
    <name>WTrackTableViewHeader</name>
    <message>
        <location filename="../../src/widget/wtracktableviewheader.cpp" line="100"/>
        <source>Show or hide columns.</source>
        <translation>Toon of verberg kolommen.</translation>
    </message>
</context>
<context>
    <name>WaveformWidgetFactory</name>
    <message>
        <location filename="../../src/waveform/waveformwidgetfactory.cpp" line="804"/>
        <source>(GLSL)</source>
        <translation>(GLSL)</translation>
    </message>
    <message>
        <location filename="../../src/waveform/waveformwidgetfactory.cpp" line="806"/>
        <source>(GL)</source>
        <translation>(GL)</translation>
    </message>
    <message>
        <location filename="../../src/waveform/waveformwidgetfactory.cpp" line="816"/>
        <source>(GLSL ES)</source>
        <translation>(GLSL ES)</translation>
    </message>
    <message>
        <location filename="../../src/waveform/waveformwidgetfactory.cpp" line="818"/>
        <source>(GL ES)</source>
        <translation>(GL ES)</translation>
    </message>
</context>
<context>
    <name>mixxx::DlgTrackMetadataExport</name>
    <message>
        <location filename="../../src/library/dlgtrackmetadataexport.cpp" line="15"/>
        <source>Export Modified Track Metadata</source>
        <translation>Gewijzigde track metagegevens exporteren</translation>
    </message>
    <message>
        <location filename="../../src/library/dlgtrackmetadataexport.cpp" line="16"/>
        <source>Mixxx may wait to modify files until they are not loaded to any decks or samplers. If you do not see changed metadata in other programs immediately, eject the track from all decks and samplers or shutdown Mixxx.</source>
        <translation>Mixxx kan wachten met het wijzigen van bestanden zolang ze niet in decks of samplers zijn geladen. Als je niet meteen gewijzigde metadata in andere programma&apos;s ziet, verwijder dan de track uit alle spelers en samplers of schakel Mixxx uit.</translation>
    </message>
</context>
</TS>