#!/usr/bin/make -f

export CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
# The build system ignores CPPFLAGS, pass them to CFLAGS/CXXFLAGS to enable
# the missing (hardening) flags.
export CFLAGS:=$(shell dpkg-buildflags --get CFLAGS) $(CPPFLAGS)
export CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS) $(CPPFLAGS)
export LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS) -Wl,--as-needed

export QT_SELECT=5

export DEB_HOST_ARCH      ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

# flags suggested by upstream
SCONS_OPTS  = build=release
SCONS_OPTS += qt_sqlite_plugin=0
SCONS_OPTS += virtualize=0
SCONS_OPTS += localecompare=1
# destdir, prefix and machine
SCONS_OPTS += prefix=/usr
SCONS_OPTS += install_root=$(CURDIR)/debian/tmp/usr
SCONS_OPTS += machine=$(DEB_HOST_ARCH)
# Qt
SCONS_OPTS += qt5=1
SCONS_OPTS += qtdir=/usr/share/qt5
# disable use of __DATE__ and __TIME__
SCONS_OPTS += buildtime=0
# build with OpenGL ES 2 on armel and armhf
ifeq (,$(filter-out armel armhf,$(DEB_HOST_ARCH)))
SCONS_OPTS += opengles=1
endif
# parallel builds for scons
ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
SCONS_OPTS += -j$(NUMJOBS)
endif

%:
	dh $@ --with=scour

override_dh_auto_build:
	scons $(SCONS_OPTS) w=1
	docbook-to-man debian/mixxx.sgml > mixxx.1

override_dh_auto_clean:
	scons $(SCONS_OPTS) -c || true
	rm -rf .sconf_temp/ cache/ linux_build/
	dh_clean .sconsign.dblite cachecustom.py \
		config.log src/build.h build/__pycache__/*.pyc mixxx.1
	dh_auto_clean
	rm -rf lin*_build/

override_dh_auto_install:
	scons $(SCONS_OPTS) install
	find $(CURDIR)/debian/tmp \( -name COPYING -o -name Thumbs.db \
		-o -name shifter.sh -o -name '*.xsl' -o -name '*.php' \
		-o -name LICENSE -o -name README \) -exec rm -f {} \;
	find $(CURDIR)/debian/tmp -name '*.js' -exec chmod -x {} \;
	find $(CURDIR)/debian/tmp/usr/share/mixxx/ -type f -exec chmod -x {} \;
	chrpath -d debian/tmp/usr/bin/mixxx \
		debian/tmp/usr/lib/mixxx/plugins/vampqt5/libmixxxminimal.so
